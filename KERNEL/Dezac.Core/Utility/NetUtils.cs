﻿using Dezac.Core.Exceptions;
using log4net;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Dezac.Core.Utility
{
    public static class NetUtils
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(NetUtils));

        public static NetworkInterface GetNetworkInterfaceTest()
        {
            var networks = NetworkInterface.GetAllNetworkInterfaces();
            NetworkInterface network = null;

            logger.InfoFormat("Enumerating Network Interfaces.");
            foreach (var nw in networks)
                if (nw.OperationalStatus == OperationalStatus.Up && (nw.NetworkInterfaceType == NetworkInterfaceType.Ethernet || nw.NetworkInterfaceType == NetworkInterfaceType.GigabitEthernet))
                {
                    logger.InfoFormat("Using Network Interface -> {0}", nw.Name);
                    var prop = nw.GetIPProperties();
                    if (prop.DnsSuffix.Trim().ToLower().Contains("dezac.net"))
                        continue;

                    foreach (var adpt in prop.UnicastAddresses.ToList())
                        if (adpt.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            logger.InfoFormat("Network Interface Selected to work -> {0}", nw.Name);
                            network = nw;
                        }

                    if (network != null)
                        break;
                }

            return network;
        }

        public static string GetDefaultGateway()
        {
            IPAddress internalDns = null;

            foreach (var card in NetworkInterface.GetAllNetworkInterfaces().Where(p => p.OperationalStatus == OperationalStatus.Up && p.NetworkInterfaceType == NetworkInterfaceType.Ethernet))
                if (card != null)
                {
                    logger.InfoFormat("Using Network Interface -> {0}", card.Name);
                    var address = card.GetIPProperties().DhcpServerAddresses.FirstOrDefault();
                    if (address != null)
                        if (card.GetIPProperties().DnsSuffix != "dezac.net")
                        {
                            logger.InfoFormat("Network Interface Selected to work -> {0}", card.Name);
                            internalDns = address;
                            break;
                        }
                }

            if (internalDns == null)
                TestException.Create().SOFTWARE.FALLO_RED.CONFIGURACION_INCORRECTA("Red del Router de la Torre no encontrada").Throw();

            return internalDns.ToString();
        }

        public static string GetMyIP()
        {
            IPAddress miIP = null;

            logger.InfoFormat("Enumerating Network Interfaces.");
            foreach (var card in NetworkInterface.GetAllNetworkInterfaces().Where(p => p.OperationalStatus == OperationalStatus.Up && p.NetworkInterfaceType == NetworkInterfaceType.Ethernet))
                if (card != null)
                {
                    logger.InfoFormat("Using Network Interface -> {0}", card.Name);
                    var props = card.GetIPProperties();
                    var address = props.UnicastAddresses.Where(a => a.Address.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault();
                    if (address != null)
                    {
                        logger.InfoFormat("DnsSuffix: {0}  -> IP: {1}", props.DnsSuffix, address.Address);
                        if (props.DnsSuffix != "dezac.net")
                        {
                            miIP = address.Address;
                            break;
                        }
                        else
                            miIP = address.Address;
                    }
                }

            return miIP.ToString();
        }

        //*******************************************************************
        //*******************************************************************
        public static void ConfigIP(string startIpAddress, string gateway, string subnetMask = "255.255.255.0", bool isAdmin = false)
        {
            var adapter = GetNetworAdapter();

            if (!adapter.Dhcp && adapter.IPAAddress == startIpAddress && adapter.SubNetMask == subnetMask)
                return;    // no change necessar

            var argmunet = $"interface ip set address \"{adapter.Name}\" source=static address={startIpAddress} mask={subnetMask} gateway={gateway}";

            PowerShell.ProcessNetsh("ConfigIP", argmunet, isAdmin);
        }

        public static void ConfigIP_DHCP(bool isAdmin = false)
        {
            var adapter = GetNetworAdapter();

            var argmunet = $"interface ip set address \"{adapter.Name}\" dhcp";

            logger.InfoFormat("interface set DHCP-");

            if (!adapter.Dhcp)
                PowerShell.ProcessNetsh("ConfigIP_DHCP", argmunet, isAdmin);
        }

        public static void AddIPtoARP(string ipAdress, string mac, bool isAdmin = false )
        {
            var adapter = GetNetworAdapter();

            var argmunet = $"interface ipv4 add neighbors \"{adapter.Name}\" {ipAdress} {mac.Replace(":","-")}";

            PowerShell.ProcessNetsh("Add_ARP_IP", argmunet, isAdmin);
        }

        public static void DeleteARP(bool isAdmin = false)
        {
            var adapter = GetNetworAdapter();

            var argmunet = $"interface ipv4 delete neighbors \"{adapter.Name}\"";

            PowerShell.ProcessNetsh("Delete_ARP_IP", argmunet, isAdmin);
        }

        public static string GetARP(string filterAdapter)
        {
            Process p = null;
            string output = string.Empty;

            try
            {
                logger.InfoFormat("GetARP -- ProcessStartInfo Start ->  arp -a -N {0}", filterAdapter);

                p = Process.Start(new ProcessStartInfo("arp", string.Format("-a -N {0}", filterAdapter))
                {
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    RedirectStandardOutput = true
                });

                output = p.StandardOutput.ReadToEnd();

                logger.InfoFormat("GetARP -- ProcessStartInfo end ->  output {0}", output);

                p.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("IPInfo: Error Retrieving 'arp -a' Results", ex);
            }
            finally
            {
                if (p != null)
                    p.Close();
            }

            return output;
        }

        public class NetworkAdapter
        {
            public string Name { get; set; }
            public string IPAAddress { get; set; }
            public string SubNetMask { get; set; }
            public bool Dhcp { get; set; }
        }

        public static NetworkAdapter GetNetworAdapter()
        {
            NetworkInterface network = GetNetworkInterfaceTest();
            if (network == null)
                TestException.Create().HARDWARE.PC.ADAPTADOR_DE_RED_ETHERNET("NO SE HA DETECTADO UN ADAPTADOR DE RED A CONFIGURAR").Throw();

            var ipProperties = network.GetIPProperties();
            var ipInfo = ipProperties.UnicastAddresses.FirstOrDefault(ip => ip.Address.AddressFamily == AddressFamily.InterNetwork);
            var networkAdaper = new NetworkAdapter()
            {
                Name = network.Name,
                IPAAddress = ipInfo.Address.ToString(),
                SubNetMask = ipInfo.IPv4Mask.ToString(),
                Dhcp = ipProperties.GetIPv4Properties().IsDhcpEnabled
            };

            logger.InfoFormat("NetworkInterface Name: {0}", networkAdaper.Name);
            logger.InfoFormat("currentIPaddress: {0}", networkAdaper.IPAAddress);
            logger.InfoFormat("currentSubnetMask: {0}", networkAdaper.SubNetMask);
            logger.InfoFormat("isDHCPenabled: {0}", networkAdaper.Dhcp);

            return networkAdaper;
        }

    }

    public class ClientEntry
    {
        public string Name { get; set; }
        public string IP { get; set; }
        public string MAC { get; set; }
        public string Interface { get; set; }
        public string Expire { get; set; }
        public int OrderIP { get; set; }
    }
}
