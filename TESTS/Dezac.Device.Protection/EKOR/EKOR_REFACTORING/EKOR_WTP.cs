﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.01)]
    public class EKOR_WTP : EKOR_SAFE
    {
        public EKOR_WTP()
        {

        }

        public EKOR_WTP(int port)
        {
            SetPort(port);
        }

        #region Enumerados
        public enum Scale
        {
            Esc1 = 0,
            Esc2 = 1,
            EscCC = 2
        }

        [Flags]
        public enum Channel
        {
            [Browsable(false)]
            ALL = 0x03FF,
            IN = 0x0008,
            VL123_VF123_IL123 = ALL - IN,
        }

        public enum Lines
        {
            L1 = 1,
            L2 = 2,
            L3 = 3,
            LH = 5,
            L4 = 6,
            L5 = 7,
            L6 = 8,
        }

        [Flags]
        public enum enumInputs
        {
            [Description("Entrada SAFE1")]
            SAFE1 = 0,
            [Description("Entrada SAFE2")]
            SAFE2 = 1,
            [Description("Entrada digital 1")]
            IN1 = 2,
            [Description("Entrada digital 2")]
            IN2 = 3,
            [Description("Entrada digital 3")]
            IN3 = 4,
            [Description("Entrada digital 4")]
            IN4 = 5,
            [Description("Entrada digital 5")]
            IN5 = 6,
            [Description("Entrada digital 6")]
            IN6 = 7,
            [Description("Entrada digital 7")]
            IN7 = 8,
            [Description("Entrada digital 8")]
            IN8 = 9,
        }

        #endregion

        #region Metodos AutoCaliobracion

        public void AutoCalibration_All_Sin_IN(Scale scale, TriLineValue voltage, TriLineValue current, int numberCycles, double relGanaciasTension, double relGanaciasCorriente)
        {
            LogMethod();

            var channel = Channel.VL123_VF123_IL123;
            _logger.InfoFormat("AutoCalibration  con Channel = {0}  Scala = {1}",  channel.ToString(), scale.ToString());

            if (relGanaciasCorriente == 0)
                TestException.Create().PROCESO.PARAMETROS_ERROR.FALTA_VALOR_PARAMETRO("Relacion_Corriente").Throw();

            if (relGanaciasTension == 0)
                TestException.Create().PROCESO.PARAMETROS_ERROR.FALTA_VALOR_PARAMETRO("Relacion_Tension").Throw();

            _logger.InfoFormat("AutoCalibration Voltage = {0}", voltage.ToString());
            _logger.InfoFormat("AutoCalibration Corriente = {0}", current.ToString());

            current *= relGanaciasCorriente;
            voltage *= relGanaciasTension;

            var valorVoltageL1 = (int)(voltage.L1 * FactorPasaTapas.L1_FactorAjuste);
            var valorVoltageL2 = (int)(voltage.L2 * FactorPasaTapas.L2_FactorAjuste);
            var valorVoltageL3 = (int)(voltage.L3 * FactorPasaTapas.L3_FactorAjuste);
            var valorVoltageL4 = (int)(voltage.L1 * FactorPasaTapas.L4_FactorAjuste);
            var valorVoltageL5 = (int)(voltage.L2 * FactorPasaTapas.L5_FactorAjuste);
            var valorVoltageL6 = (int)(voltage.L3 * FactorPasaTapas.L6_FactorAjuste);

            _logger.InfoFormat("AutoCalibration Voltage L1 con Relacion = {0}  Factor Pasa Tapas = {1} Voltage = {2}", relGanaciasTension, FactorPasaTapas.L1_FactorAjuste, valorVoltageL1.ToString());
            _logger.InfoFormat("AutoCalibration Voltage L2 con Relacion = {0}  Factor Pasa Tapas = {1} Voltage = {2}", relGanaciasTension, FactorPasaTapas.L2_FactorAjuste, valorVoltageL2.ToString());
            _logger.InfoFormat("AutoCalibration Voltage L3 con Relacion = {0}  Factor Pasa Tapas = {1} Voltage = {2}", relGanaciasTension, FactorPasaTapas.L3_FactorAjuste, valorVoltageL3.ToString());
            _logger.InfoFormat("AutoCalibration Voltage L4 con Relacion = {0}  Factor Pasa Tapas = {1} Voltage = {2}", relGanaciasTension, FactorPasaTapas.L4_FactorAjuste, valorVoltageL4.ToString());
            _logger.InfoFormat("AutoCalibration Voltage L5 con Relacion = {0}  Factor Pasa Tapas = {1} Voltage = {2}", relGanaciasTension, FactorPasaTapas.L5_FactorAjuste, valorVoltageL5.ToString());
            _logger.InfoFormat("AutoCalibration Voltage L6 con Relacion = {0}  Factor Pasa Tapas = {1} Voltage = {2}", relGanaciasTension, FactorPasaTapas.L6_FactorAjuste, valorVoltageL6.ToString());

            _logger.InfoFormat("AutoCalibration Corriente L1 con Relacion = {0}  corriente = {1}", relGanaciasCorriente, current.L1);
            _logger.InfoFormat("AutoCalibration Corriente L2 con Relacion = {0}  corriente = {1}", relGanaciasCorriente, current.L2);
            _logger.InfoFormat("AutoCalibration Corriente L3 con Relacion = {0}  corriente = {1}", relGanaciasCorriente, current.L3);

            var param = new List<string>();
            param.Add(((int)current.L1).ToString());
            param.Add(((int)current.L2).ToString());
            param.Add(((int)current.L3).ToString());
            param.Add(valorVoltageL1.ToString());
            param.Add(valorVoltageL2.ToString());
            param.Add(valorVoltageL3.ToString());
            param.Add(valorVoltageL4.ToString());
            param.Add(valorVoltageL5.ToString());
            param.Add(valorVoltageL6.ToString());

            var parameters = "";
            foreach (var data in param.ToArray())
                parameters += " " + data;

            Cirbus.Write(AUTOCALIBRATION_NEW, listErrorCalibration, ((ushort)channel).ToString("X4"), (ushort)scale, parameters.TrimStart(), numberCycles.ToString());
        }

        public void AutoCalibration_IN(Scale scale, double corriente, int numberCycles, double relGanaciasCorriente)
        {
            LogMethod();

            var channel = Channel.IN;

            _logger.InfoFormat("AutoCalibration con Channel = {0}  Scala = {1}",  channel.ToString(), scale.ToString());

            corriente *= relGanaciasCorriente;

            if (relGanaciasCorriente == 0)
                TestException.Create().PROCESO.PARAMETROS_ERROR.FALTA_VALOR_PARAMETRO("Relacion_Voltage").Throw();

            _logger.InfoFormat("AutoCalibration Corriente con Relacion = {0}  Voltage = {1}", relGanaciasCorriente, corriente.ToString());

            Cirbus.Write(AUTOCALIBRATION_NEW, listErrorCalibration, ((ushort)channel).ToString("X4"), (ushort)scale, ((int)corriente).ToString(), numberCycles.ToString());
        }

        public StructCalibrationMeasure ReadFactorsCalibrated(Scale scale)
        {
            LogMethod();
            return Cirbus.Read<StructCalibrationMeasure>(READCALIBRATIONSCALE, listErrorCalibration, (byte)scale);
        }

        public void WriteFactorsCalibration(Scale scale, StructCalibrationMeasure factorsScaleCalibrated, double relGanaciasTension, double relGanaciasCorriente)
        {
            LogMethod();

            var resp = factorsScaleCalibrated.resp;
            var factor_IL1 = Convert.ToInt64(factorsScaleCalibrated.factor_IL1 / relGanaciasCorriente);
            var factor_IL2 = Convert.ToInt64(factorsScaleCalibrated.factor_IL2 / relGanaciasCorriente);
            var factor_IL3 = Convert.ToInt64(factorsScaleCalibrated.factor_IL3 / relGanaciasCorriente);
            var factor_IH = Convert.ToInt64(factorsScaleCalibrated.factor_IH / relGanaciasCorriente);
            var factor_VL1 = Convert.ToInt64(factorsScaleCalibrated.factor_VL1 / relGanaciasTension);
            var factor_VL2 = Convert.ToInt64(factorsScaleCalibrated.factor_VL2 / relGanaciasTension);
            var factor_VL3 = Convert.ToInt64(factorsScaleCalibrated.factor_VL3 / relGanaciasTension);
            var factor_VF1 = Convert.ToInt64(factorsScaleCalibrated.factor_VF1 / relGanaciasTension);
            var factor_VF2 = Convert.ToInt64(factorsScaleCalibrated.factor_VF2 / relGanaciasTension);
            var factor_VF3 = Convert.ToInt64(factorsScaleCalibrated.factor_VF3 / relGanaciasTension);
            var offset_IL1 = factorsScaleCalibrated.offset_IL1;
            var offset_IL2 = factorsScaleCalibrated.offset_IL2;
            var offset_IL3 = factorsScaleCalibrated.offset_IL3;
            var offset_IH = factorsScaleCalibrated.offset_IH;
            var offset_VL1 = factorsScaleCalibrated.offset_VL1;
            var offset_VL2 = factorsScaleCalibrated.offset_VL2;
            var offset_VL3 = factorsScaleCalibrated.offset_VL3;
            var offset_VF1 = factorsScaleCalibrated.offset_VF1;
            var offset_VF2 = factorsScaleCalibrated.offset_VF2;
            var offset_VF3 = factorsScaleCalibrated.offset_VF3;

            _logger.InfoFormat("Factors Corriente Scala = {0}   IL1 ={1}   IL2={2}  IL3={3}  IH = {4}" , scale.ToString(), factor_IL1, factor_IL2, factor_IL3, factor_IH);
            _logger.InfoFormat("Factors Tensiones Scala = {0}   VL1 ={1}   VL2={2}  VL3={3} ", scale.ToString(), factor_VL1, factor_VL2, factor_VL3);
            _logger.InfoFormat("Factors Tensiones Fusible Scala = {0}   VF1 ={1}   VF2={2}  VFF3={3} ", scale.ToString(), factor_VF1, factor_VF2, factor_VF3);
            _logger.InfoFormat("Factors Offsets Corrientes  Scala = {0}   offset_IL1 ={1}   offset_IL2={2}  offset_IL3={3}  offset_IH = {4}", scale.ToString(), offset_IL1, offset_IL2, offset_IL3, offset_IH);
            _logger.InfoFormat("Factors Offsets Tension  Scala = {0}   offset_VL1 ={1}   offset_VL2={2}  offset_VL3={3} ", scale.ToString(), offset_VL1, offset_VL2, offset_VL3);
            _logger.InfoFormat("Factors Offsets Tension Fusible Scala = {0}   offset_VF1 ={1}   offset_VF2={2}  offset_VF3={3} ", scale.ToString(), offset_VF1, offset_VF2, offset_VF3);

            var trama = string.Format("{0:00000000000} {1:00000000000} {2:00000000000} {3:00000000000} {4:00000000000} {5:00000000000} {6:00000000000} {7:00000000000} {8:00000000000} {9:00000000000} {10:00000} {11:00000} {12:00000} {13:00000} {14:00000} {15:00000} {16:00000} {17:00000} {18:00000} {19:00000}",
    factor_IL1, factor_IL2, factor_IL3, factor_IH, factor_VL1, factor_VL2, factor_VL3, factor_VF1, factor_VF2, factor_VF3, offset_IL1, offset_IL2, offset_IL3, offset_IH, offset_VL1, offset_VL2, offset_VL3, offset_VF1, offset_VF2, offset_VF3);

            Cirbus.Write(WRITEFACTORSCALIBRATION, listErrorCalibration, (int)scale, trama);
        }

        public struct StructCalibrationMeasure
        {
            public string resp;
            public int factor_IL1;
            public int factor_IL2;
            public int factor_IL3;
            public int factor_IH;
            public int factor_VL1;
            public int factor_VL2;
            public int factor_VL3;
            public int factor_VF1;
            public int factor_VF2;
            public int factor_VF3;
            public int offset_IL1;
            public int offset_IL2;
            public int offset_IL3;
            public int offset_IH;
            public int offset_VL1;
            public int offset_VL2;
            public int offset_VL3;
            public int offset_VF1;
            public int offset_VF2;
            public int offset_VF3;
        }

        #endregion

        #region Metodos Medida

        public MedidaInstantanea ReadInstantMeasures(Lines canal)
        {
            LogMethod();

            var result = Cirbus.Read<MeasureInstante>(string.Format(READPARAMETERSMEASURE, (byte)canal), false);

            if (Rel_Voltage_Measure == 0)
                Rel_Voltage_Measure = 1;

            var _rel_Voltage_Measure = Rel_Voltage_Measure;
      
            if (Rel_Current_Measure == 0)
                Rel_Current_Measure = 1;

            var Rel_PasaTapas = 1D;

            switch (canal)
            {
                case Lines.L1:
                    Rel_PasaTapas = FactorPasaTapas.L1_FactorCalibracion;
                    break;
                case Lines.L2:
                    Rel_PasaTapas = FactorPasaTapas.L2_FactorCalibracion;
                    break;
                case Lines.L3:
                    Rel_PasaTapas = FactorPasaTapas.L3_FactorCalibracion;
                    break;
                case Lines.L4:
                    Rel_PasaTapas = FactorPasaTapas.L4_FactorCalibracion;
                    break;
                case Lines.L5:
                    Rel_PasaTapas = FactorPasaTapas.L5_FactorCalibracion;
                    break;
                case Lines.L6:
                    Rel_PasaTapas = FactorPasaTapas.L6_FactorCalibracion;
                    break;
                default:
                    Rel_PasaTapas = 1;
                    break;
            }

            _logger.InfoFormat("Medida en la linea {0} con Relación Voltage = {1}  con Relación Corriente = {2} y valor PasaTapas = {3}", canal.ToString(), _rel_Voltage_Measure, Rel_Current_Measure, Rel_PasaTapas);
            var value = new MedidaInstantanea(result, _rel_Voltage_Measure, Rel_Current_Measure, Rel_PasaTapas);
            _logger.InfoFormat("Medida en la linea {0} Valor = {1}", canal.ToString(), value);

            return value;
        }

        public AllMeasureInstante ReadAllInstantMeasures()
        {
            LogMethod();

            var l1_value = ReadInstantMeasures(Lines.L1);
            var l2_value = ReadInstantMeasures(Lines.L2);
            var l3_value = ReadInstantMeasures(Lines.L3);
            var lH_value = ReadInstantMeasures(Lines.LH);
            var l4_value = ReadInstantMeasures(Lines.L4);
            var l5_value = ReadInstantMeasures(Lines.L5);
            var l6_value = ReadInstantMeasures(Lines.L6);

            var result = new AllMeasureInstante()
            {
                L1 = l1_value,
                L2 = l2_value,
                L3 = l3_value,
                LH = lH_value,
                L4 = l4_value,
                L5 = l5_value,
                L6 = l6_value,
            };

            return result;
        }

        public struct AllMeasureInstante
        {
            public MedidaInstantanea L1;
            public MedidaInstantanea L2;
            public MedidaInstantanea L3;
            public MedidaInstantanea LH;
            public MedidaInstantanea L4;
            public MedidaInstantanea L5;
            public MedidaInstantanea L6;
        }

        public struct MedidaInstantanea
        {
            public double Voltage { get; set; }
            public double Current { get; set; }
            public double ActivePower { get; set; }
            public double ReactivePower { get; set; }
            public double AparentPower { get; set; }
            public double PowerFactor { get; set; }
            public double Phi { get; set; }

            public MedidaInstantanea(MeasureInstante value, double rel_Voltage, double rel_Current, double rel_PasaTapas)
            {
                Voltage = (value.Voltage / rel_Voltage) * rel_PasaTapas;
                Current = value.Current / rel_Current;
                ActivePower = value.ActivePower;
                ReactivePower = value.ReactivePower;
                AparentPower = value.AparentPower;
                PowerFactor = value.PowerFactor;
                Phi = value.Phi;
            }

            public override string ToString()
            {
                return string.Format("Voltage={0}  Current={1}  ActivePower={2}  ReactivePower={3}  AparentPower={4}  PowerFactor={5}  Phi={6}", Voltage, Current, ActivePower, ReactivePower, AparentPower, PowerFactor, Phi);
            }
        }

        public struct MeasureInstante
        {
            public string RESP;
            public string V;
            public string I;
            public string P;
            public string Q;
            public string S;
            public string PF;
            public string PHI;

            public double Voltage
            {
                get
                {
                    var resp = V.Replace("V:", "");
                    return Convert.ToDouble(resp);
                }
            }
            public double Current
            {
                get
                {
                    var resp = I.Replace("I:", "");
                    return Convert.ToDouble(resp);
                }
            }
            public double ActivePower
            {
                get
                {
                    var resp = P.Replace("P:", "");
                    return Convert.ToDouble(resp);
                }
            }
            public double ReactivePower
            {
                get
                {
                    var resp = Q.Replace("Q:", "");
                    return Convert.ToDouble(resp);
                }
            }
            public double AparentPower
            {
                get
                {
                    var resp = S.Replace("S:", "");
                    return Convert.ToDouble(resp);
                }
            }
            public double PowerFactor
            {
                get
                {
                    var resp = PF.Replace("PF:", "");
                    var valor = 999D;
                    Double.TryParse(resp, out valor);
                    return valor;
                }
            }
            public double Phi
            {
                get
                {
                    var resp = PHI.Replace("Phi:", "");
                    return Convert.ToDouble(resp);
                }
            }
        }

        public MedidaInstantaneaCurrentPhase ReadCurrentPhase()
        {
            LogMethod();
            var result = Cirbus.Read<MeasureStructCurrenPhase>(READCURRENTFUNDAMENTAL, listErrorFlash, "P1");

            if (Rel_Current_Measure == 0)
                Rel_Current_Measure = 1;

            _logger.InfoFormat("Medida Corriente con Relación Corriente = {0}", Rel_Current_Measure);
            var value = new MedidaInstantaneaCurrentPhase(result, Rel_Current_Measure);
            _logger.InfoFormat("Medida corriente ={0}",  value);

            return value;
        }

        public struct MeasureStructCurrenPhase
        {
            public double I1;
            public double I2;
            public double I3;
            public double IN;
            public double INS;
            public string signe;
            public double PH1;
            public double PH2;
            public double PH3;
            public double PHN;
            public double PHS;

            public double phase1 { get { return PH1 / 100; } }
            public double phase2 { get { return PH2 / 100; } }
            public double phase3 { get { return PH3 / 100; } }
            public double phaseN { get { return PHN / 100; } }
            public double phaseS { get { return PHS / 100; } }
        }

        public struct MedidaInstantaneaCurrentPhase
        {
            public double I1 { get; set; }
            public double I2 { get; set; }
            public double I3 { get; set; }
            public double IN { get; set; }
            public double INS { get; set; }
            public double PH1 { get; set; }
            public double PH2 { get; set; }
            public double PH3 { get; set; }
            public double PHN { get; set; }
            public double PHS { get; set; }

            public MedidaInstantaneaCurrentPhase(MeasureStructCurrenPhase value, double rel_Current)
            {
                I1 = value.I1 / rel_Current;
                I2 = value.I2 / rel_Current;
                I3 = value.I3 / rel_Current;
                IN = value.IN / rel_Current;
                INS = value.INS / rel_Current;

                PH1 = value.PH1 / 100;
                PH2 = value.PH2 / 100;
                PH3 = value.PH3 / 100;
                PHN = value.PHN / 100;
                PHS = value.PHS / 100;
            }

            public override string ToString()
            {
                return string.Format("I1={0}  I2={1}  I3={2}  INS={3}  PH1={4}  PH2={5}  PH3={6}  PHN={7}  PHS={8}", 
                    I1, I2, I3, IN, INS, PH1,PH2,PH3, PHN, PHS);
            }
        }

        public MedidaInstantaneaVoltagePhase ReadVoltage()
        {
            LogMethod();
            var result = Cirbus.Read<MeasureStructVoltagePhase>(READVOLTAGEFUNFAMENTAL, listErrorFlash, "P1");

            if (Rel_Voltage_Measure == 0)
                Rel_Voltage_Measure = 1;

            _logger.InfoFormat("Medida Tension con Relación Tension = {0}", Rel_Voltage_Measure);
            var value = new MedidaInstantaneaVoltagePhase(result, Rel_Voltage_Measure);
            _logger.InfoFormat("Medida Tension ={0}", value);

            return value;
        }

        public struct MeasureStructVoltagePhase
        {
            public double V1;
            public double V2;
            public double V3;
            public double VN;
            public double VNS;
            public double VF1;
            public double VF2;
            public double VF3;
            public double VFN;
            public string signe;
            public double PH1;
            public double PH2;
            public double PH3;
            public double PHN;
            public double PHNS;
            public double PHF1;
            public double PHF2;
            public double PHF3;
            public double PHFN;

            public double phase1 { get { return PH1 / 100; } }
            public double phase2 { get { return PH2 / 100; } }
            public double phase3 { get { return PH3 / 100; } }
            public double phaseN { get { return PHN / 100; } }
            public double phaseFuse1 { get { return PHF1 / 100; } }
            public double phaseFuse2 { get { return PHF2 / 100; } }
            public double phaseFuse3 { get { return PHF3 / 100; } }
            public double phaseFuseN { get { return PHFN / 100; } }
        }

        public struct MedidaInstantaneaVoltagePhase
        {
            public double V1 { get; set; }
            public double V2 { get; set; }
            public double V3 { get; set; }
            public double VN { get; set; }
            public double VNS { get; set; }
            public double VF1 { get; set; }
            public double VF2 { get; set; }
            public double VF3 { get; set; }
            public double VFN { get; set; }
            public double PH1 { get; set; }
            public double PH2 { get; set; }
            public double PH3 { get; set; }
            public double PHN { get; set; }
            public double PHNS { get; set; }
            public double PHF1 { get; set; }
            public double PHF2 { get; set; }
            public double PHF3 { get; set; }
            public double PHFN { get; set; }

            public MedidaInstantaneaVoltagePhase(MeasureStructVoltagePhase value, double rel_Voltage)
            {
                V1 = value.V1 / rel_Voltage;
                V2 = value.V2 / rel_Voltage;
                V3 = value.V3 / rel_Voltage;
                VN = value.VN / rel_Voltage;
                VNS = value.VNS / rel_Voltage;

                VF1 = value.VF1 / rel_Voltage;
                VF2 = value.VF2 / rel_Voltage;
                VF3 = value.VF3 / rel_Voltage;
                VFN = value.VFN / rel_Voltage;

                PH1 = value.PH1 / 100;
                PH2 = value.PH2 / 100;
                PH3 = value.PH3 / 100;
                PHN = value.PHN / 100;
                PHNS = value.PHNS / 100;

                PHF1 = value.PHF1 / 100;
                PHF2 = value.PHF2 / 100;
                PHF3 = value.PHF3 / 100;
                PHFN = value.PHFN / 100;
            }

            public override string ToString()
            {
                return string.Format("V1={0}  V2={1}  V3={2}  VN={3}  VNS={4}  VF1={5}  VF2={6} VF3={7} VFN={8} PH1={9}  PH2={10}  PH3={11}  PHN={12}  PHNS={13}  PHF1={14}  PHF2={15}  PHF3={16}  PHFN={12}",
                    V1, V2, V3, VN, VNS, VF1, VF2, VF3, VFN, PH1, PH2, PH3, PHN, PHNS, PHF1, PHF2, PHF3, PHFN);
            }
        }  

        #endregion


    }
}
