﻿using System;
using System.Collections.Generic;

namespace Dezac.Core.Utility
{
    public struct TriLinePower
    {
        public TriLineValue KW { get; set; }
        public TriLineValue KVAR { get; set; }
        public TriLineValue KVA { get; set; }

        public readonly static TriLineValue Zero = new TriLineValue();

        public static TriLinePower Create(TriLineValue voltage, TriLineValue current, TriLineValue phaseGap)
        {
            var power = voltage * current;
            var phaseActive = TriLineValue.Create(Math.Cos(phaseGap.L1.ToRadians()), Math.Cos(phaseGap.L2.ToRadians()), Math.Cos(phaseGap.L3.ToRadians()));
            var phaseReactive = TriLineValue.Create(Math.Sin(phaseGap.L1.ToRadians()), Math.Sin(phaseGap.L2.ToRadians()), Math.Sin(phaseGap.L3.ToRadians()));

            TriLinePower Power = new TriLinePower()
            {
                KW = power * phaseActive,        
                KVAR = power *  phaseReactive,    
                KVA = power,
            };
            return Power;
        }

        public static TriLinePower Create(double voltage, double current, double phaseGap)
        {
            var kw = TriLineValue.Create(voltage * current * Math.Cos(phaseGap.ToRadians()));
            var kvar = TriLineValue.Create(voltage * current * Math.Sin(phaseGap.ToRadians()));
            TriLinePower Power = new TriLinePower()
            {
                KW=kw,
                KVAR=kvar,
                KVA = voltage * current,
            };
            return Power;
        }

        public static TriLinePower operator *(double x, TriLinePower y)
        {
            y.KVAR *= x;
            y.KW *= x;
            y.KVA *= x;

            return y;
        }

        public override string ToString()
        {
            return string.Format("POT.ACTIVA L1={0}, L2={1}, L3={2}    POT.REACTIVA L1{3}, L2={4}, L3={5}    POT.APARENTE L1{6}, L2={7}, L3={8}", KW.L1, KW.L2, KW.L3, KVAR.L1, KVAR.L2, KVAR.L3, KVA.L1, KVA.L2, KVA.L3);
        }

        public double[] ToArray()
        {
            var list = new List<double>();
            list.AddRange(KW.ToArray());
            list.AddRange(KVAR.ToArray());
            list.AddRange(KVA.ToArray());
            return list.ToArray();
        }

        public static double[] ToArray(TriLineValue value)
        {
            var list = new List<double>();
            list.AddRange(value.ToArray());
            list.AddRange(value.ToArray());
            return list.ToArray();
        }

    }
}
