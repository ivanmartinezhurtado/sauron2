﻿using Dezac.Tests.Model;
using TaskRunner.Model;

namespace WinTaskRunner.Views
{
    partial class VariablesView
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn5 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn6 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn7 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn8 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn9 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn10 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn11 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn12 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn13 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn14 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Instancia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tiempo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Variable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Plot = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dataGridLogPlot = new System.Windows.Forms.DataGridView();
            this.Instance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Parameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enumBinder1 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder2 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder3 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder4 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder5 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder6 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder7 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder8 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder9 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder10 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder11 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder12 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder13 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder14 = new Telerik.WinControls.UI.Data.EnumBinder();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridLogPlot)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dataGridView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel2Collapsed = true;
            this.splitContainer1.Size = new System.Drawing.Size(1190, 712);
            this.splitContainer1.SplitterDistance = 639;
            this.splitContainer1.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Instancia,
            this.Tiempo,
            this.Variable,
            this.Valor,
            this.Plot});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1190, 712);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // Instancia
            // 
            this.Instancia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Instancia.Frozen = true;
            this.Instancia.HeaderText = "Instancia";
            this.Instancia.MinimumWidth = 55;
            this.Instancia.Name = "Instancia";
            this.Instancia.ReadOnly = true;
            this.Instancia.Width = 77;
            // 
            // Tiempo
            // 
            this.Tiempo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Tiempo.Frozen = true;
            this.Tiempo.HeaderText = "Tiempo";
            this.Tiempo.MinimumWidth = 100;
            this.Tiempo.Name = "Tiempo";
            this.Tiempo.ReadOnly = true;
            // 
            // Variable
            // 
            this.Variable.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Variable.HeaderText = "Variable";
            this.Variable.MinimumWidth = 150;
            this.Variable.Name = "Variable";
            this.Variable.ReadOnly = true;
            // 
            // Valor
            // 
            this.Valor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Valor.HeaderText = "Valor";
            this.Valor.MinimumWidth = 200;
            this.Valor.Name = "Valor";
            this.Valor.ReadOnly = true;
            this.Valor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Plot
            // 
            this.Plot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Plot.FalseValue = "False";
            this.Plot.HeaderText = "Plot";
            this.Plot.IndeterminateValue = "False";
            this.Plot.MinimumWidth = 50;
            this.Plot.Name = "Plot";
            this.Plot.ReadOnly = true;
            this.Plot.TrueValue = "True";
            this.Plot.Width = 50;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.chart);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dataGridLogPlot);
            this.splitContainer2.Size = new System.Drawing.Size(547, 712);
            this.splitContainer2.SplitterDistance = 416;
            this.splitContainer2.TabIndex = 30;
            // 
            // chart
            // 
            this.chart.BackColor = System.Drawing.Color.LightSteelBlue;
            this.chart.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            this.chart.BackSecondaryColor = System.Drawing.Color.Azure;
            chartArea1.AxisX.LabelStyle.Format = "HH:mm:ss";
            chartArea1.AxisY.IsStartedFromZero = false;
            chartArea1.BackColor = System.Drawing.Color.White;
            chartArea1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chartArea1.BackSecondaryColor = System.Drawing.Color.AliceBlue;
            chartArea1.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea1);
            this.chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "ChartLegend";
            this.chart.Legends.Add(legend1);
            this.chart.Location = new System.Drawing.Point(0, 0);
            this.chart.Name = "chart";
            this.chart.Size = new System.Drawing.Size(547, 416);
            this.chart.TabIndex = 19;
            // 
            // dataGridLogPlot
            // 
            this.dataGridLogPlot.AllowUserToAddRows = false;
            this.dataGridLogPlot.AllowUserToDeleteRows = false;
            this.dataGridLogPlot.AllowUserToOrderColumns = true;
            this.dataGridLogPlot.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridLogPlot.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridLogPlot.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridLogPlot.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridLogPlot.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridLogPlot.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridLogPlot.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridLogPlot.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Instance,
            this.Time,
            this.Parameter,
            this.Value});
            this.dataGridLogPlot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridLogPlot.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridLogPlot.EnableHeadersVisualStyles = false;
            this.dataGridLogPlot.Location = new System.Drawing.Point(0, 0);
            this.dataGridLogPlot.MultiSelect = false;
            this.dataGridLogPlot.Name = "dataGridLogPlot";
            this.dataGridLogPlot.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridLogPlot.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridLogPlot.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridLogPlot.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridLogPlot.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridLogPlot.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridLogPlot.Size = new System.Drawing.Size(547, 292);
            this.dataGridLogPlot.TabIndex = 1;
            // 
            // Instance
            // 
            this.Instance.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Instance.HeaderText = "Instance";
            this.Instance.MinimumWidth = 50;
            this.Instance.Name = "Instance";
            this.Instance.ReadOnly = true;
            this.Instance.Width = 74;
            // 
            // Time
            // 
            this.Time.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Time.HeaderText = "Time";
            this.Time.MinimumWidth = 85;
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.Width = 85;
            // 
            // Parameter
            // 
            this.Parameter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Parameter.HeaderText = "Parameter";
            this.Parameter.MinimumWidth = 150;
            this.Parameter.Name = "Parameter";
            this.Parameter.ReadOnly = true;
            // 
            // Value
            // 
            this.Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Value.HeaderText = "Value";
            this.Value.MinimumWidth = 175;
            this.Value.Name = "Value";
            this.Value.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Valor";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn1.HeaderText = "Valor";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 300;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Valor";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn2.HeaderText = "Valor";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 300;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Valor";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn7.HeaderText = "Valor";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 300;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // enumBinder1
            // 
            this.enumBinder1.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn1.DataSource = this.enumBinder1;
            gridViewComboBoxColumn1.DisplayMember = "Description";
            gridViewComboBoxColumn1.ValueMember = "Value";
            this.enumBinder1.Target = gridViewComboBoxColumn1;
            // 
            // enumBinder2
            // 
            this.enumBinder2.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn2.DataSource = this.enumBinder2;
            gridViewComboBoxColumn2.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn2.DisplayMember = "Description";
            gridViewComboBoxColumn2.FieldName = "IdUnidad";
            gridViewComboBoxColumn2.HeaderText = "IdUnidad";
            gridViewComboBoxColumn2.IsAutoGenerated = true;
            gridViewComboBoxColumn2.Name = "IdUnidad";
            gridViewComboBoxColumn2.ValueMember = "Value";
            this.enumBinder2.Target = gridViewComboBoxColumn2;
            // 
            // enumBinder3
            // 
            this.enumBinder3.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn3.DataSource = this.enumBinder3;
            gridViewComboBoxColumn3.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn3.DisplayMember = "Description";
            gridViewComboBoxColumn3.FieldName = "IdUnidad";
            gridViewComboBoxColumn3.HeaderText = "IdUnidad";
            gridViewComboBoxColumn3.IsAutoGenerated = true;
            gridViewComboBoxColumn3.Name = "IdUnidad";
            gridViewComboBoxColumn3.ValueMember = "Value";
            this.enumBinder3.Target = gridViewComboBoxColumn3;
            // 
            // enumBinder4
            // 
            this.enumBinder4.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn4.DataSource = this.enumBinder4;
            gridViewComboBoxColumn4.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn4.DisplayMember = "Description";
            gridViewComboBoxColumn4.FieldName = "IdUnidad";
            gridViewComboBoxColumn4.HeaderText = "IdUnidad";
            gridViewComboBoxColumn4.IsAutoGenerated = true;
            gridViewComboBoxColumn4.Name = "IdUnidad";
            gridViewComboBoxColumn4.ValueMember = "Value";
            this.enumBinder4.Target = gridViewComboBoxColumn4;
            // 
            // enumBinder5
            // 
            this.enumBinder5.Source = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn5.DataSource = this.enumBinder5;
            gridViewComboBoxColumn5.DataType = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn5.DisplayMember = "Description";
            gridViewComboBoxColumn5.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn5.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn5.IsAutoGenerated = true;
            gridViewComboBoxColumn5.Name = "IdTipoGrupo";
            gridViewComboBoxColumn5.ValueMember = "Value";
            this.enumBinder5.Target = gridViewComboBoxColumn5;
            // 
            // enumBinder6
            // 
            this.enumBinder6.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn6.DataSource = this.enumBinder6;
            gridViewComboBoxColumn6.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn6.DisplayMember = "Description";
            gridViewComboBoxColumn6.FieldName = "IdUnidad";
            gridViewComboBoxColumn6.HeaderText = "IdUnidad";
            gridViewComboBoxColumn6.IsAutoGenerated = true;
            gridViewComboBoxColumn6.Name = "IdUnidad";
            gridViewComboBoxColumn6.ValueMember = "Value";
            this.enumBinder6.Target = gridViewComboBoxColumn6;
            // 
            // enumBinder7
            // 
            this.enumBinder7.Source = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn7.DataSource = this.enumBinder7;
            gridViewComboBoxColumn7.DataType = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn7.DisplayMember = "Description";
            gridViewComboBoxColumn7.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn7.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn7.IsAutoGenerated = true;
            gridViewComboBoxColumn7.Name = "IdTipoGrupo";
            gridViewComboBoxColumn7.ValueMember = "Value";
            this.enumBinder7.Target = gridViewComboBoxColumn7;
            // 
            // enumBinder8
            // 
            this.enumBinder8.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn8.DataSource = this.enumBinder8;
            gridViewComboBoxColumn8.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn8.DisplayMember = "Description";
            gridViewComboBoxColumn8.FieldName = "IdUnidad";
            gridViewComboBoxColumn8.HeaderText = "IdUnidad";
            gridViewComboBoxColumn8.IsAutoGenerated = true;
            gridViewComboBoxColumn8.Name = "IdUnidad";
            gridViewComboBoxColumn8.ValueMember = "Value";
            this.enumBinder8.Target = gridViewComboBoxColumn8;
            // 
            // enumBinder9
            // 
            this.enumBinder9.Source = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn9.DataSource = this.enumBinder9;
            gridViewComboBoxColumn9.DataType = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn9.DisplayMember = "Description";
            gridViewComboBoxColumn9.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn9.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn9.IsAutoGenerated = true;
            gridViewComboBoxColumn9.Name = "IdTipoGrupo";
            gridViewComboBoxColumn9.ValueMember = "Value";
            this.enumBinder9.Target = gridViewComboBoxColumn9;
            // 
            // enumBinder10
            // 
            this.enumBinder10.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn10.DataSource = this.enumBinder10;
            gridViewComboBoxColumn10.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn10.DisplayMember = "Description";
            gridViewComboBoxColumn10.FieldName = "IdUnidad";
            gridViewComboBoxColumn10.HeaderText = "IdUnidad";
            gridViewComboBoxColumn10.IsAutoGenerated = true;
            gridViewComboBoxColumn10.Name = "IdUnidad";
            gridViewComboBoxColumn10.ValueMember = "Value";
            this.enumBinder10.Target = gridViewComboBoxColumn10;
            // 
            // enumBinder11
            // 
            this.enumBinder11.Source = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn11.DataSource = this.enumBinder11;
            gridViewComboBoxColumn11.DataType = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn11.DisplayMember = "Description";
            gridViewComboBoxColumn11.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn11.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn11.IsAutoGenerated = true;
            gridViewComboBoxColumn11.Name = "IdTipoGrupo";
            gridViewComboBoxColumn11.ValueMember = "Value";
            this.enumBinder11.Target = gridViewComboBoxColumn11;
            // 
            // enumBinder12
            // 
            this.enumBinder12.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn12.DataSource = this.enumBinder12;
            gridViewComboBoxColumn12.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn12.DisplayMember = "Description";
            gridViewComboBoxColumn12.FieldName = "IdUnidad";
            gridViewComboBoxColumn12.HeaderText = "IdUnidad";
            gridViewComboBoxColumn12.IsAutoGenerated = true;
            gridViewComboBoxColumn12.Name = "IdUnidad";
            gridViewComboBoxColumn12.ValueMember = "Value";
            this.enumBinder12.Target = gridViewComboBoxColumn12;
            // 
            // enumBinder13
            // 
            this.enumBinder13.Source = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn13.DataSource = this.enumBinder13;
            gridViewComboBoxColumn13.DataType = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn13.DisplayMember = "Description";
            gridViewComboBoxColumn13.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn13.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn13.IsAutoGenerated = true;
            gridViewComboBoxColumn13.Name = "IdTipoGrupo";
            gridViewComboBoxColumn13.ValueMember = "Value";
            this.enumBinder13.Target = gridViewComboBoxColumn13;
            // 
            // enumBinder14
            // 
            this.enumBinder14.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn14.DataSource = this.enumBinder14;
            gridViewComboBoxColumn14.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn14.DisplayMember = "Description";
            gridViewComboBoxColumn14.FieldName = "IdUnidad";
            gridViewComboBoxColumn14.HeaderText = "IdUnidad";
            gridViewComboBoxColumn14.IsAutoGenerated = true;
            gridViewComboBoxColumn14.Name = "IdUnidad";
            gridViewComboBoxColumn14.ValueMember = "Value";
            this.enumBinder14.Target = gridViewComboBoxColumn14;
            // 
            // VariablesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.splitContainer1);
            this.Name = "VariablesView";
            this.Size = new System.Drawing.Size(1190, 712);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridLogPlot)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder1;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder2;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder3;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder4;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder5;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder6;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder7;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder8;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder9;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder10;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder11;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder12;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder13;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder14;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn instanciaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn variableDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn propiedadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn plotDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridView dataGridLogPlot;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Instancia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tiempo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Variable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valor;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Plot;
        private System.Windows.Forms.DataGridViewTextBoxColumn Instance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Parameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
    }
}