﻿using Dezac.Device;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(DisposeModbusDeviceAction))]
    public class DisposedModbusDeviceAction : ActionBase
    {
        [Description("Nombre de la instancia del objecto creado")]
        [DefaultValue("ModbusDevice")]
        [Category("3. Configuration")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "ModbusDevice";
            var uut = GetVariable(name) as ModbusDevice;

            if (uut != null)            
                uut.Dispose();          
        }
    }

    public class DisposeModbusDeviceAction : ActionDescription
    {
        public override string Name { get { return "DisposeModbusDevice"; } }
        public override string Description { get { return "Cierra el puerto modbus serie del objeto especificado"; } }
        public override string Category { get { return "Tools/Comunications/Modbus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
    }

}
