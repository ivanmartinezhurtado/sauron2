﻿using Dezac.Device;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(WriteCoilActionDescription))]
    public class WriteCoil : ActionBase
    {
        [Description("Dreiccion Modbus en decimal")]
        [Category("Configuracion")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ushort Address { get; set; }

        [Description("Nombre de la instancia del objecto creado")]
        [Category("Instancia")]
        public string DeviceName { get; set; }

        [Description("Descripción del nombre de la variable a validar")]
        [Category("Configuracion")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public bool Value { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "ModbusDevice";
            var uut = GetVariable(name) as ModbusDevice;
            var value = Value;
            Context.ResultList.Add(string.Format("{0} - Write 1 Registers type Coil in Address {1} -> {2}", name, Address, value));
            Logger.InfoFormat("{0} - WriteInt32 2 Registers Address {1} -> {2}", name, Address, value);
            uut.WriteSingleCoil(Address, value);
        }
    }

    public class WriteCoilActionDescription : ActionDescription
    {
        public override string Name { get { return "WriteCoil"; } }
        public override string Description { get { return "Escribe un booleano en un registro del tipo Coil"; } }
        public override string Category { get { return "Tools/Comunications/Modbus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
        public override string Dependencies { get { return "InitModbusDeviceAction;DisposedModbusDeviceAction:2"; } }
    }
}
