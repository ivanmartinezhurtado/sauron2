using System.IO;
using TaskRunner.Model;

namespace WinTestPlayer.Services
{
    public class SequenceDocument
    {
        public SequenceModel Model { get; private set; }
        public string FileName { get; private set; }

        public SequenceDocument()
        {
            AddNew();
        }

        public SequenceModel AddNew()
        {
            Model = SequenceModel.CreateNew();
            FileName = null;

            return Model;
        }

        public void Load(string fileName)
        {
            Model = SequenceModel.LoadFile(fileName);
            FileName = fileName;
        }

        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(FileName))
                    return "Untitled";

                return Path.GetFileNameWithoutExtension(FileName);
            }
        }

        public bool IsNew
        {
            get { return FileName == null; }
        }

        public void Save()
        {
            Model.Save(FileName);
        }

        public void Save(string fileName)
        {
            FileName = fileName;
            Model.Save(FileName);
        }
    }
}
