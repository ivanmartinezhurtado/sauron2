﻿using Comunications.Ethernet;
using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Dezac.Device.Metering.SOAP;
using FileHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.04)]
    public class COMPACT_DC : DeviceBase
    {
        private readonly static CultureInfo enUS = new CultureInfo("en-US");

        private CompactDctSoapService _soap;

        private SSHClient _ssh;

        private Ports puertos;

        private Modem modem;

        private UART uart;

        public string HostName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string PrivateKeyFile { get; set; }

        public string XmlVersion { get; set; }

        [JsonIgnore]
        protected SSHClient Ssh
        {
            get
            {
                if (_ssh == null)
                {
                    if (string.IsNullOrEmpty(PrivateKeyFile))
                        _ssh = new SSHClient(HostName, UserName, Password, _logger);
                    else                  
                        _ssh = new SSHClient(HostName, UserName, Password, _logger, PrivateKeyFile);
                    
                }

                return _ssh;
            }
        }

        [JsonIgnore]
        public CompactDctSoapService Soap
        {
            get
            {
                if (_soap == null)
                    _soap = new CompactDctSoapService(XmlVersion);

                return _soap;
            }
        }

        [JsonIgnore]
        public Ports Puertos
        {
            get
            {
                if (puertos == null)
                    puertos = new Ports(Ssh);

                return puertos;
            }
        }

        [JsonIgnore]
        public UART Uart
        {
            get
            {
                if (uart == null)
                    uart = new UART(Ssh);

                return uart;
            }
        }

        [JsonIgnore]
        public Modem Modem3G
        {
            get
            {
                if (modem == null)
                    modem = new Modem(Ssh);

                return modem;
            }
        }

        public COMPACT_DC()
        {
        }

        public COMPACT_DC(string version)
        {
            this.HostName = "100.0.0.1";
            this.UserName = "root";
            this.Password = "j7awg54q";
            this.XmlVersion = version;
        }

        public COMPACT_DC(string hostname, string username, string password, string version)
        {
            this.HostName = hostname;
            this.UserName = username;
            this.Password = password;
            this.XmlVersion = version;
        }

        public void KillConcentratorAplication(string concentrator = "concentrator")
        {
            LogMethod();
            Ssh.Run(string.Format("killall {0}", concentrator), false);
        }

        public void RunConcentratorAplication()
        {
            LogMethod();
            Ssh.Run("concentrator &");
        }

        public enum typePort
        {
            Input,
            Output
        }

        public enum statusPort
        {
            On,
            Off
        }

        public statusPort ReadStatusResetButton()
        {
            LogMethod();
            return Puertos.ReadInputsStatus(Ports.DefinePortsInputs.IN_SOFT_RESET);
        }

        public class Memory
        {
            public string Filesystem { get; set; }
            public string Size { get; set; }
            public string Used { get; set; }
        }

        public List<Memory> StatusInternalMemory()
        {
            LogMethod();
            var memory = Ssh.Run("df -h").Split('\n'); //.Where((p) => p != "").ToArray();
            var memoryList = new List<Memory>();      
            foreach (string row in memory)
            {
                var file = row.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);

                if (file.Count() != 0)
                    memoryList.Add(new Memory()
                    {
                        Filesystem = file[0],
                        Size = file[1],
                        Used = file[2],
                    });
            }
            return memoryList;
        }

        public int GetSizeMemoryRAM()
        {
            LogMethod();
            return Convert.ToInt32(Ssh.Run("free | grep Mem: | awk '{print $2}'"));
        }

        public string StatusExternalMemory(string file)
        {
            LogMethod();
            var result = Ssh.Run(string.Format("cat /tmp/{0} | grep \"Last shut down\"",file));
            var sentence = "Last shut down at";

            if (string.IsNullOrEmpty(result))
                throw new Exception("Error no se ha leido nada del log.txt con el grep 'Last shut down'");

            var index = result.LastIndexOf(sentence);

            result = result.Substring(index).Replace(sentence, "");

            return result.Replace("\n", "");
        }

        public double VerificationTemperature()
        {
            LogMethod();
            var result = Ssh.Run("cat /sys/bus/iio/devices/iio:device0/in_voltage7_raw");

            if (string.IsNullOrEmpty(result))
                throw new Exception("Error no se ha leido nada del log.txt con el grep 'Last shut down'");

            var adcval = Convert.ToDouble(result.Replace("\n", ""));

            var temperature = (5.05e-9 * (adcval * adcval * adcval)) - (2.546e-5 * (adcval * adcval)) + (5.986e-2 * adcval) - 45.37;

            return temperature;
        }

        public void DeleteListNodes(string folder = "db")
        {
            LogMethod();
            Ssh.Run(String.Format("rm /data/{0}/nodes.sqlite3",folder), false);
            Ssh.Run("sync");
        }

        public void DeleteModemConfiguration()
        {
            LogMethod();
            Ssh.Run("rm /etc/modem.conf");
            Ssh.Run("sync");
        }

        public List<PLCCommunicationsQuality> ReadPLCCommunicationsQuality()
        {
            LogMethod();

            // FileHelper es un paquete de nugget que permite procesar facilmente archivos CSV
            var csvReader = new FileHelperEngine<PLCCommunicationsQuality>();
            // Configuro para que en caso de que de un error porque la longitud de datos o el formato de los mismos sea incorrecto, lo ignore y siga adelante
            csvReader.ErrorMode = ErrorMode.IgnoreAndContinue;

            // No uso un Statisctical List porque me da un error al intentar hacer un add, en la excepción, lo unico que leo es "error", por lo que no puedo saber que esta pasando
            var outgoingCommunications = new List<PLCCommunicationsQuality>();
            //var amountOfColumns = Ssh.Run("head /tmp/sniffer.csv -n 1").Replace("\r", string.Empty).Split('\n').First().Split(',').Count();
            var communications = Ssh.Run("tail /tmp/sniffer.csv -n 20").Replace(",\r", string.Empty).Split('\n');
            var communicationEvents = communications.Where(val => val.Contains("UP") & val.Contains(",-")).ToArray();

            if (communications.Length > 10)
            {
                foreach (string currentEvent in communicationEvents)
                //if (currentEvent.Split(',').Count().Equals(amountOfColumns))
                    outgoingCommunications.Add(csvReader.ReadString(currentEvent).First());

                //return outgoingCommunications.Average((p) => Convert.ToDouble(p.PHYLevel, new CultureInfo("en-US")));
                return outgoingCommunications;
            }
            else
                return null;           
        }

        public string ReadMac()
        {
            LogMethod();
            //var amountOfColumns = Ssh.Run("head /tmp/sniffer.csv -n 1").Replace("\r", string.Empty).Split('\n').First().Split(',').Count();
            var communications = Ssh.Run("cat /tmp/sniffer.csv").Replace(",\r", string.Empty).Split('\n');
            var macArray = communications.Where(val => val.Contains("EUI48:")).ToList().FirstOrDefault();
            var macSubstring = macArray.Substring(macArray.IndexOf("EUI48:")).Replace("EUI48:","").Trim();
            if (string.IsNullOrEmpty(macSubstring))
                throw new Exception("Error no e ha obtenido Numero de MAC del fichero sniffer.csv");

            return macSubstring.Substring(0,macSubstring.IndexOf(" ")).Trim().ToUpper();
        }

        public void DeleteSnifferFile()
        {
            LogMethod();
            Ssh.Run("cat /dev/null > /tmp/sniffer.csv");
        }

        public enum Client
        {
            GENERAL = 0,
            FENOSA = 1,
            IBERDROLA = 2
        }

        [DelimitedRecord(",")]
        public class PLCCommunicationsQuality
        {
            public string PCTime;       
            public string Time;        
            public string DeltaTime;         
            public string Htype;          
            public string DO;        
            public string LEVEL;         
            public string HCS;        
            public string SID;      
            public string LNID;          
            public string PRIO;       
            public string Len;        
            public string CRC;     
            public string Txt;     
            public string Scheme;      
            public string PHYLevel;         
            public string PHYSNR;      
            public string PHYRQ;
        }
        
        public class Modem
        {
            private SSHClient Ssh;

            public Modem(SSHClient ssh)
            {
                Ssh = ssh;
            }

            public class configurationSIM
            {
                public string Pin { get; set; }
                public string Apn { get; set; }
                public string User { get; set; }
                public string Password { get; set; }
                public byte Authentication { get; set; }
                public byte Mode { get; set; }
            }

            public void ConfigurationServiceSIM(configurationSIM sim)
            {
                var config = new StringBuilder();
                config.AppendFormat("sim1pin={0}\n", sim.Pin);
                config.AppendFormat("sim1apn={0}\n", sim.Apn);
                config.AppendFormat("sim1user={0}\n", sim.User);
                config.AppendFormat("sim1password={0}\n", sim.Password);
                config.AppendFormat("sim1authentication={0}\n", sim.Authentication);
                config.AppendFormat("sim1mode={0}",sim.Mode);

                Ssh.Run("printf \"" + config.ToString() + "\" > /etc/modem.conf");
            }

            public void RestartService()
            {
                Ssh.Run("/etc/init.d/modem.sh restart");
            }

            public class modemStatus
            {
                public string timeConnected { get; set; }
                public string ipAssigned { get; set; }
                public string typeConnection { get; set; }
                public string csq { get; set; }
                public string imsi { get; set; }
                public string idCell { get; set; }
                public string activeSim { get; set; }
                public SimStatus statusSim1 { get; set; }
                public SimStatus statusSim2 { get; set; }
                public string txBytes { get; set; }
                public string rxBytes { get; set; }
                public string manufacturer { get; set; }
                public string moddel { get; set; }
                public string version { get; set; }
                public string imei { get; set; }
                public string IP { get { return ipAssigned.Replace("ipAssigned=", "").Replace("\n",""); } }
            }

            public enum SimStatus
            {
                SIM_STATUS_UNKNOWN = 0,
                SIM_STATUS_READY,
                SIM_STATUS_NOT_DETECTED,
                SIM_STATUS_PIN_INVALID,
                SIM_STATUS_PUK_REQUIRED,
            }

            public modemStatus ReadModemStatus()
            {
                var response = Ssh.Run("cat /tmp/modemStatus").Split('\n').ToArray(); ;

                var modemStatus = new modemStatus();

                modemStatus.timeConnected = response.Where<string>(x => x.Contains("timeConnected=")).FirstOrDefault().Replace("timeConnected=","").Replace("\r","");
                modemStatus.ipAssigned = response.Where<string>(x => x.Contains("ipAssigned=")).FirstOrDefault().Replace("ipAssigned=", "").Replace("\r", "");
                modemStatus.typeConnection = response.Where<string>(x => x.Contains("typeConnection=")).FirstOrDefault().Replace("typeConnection=", "").Replace("\r", "");
                modemStatus.csq = response.Where<string>(x => x.Contains("csq=")).FirstOrDefault().Replace("csq=", "").Replace("\r", "");
                modemStatus.imsi = response.Where<string>(x => x.Contains("imsi=")).FirstOrDefault().Replace("imsi=", "").Replace("\r", "");
                modemStatus.idCell = response.Where<string>(x => x.Contains("idCell=")).FirstOrDefault().Replace("idCell=", "").Replace("\r", "");
                modemStatus.activeSim = response.Where<string>(x => x.Contains("activeSim=")).FirstOrDefault().Replace("activeSim=", "").Replace("\r", "");
                modemStatus.statusSim1 = (SimStatus)Convert.ToByte(response.Where<string>(x => x.Contains("statusSim1=")).FirstOrDefault().Replace("statusSim1=", "").Replace("\r", ""));
                modemStatus.statusSim2 = (SimStatus)Convert.ToByte(response.Where<string>(x => x.Contains("statusSim2=")).FirstOrDefault().Replace("statusSim2=", "").Replace("\r", ""));
                modemStatus.txBytes = response.Where<string>(x => x.Contains("txBytes=")).FirstOrDefault().Replace("txBytes=", "").Replace("\r", "");
                modemStatus.rxBytes = response.Where<string>(x => x.Contains("rxBytes=")).FirstOrDefault().Replace("rxBytes=", "").Replace("\r", "");
                modemStatus.manufacturer = response.Where<string>(x => x.Contains("manufacturer=")).FirstOrDefault().Replace("manufacturer=", "").Replace("\r", "");
                modemStatus.moddel = response.Where<string>(x => x.Contains("model=")).FirstOrDefault().Replace("model=", "").Replace("\r", "");
                modemStatus.version = response.Where<string>(x => x.Contains("version=")).FirstOrDefault().Replace("version=", "").Replace("\r", "");
                modemStatus.imei = response.Where<string>(x => x.Contains("imei=")).FirstOrDefault().Replace("imei=", "").Replace("\r", "");

                return modemStatus;
            }
        } 

        public class Ports
        {
            private SSHClient Ssh;

            public enum DefinePortsOutputs
            {
                OUT_NOT_USED1 = 81,      // Not used         (PIN 1 - GPIO2_17)
                OUT_NOT_USED2 = 8,       // Not used         (PIN 2 - GPIO0_8)
                OUT_LED_PLC_LINK = 9,    // LED PLC link     (PIN 3 - GPIO0_9)
                OUT_LED_POWER = 10,      // LED power        (PIN 4 - GPIO0_10)
                OUT_LED_SBT_LINK = 11,   // LED SBT link     (PIN 5 - GPIO0_11)
                OUT_WATCHDOG = 75,       // Watchdog         (PIN 6 - GPIO2_11)
                OUT_LED_MODEM_LINK = 78, // LED modem link   (PIN 11 - GPIO2_14)
                OUT_NOT_USED3 = 79,      // Not used         (PIN 12 - GPIO2_15)
                OUT_RS485_ENTX = 80,     // OUT_NOT_USED4    (PIN 13 - GPIO2_16)
                OUT_GLOBAL_RESET = 71,   // Global reset     (PIN 14 - GPIO2_7)
                OUT_GLOBAL_SUPPLY = 70,  // Global supply    (PIN 15 - GPIO2_6)
                //OUT_NOT_USED5 = 73,      // Not used         (PIN 16 - GPIO2_9)
                //OUT_NOT_USED6 = 72,      // Not used         (PIN 17 - GPIO2_8)
                //OUT_NOT_USED7 = 74,      // Not used         (PIN 19 - GPIO2_10)

                OUT_SBT_SWITCH = 65,     // Select SBT       (PIN 76 - GPIO2_1)
                //OUT_NOT_USED8 = 45,      // Not used         (PIN 85 - GPIO1_13)
                //OUT_NOT_USED9 = 44,      // Not used         (PIN 86 - GPIO1_12)
                //OUT_NOT_USED10 = 103,    // Not used         (PIN 111 - GPIO3_7)
                //OUT_NOT_USED11 = 20,     // Not used         (PIN 135 - GPIO0_20)
                //OUT_NOT_USED12 = 6,      // Not used         (PIN 171 - GPIO0_6)
                //OUT_NOT_USED13 = 7,      // Not used         (PIN 173 - GPIO0_7)
            }

            public enum DefinePortsInputs
            {
                IN_SLOT_ID0 = 87,        // Slot ID0         (PIN 20 - GPIO2_23)
                IN_SLOT_ID1 = 89,        // Slot ID1         (PIN 21 - GPIO2_25)
                IN_SLOT_ID2 = 88,        // Slot ID2         (PIN 22 - GPIO2_24)
                IN_SLOT_ID3 = 64,        // Slot ID3         (PIN 47 - GPIO2_0)
                IN_PG = 100,             // Power good       (PIN 72 - GPIO3_4)
                IN_INTERRUPT = 47,       // Interrupt        (PIN 87 - GPIO1_15)
                IN_SOFT_RESET = 46,      // Soft reset       (PIN 88 - GPIO1_14)
                IN_PLC_MONITOR = 104,    // PLC monitor      (PIN 160 - GPIO3_8)
                IN_PLC_SECONDARY = 102,  // PLC secondary    (PIN 172 - GPIO3_6)
            }

            public Ports(SSHClient ssh)
            {
                Ssh = ssh;
            }

            public void ConfigurationAllPorts()
            {
                foreach (DefinePortsInputs port in Enum.GetValues(typeof(DefinePortsInputs)))
                {
                    ActivePort((byte)port);
                    ConfigPort(typePort.Input, (byte)port);

                    if (!VerificationTypePort(typePort.Input, (byte)port))
                        throw new Exception(string.Format("Error no se ha podido configurar el Puerto {0} como entrada", port.ToString()));

                }

                foreach (DefinePortsOutputs port in Enum.GetValues(typeof(DefinePortsOutputs)))
                {
                    ActivePort((byte)port);
                    ConfigPort(typePort.Output,(byte)port);

                    if (!VerificationTypePort(typePort.Output, (byte)port))
                        throw new Exception(string.Format("Error no se ha podido configurar el Puerto {0} como salida", (byte)port));
                }
            }

            public void ActivePort(byte port)
            {
                Ssh.Run("echo " + port + " > /sys/class/gpio/export");
            }

            public void ConfigPort(typePort type, byte port)
            {
                var function = type == typePort.Output ? "out" : "in";

                Ssh.Run("echo " + function + " > /sys/class/gpio/gpio" + port + "/direction");
            }

            public bool VerificationTypePort(typePort type, byte port)
            {
                var valueType = Ssh.Run("cat /sys/class/gpio/gpio" + port + "/direction").CRoNetString();

                typePort typePort = valueType == "in" ? COMPACT_DC.typePort.Input : COMPACT_DC.typePort.Output;

                if (typePort == type)
                    return true;

                return false;
            }

            public void Off(DefinePortsOutputs port)
            {
                Ssh.Run("echo 0 > /sys/class/gpio/gpio" + (byte)port + "/value");
            }

            public void On(DefinePortsOutputs port)
            {
                Ssh.Run("echo 1 > /sys/class/gpio/gpio" + (byte)port + "/value");
            }

            public bool VerificationOutputStatus(statusPort status, DefinePortsOutputs port)
            {
                var value = Ssh.Run("cat /sys/class/gpio/gpio" + (byte)port + "/value").CRoNetString();

                bool Status = status == statusPort.On ? true : false;

                bool statusRead = value.Trim() == "1" ? true : false;

                return Status == statusRead;
            }

            public bool VerificationInputsStatus(statusPort status, DefinePortsInputs port)
            {
                var value = Ssh.Run("cat /sys/class/gpio/gpio" + (byte)port + "/value").CRoNetString();

                bool Status = status == statusPort.On ? true : false;

                bool statusRead = value.Trim() == "1" ? true : false;

                return Status == statusRead;
            }

            public statusPort ReadInputsStatus(DefinePortsInputs port)
            {
                var value = Ssh.Run("cat /sys/class/gpio/gpio" + (byte)port + "/value").CRoNetString();

               statusPort status;

               status = value.Trim() == "1" ? statusPort.On : statusPort.Off;

               return status;
            }
        }

        public class UART
        {
            private SSHClient Ssh;

            protected MessageTransport transport;

            protected IProtocol protocol;

            private SerialPort sp;

            private byte _port = 12;

            public enum defineUART
            {
                UART1_SBT,
                UART2_PLC,
                UART4_RS485,
                USB1
            }

            public UART(SSHClient ssh)
            {
                Ssh = ssh;
            }

            public void Active_UART(defineUART uart)
            {
                switch (uart)
                {
                    case defineUART.UART1_SBT:
                        Ssh.Run("~# microcom /dev/ttyO1");
                        break;
                    case defineUART.UART2_PLC:
                        Ssh.Run("~# microcom /dev/ttyO2");
                        break;
                    case defineUART.UART4_RS485:
                        Ssh.Run("~# microcom /dev/ttyO4");
                        break;
                    case defineUART.USB1:
                        Ssh.Run("~# microcom /dev/ttyUSB1");
                        break;
                }
            }

            public void Desactive_UART()
            {
                Ssh.Run("~# microcom exit");
            }

            public void SerialPortConfigure()
            {
                if (sp == null)
                    sp = new SerialPort();

                //sets the read and write timeout to 3 seconds
                sp.WriteTimeout = sp.ReadTimeout = 3000;
                sp.BaudRate = 9600;
                sp.StopBits = StopBits.One;
                sp.Parity = Parity.None;
                sp.DataBits = 8;
                sp.PortName = "COM" + _port.ToString();

                if (protocol == null)
                    protocol = new ASCIIProtocol() { CaracterFinTx = "\r", CaracterFinRx = "\r" };

                if (transport == null)
                    transport = new MessageTransport(protocol, new SerialPortAdapter(sp), _logger);
            }

            public byte SerialPort
            {
                get { return _port; }
                set
                {
                    if (_port == value)
                        return;

                    _port = value;
                    if (sp.IsOpen)
                    {
                        sp.Close();
                        sp.PortName = "COM" + _port;
                        sp.Open();
                    }
                    else
                        sp.PortName = "COM" + _port;
                }
            }

            public void ExecuteCommand(string command, int numRetries)
            {
                bool error = false;
                StringMessage message;
                do
                {
                    try
                    {
                        _logger.InfoFormat("Command {0}", command);
                        error = false;
                        message = StringMessage.Create(enUS, command);
                        message.Validator = (m) => { return m.Text == command; };
                        var response = transport.SendMessage<StringMessage>(message);
                    }
                    catch (Exception ex)
                    {
                        error = true;
                        numRetries--;

                        _logger.ErrorFormat("{0}, retry: {1} -> {2}", ex.GetType().Name, numRetries, ex.Message);

                        if (numRetries <= 0 || ex is NotSupportedException)
                            throw;
                    }
                } while (error);
            }

            public void Dispose()
            {
                if (sp != null)
                    sp.Dispose();
            }
        }

        public void CloseConectionSsH()
        {
            LogMethod();

            if (_ssh != null)
            {
                _ssh.Close();
                _ssh.Dispose();
            }
        }

        public override void Dispose()
        {
            CloseConectionSsH();
        }
    }
}
