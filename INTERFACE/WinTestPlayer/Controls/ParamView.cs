﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dezac.Tests.Core.Services;
using WinTestPlayer.Utils;
using System.IO;

namespace WinTestPlayer.Controls
{
    public partial class ParamView : UserControl
    {
        TestContextModel model = null;
            
        public ParamView()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            try
            {
                model = AppProfile.DeserializeFile<TestContextModel>(Path.Combine(AppProfile.ProgramPath, "data.json"));

                LoadData();
            } catch(Exception ex)
            {

            }
        }

        private void OnRefreshData(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            if (model == null)
                return;

            if (rbComs.Checked)
                bsParams.DataSource = model.Comunicaciones.Values;
            else if (rbConfs.Checked)
                bsParams.DataSource = model.Configuracion.Values;
            else if (rbCons.Checked)
                bsParams.DataSource = model.Consignas.Values;
            else if (rbIden.Checked)
                bsParams.DataSource = model.Identificacion.Values;
            else if (rbMarg.Checked)
                bsParams.DataSource = model.Margenes.Values;
            else if (rbRes.Checked)
                bsParams.DataSource = model.Resultados.Values;
        }
    }
}
