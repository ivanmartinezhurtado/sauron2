﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Dezac.Tests.Visio.DTO;
using log4net;
using System.Threading;

namespace Dezac.Tests.Visio
{
    public class Halcon
    {
        private static ILog logger = LogManager.GetLogger("Halcon");

        public static HalconCallResult CallProcedure(string procedure, List<ImageStruct> listImages, Dictionary<string, object> parameters)
        {
            var images = new Dictionary<string,byte[]>();

            foreach (var image in listImages)
                using (var stream = new MemoryStream())
                {                 
                    image.Image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                    images.Add(image.ImageName, stream.ToArray());
                }

            return CallProcedure(procedure, images, parameters);
        }
        private static HalconCallResult CallProcedure(string procedure, Dictionary<string, byte[]> images, Dictionary<string, object> parameters)
        {
            int retry = 0;
            HalconCallResult result;
            do
            {
                using (var client = new HttpClient())
                {
                    using (var content = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString()))
                    {
                        foreach (KeyValuePair<string, byte[]> image in images)
                            content.Add(new StreamContent(new MemoryStream(image.Value)), image.Key, image.Key);

                        if (parameters != null)
                            foreach (var item in parameters)
                                content.Add(new StringContent(item.Value != null ? item.Value.ToString() : null), item.Key);

                        string url = $"{ConfigurationManager.AppSettings["UrlVisionServer"]}/api/halcon/procedure?name={procedure}";

                        client.Timeout = new TimeSpan(0, 10, 0);
                        HttpResponseMessage message = null;
                        result = null;

                        logger.Debug("Sending POST request to HalconService");
                        try 
                        {
                            message = client.PostAsync(url, content).Result;
                            message.EnsureSuccessStatusCode();
                            var json = message.Content.ReadAsStringAsync().Result;
                            result = JsonConvert.DeserializeObject<HalconCallResult>(json);
                            if (result.Success)
                                return result;
                            logger.Warn($"Exception in the POST request: {result.ErrorMessage}");
                            logger.Debug("Request will be retried in 1 seconds");
                            Thread.Sleep(1000);
                        }
                        catch (Exception ex)
                        {
                            logger.Warn($"Exception in the POST request: {ex}");
                            logger.Debug("Request will be retried in 1 seconds");
                            Thread.Sleep(1000);
                        }
                    }
                }
            } while (retry++ < 2);
            return result;
        }
    }
}
