﻿using System.Threading;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.01)]
    public class VTN : DeviceDlmsBase
    {
        public void WriteVoltageGain(ushort voltageGain)
        {
            LogMethod();
            Dlms.WriteData<ushort>(OBIS_VOLTAGE_GAIN[0], 2, voltageGain);
            Thread.Sleep(3000);
        }

        public ushort ReadVoltageGain()
        {
            LogMethod();
            return Dlms.ReadData<ushort>(OBIS_VOLTAGE_GAIN[0], 2);
        }

        public double ReadVoltage()
        {
            LogMethod();
            return Dlms.ReadRegister<double>("1.0.92.128.0.255", 2) / 10d;
        }

        public ushort CalculateVoltageGain(double measures, double voltageReference)
        {
            var oldGain = ReadVoltageGain();
            return (ushort)(voltageReference *  oldGain / measures);
        }
    }
}
