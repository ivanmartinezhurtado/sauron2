﻿using Comunications.Utility;
using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.03)]
    public class CEMC20 : CEMBase
    {
        public CEMC20()
            :base()
        {
        }

        public CEMC20(int port, int baudRate)
            : base(port, baudRate)
        {
        }

        public CEMC20(string hostname, int port)
            : base(hostname, port)
        {
        }

        public void WriteDisplay(DisplayConfigurations value)
        {
            Modbus.Write<Display>(displayConfigurations[value]);
        }

        public TriLineGain ReadGainGap()
        {
            return base.ReadGainGap<TriLineGain>();
        }

        public void WriteGainsGap(int GainL1, int GainL2, int GainL3)
        {
            var value = new CEMBase.TriLineGain() { L1 = (ushort)GainL1, L2 = (ushort)GainL2, L3 = (ushort)GainL3 };
            WriteGainsGap(value);
        }

        public void WriteGainsGap(TriLineGain value)
        {
            base.WriteGainsGap(value);
        }
    
        public TriLineGain ReadGainVoltage()
        {
            return base.ReadGainVoltage<TriLineGain>();
        }

        public void WriteGainsVoltage(int GainL1, int GainL2, int GainL3)
        {
            var value = new CEMBase.TriLineGain() { L1 = (ushort)GainL1, L2 = (ushort)GainL2, L3 = (ushort)GainL3 };
            WriteGainsGap(value);
        }

        public void WriteGainsVoltage(TriLineGain value)
        {
            base.WriteGainsVoltage(value);
        }

        public TriLineGain ReadGainActive()
        {
            return base.ReadGainActive<TriLineGain>();
        }

        public TriLineGain ReadGainReactive()
        {
            return base.ReadGainReactive<TriLineGain>();
        }

        public void WriteGainsActive(TriLineGain value)
        {
            base.WriteGainsActive(value);
        }

        public void WriteGainsReactive(TriLineGain value)
        {
            base.WriteGainsReactive(value);
        }

        public TriLineMeasure ReadVoltage()
        {
            return base.ReadVoltage<TriLineMeasure>();
        }

        public TriLineMeasure ReadCurrent()
        {
            return base.ReadCurrent<TriLineMeasure>();
        }

        public TriLineValue ReadPowerActiveHiResolution()
        {
            var points = ReadPointsActive();
            var gain = ReadGainActive();
            var offset = ReadOffsetActive();
            TriLineValue result = new TriLineValue();
            result.L1 = (double)(points.L1 - offset.L1) / (double)gain.L1;
            result.L2 = (double)(points.L2 - offset.L2) / (double)gain.L2;
            result.L3 = (double)(points.L3 - offset.L3) / (double)gain.L3;
            return result;
        }

        public TriLineValue ReadPowerReactiveHiResolution()
        {
            var points = ReadPointsReactive();
            var gain = ReadGainActive();
            var offset = ReadOffsetReactive();
            TriLineValue result = new TriLineValue();
            result.L1 = (double)(points.L1 - offset.L1) / (double)gain.L1;
            result.L2 = (double)(points.L2 - offset.L2) / (double)gain.L2;
            result.L3 = (double)(points.L3 - offset.L3) / (double)gain.L3;
            return result;
        }

        public TriLineMeasure ReadPointsVoltage()
        {
            return base.ReadPointsVoltage<TriLineMeasure>();
        }

        public TriLineMeasure ReadPointsActive()
        {
            return base.ReadPointsActive<TriLineMeasure>();
        }

        [Browsable(false)]
        public TriLineMeasure ReadPointsReactive()
        {
            return base.ReadPointsReactive<TriLineMeasure>();
        }

        public TriLineMeasure ReadOffsetActive()
        {
            return base.ReadOffsetActive<TriLineMeasure>();
        }

        public TriLineMeasure ReadOffsetReactive()
        {
            return base.ReadOffsetReactive<TriLineMeasure>();
        }

        public void WriteGainOffsetActive(TriLineMeasure value)
        {
            base.WriteGainOffsetActive(value);
        }

        public void WriteGainOffsetReactive(TriLineMeasure value)
        {
            base.WriteGainOffsetReactive(value);
        } 

        //***************************************************

        [Browsable(false)]
        public Tuple<bool, TriLineGain> CalculateGapAdjustMeasureFactors(int delFirst, int initCount, int samples, int interval, TriLineValue anguloDesfase, Func<TriLineGain, bool> adjustValidation, Func<TriLineGain, bool> sampleValidation = null)
        {
            TriLineGain resultadoAjuste = new TriLineGain();

            var gainGapCurrent = ReadGainGap();
            short gainGapL1 = (short)gainGapCurrent.L1;
            short gainGapL2 = (short)gainGapCurrent.L2;
            short gainGapL3 = (short)gainGapCurrent.L3;

            var factorCalibration = Frequency == TypeMeterConection.frequency._50Hz ? -22.2 : -18.5;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var pointsActive = ReadPointsActive();
                    var pointsReactive = ReadPointsReactive();
                    var lecturas = new List<double>();
                    lecturas.Add(pointsActive.L1);
                    lecturas.Add(pointsReactive.L1);
                    lecturas.Add(pointsActive.L2);
                    lecturas.Add(pointsReactive.L2);
                    lecturas.Add(pointsActive.L3);
                    lecturas.Add(pointsReactive.L3);
                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    var anguloCalculado = (1.5707963267949 - Math.Atan(listValues.Average(0) / listValues.Average(1))).ToDegree();
                    var errorCalculado = factorCalibration * (anguloDesfase.L1 - anguloCalculado);
                    resultadoAjuste.L1 = (ushort)(gainGapL1 + errorCalculado);

                    anguloCalculado = (1.5707963267949 - Math.Atan(listValues.Average(2) / listValues.Average(3))).ToDegree();
                    errorCalculado = factorCalibration * (anguloDesfase.L2 - anguloCalculado);
                    resultadoAjuste.L2 = (ushort)(gainGapL2 + errorCalculado);

                    anguloCalculado = (1.5707963267949 - Math.Atan(listValues.Average(4) / listValues.Average(5))).ToDegree();
                    errorCalculado = factorCalibration * (anguloDesfase.L3 - anguloCalculado);
                    resultadoAjuste.L3 = (ushort)(gainGapL3 + errorCalculado);

                    return adjustValidation(resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }

        [Browsable(false)]
        public Tuple<bool, TriLineGain> CalculateVoltageAdjustMeasureFactors(int delFirst, int initCount, int samples, int interval, TriLineValue voltage, Func<TriLineGain, bool> adjustValidation, Func<TriLineGain, bool> sampleValidation = null)
        {
            TriLineGain resultadoAjuste = new TriLineGain();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var points = ReadPointsVoltage();
                    var lecturas = new List<double>();
                    lecturas.Add(points.L1);
                    lecturas.Add(points.L2);
                    lecturas.Add(points.L3);
                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    resultadoAjuste.L1 = (ushort)(Math.Sqrt(listValues.Average(0)) / (voltage.L1 / 100));
                    resultadoAjuste.L2 = (ushort)(Math.Sqrt(listValues.Average(1)) / (voltage.L2 / 100));
                    resultadoAjuste.L3 = (ushort)(Math.Sqrt(listValues.Average(2)) / (voltage.L3 / 100));
                    return adjustValidation(resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }

        [Browsable(false)]
        public Tuple<bool, TriLineGain> CalculatePowerMeasureFactors(int delFirst, int initCount, int samples, int interval, TriLineValue power, bool IsReactive, Func<TriLineGain, bool> adjustValidation, Func<TriLineGain, bool> sampleValidation = null)
        {
            TriLineGain resultadoAjuste = new TriLineGain();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var points = IsReactive == false ? ReadPointsActive() : ReadPointsReactive();
                    var lecturas = new List<double>();
                    lecturas.Add(points.L1);
                    lecturas.Add(points.L2);
                    lecturas.Add(points.L3);
                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    resultadoAjuste.L1 = (ushort)(listValues.Average(0) / power.L1);
                    resultadoAjuste.L2 = (ushort)(listValues.Average(1) / power.L2);
                    resultadoAjuste.L3 = (ushort)(listValues.Average(2) / power.L3);
                    return adjustValidation(resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }

        [Browsable(false)]
        public Tuple<bool, TriLineMeasure> CalculateOffsetFactors(int delFirst, int initCount, int samples, int interval, TriLineValue power, bool IsReactive, Func<TriLineMeasure, bool> adjustValidation, Func<TriLineMeasure, bool> sampleValidation = null)
        {
            TriLineMeasure resultadoAjuste = new TriLineMeasure();

            var gainPower = IsReactive == false ? ReadGainActive() : ReadGainReactive();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var points = IsReactive == false ? ReadPointsActive() : ReadPointsReactive();
                    var lecturas = new List<double>();
                    lecturas.Add(points.L1);
                    lecturas.Add(points.L2);
                    lecturas.Add(points.L3);
                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    resultadoAjuste.L1 = (int)(((listValues.Average(0) / gainPower.L1) - power.L1) * gainPower.L1);
                    resultadoAjuste.L2 = (int)(((listValues.Average(1) / gainPower.L2) - power.L2) * gainPower.L2);
                    resultadoAjuste.L3 = (int)(((listValues.Average(2) / gainPower.L3) - power.L3) * gainPower.L3);

                    return adjustValidation(resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }


        /*ADJUST ACTIONS*/
        public TriLineMeasure CalculateGapAdjustMeasureFactors(int delFirst, int initCount, int samples, int interval, TriLineValue anguloDesfase)
        {
            TriLineMeasure resultadoAjuste = new TriLineMeasure();

            var gainGapCurrent = ReadGainGap();
            short gainGapL1 = (short)gainGapCurrent.L1;
            short gainGapL2 = (short)gainGapCurrent.L2;
            short gainGapL3 = (short)gainGapCurrent.L3;

            var factorCalibration = Frequency == TypeMeterConection.frequency._50Hz ? -22.2 : -18.5;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var pointsActive = ReadPointsActive();
                    var pointsReactive = ReadPointsReactive();
                    var lecturas = new List<double>();
                    lecturas.Add(pointsActive.L1);
                    lecturas.Add(pointsReactive.L1);
                    lecturas.Add(pointsActive.L2);
                    lecturas.Add(pointsReactive.L2);
                    lecturas.Add(pointsActive.L3);
                    lecturas.Add(pointsReactive.L3);
                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    var anguloCalculado = (1.5707963267949 - Math.Atan(listValues.Average(0) / listValues.Average(1))).ToDegree();
                    var errorCalculado = factorCalibration * (anguloDesfase.L1 - anguloCalculado);
                    resultadoAjuste.L1 = (int)(gainGapL1 + errorCalculado);

                    anguloCalculado = (1.5707963267949 - Math.Atan(listValues.Average(2) / listValues.Average(3))).ToDegree();
                    errorCalculado = factorCalibration * (anguloDesfase.L2 - anguloCalculado);
                    resultadoAjuste.L2 = (int)(gainGapL2 + errorCalculado);

                    anguloCalculado = (1.5707963267949 - Math.Atan(listValues.Average(4) / listValues.Average(5))).ToDegree();
                    errorCalculado = factorCalibration * (anguloDesfase.L3 - anguloCalculado);
                    resultadoAjuste.L3 = (int)(gainGapL3 + errorCalculado);

                    return true;// Validación en la secuencia;
                });

            return resultadoAjuste;
        }

        public TriLineGain CalculateVoltageAdjustMeasureFactors(int delFirst, int initCount, int samples, int interval, TriLineValue voltage)
        {
            TriLineGain resultadoAjuste = new TriLineGain();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var points = ReadPointsVoltage();
                    var lecturas = new List<double>();
                    lecturas.Add(points.L1);
                    lecturas.Add(points.L2);
                    lecturas.Add(points.L3);
                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    resultadoAjuste.L1 = (ushort)(Math.Sqrt(listValues.Average(0)) / (voltage.L1 / 100));
                    resultadoAjuste.L2 = (ushort)(Math.Sqrt(listValues.Average(1)) / (voltage.L2 / 100));
                    resultadoAjuste.L3 = (ushort)(Math.Sqrt(listValues.Average(2)) / (voltage.L3 / 100));
                    return true;
                });

            return resultadoAjuste;
        }

        public TriLineGain CalculatePowerMeasureFactors(int delFirst, int initCount, int samples, int interval, TriLineValue power, bool IsReactive)
        {
            TriLineGain resultadoAjuste = new TriLineGain();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var points = IsReactive == false ? ReadPointsActive() : ReadPointsReactive();
                    var lecturas = new List<double>();
                    lecturas.Add(points.L1);
                    lecturas.Add(points.L2);
                    lecturas.Add(points.L3);
                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    resultadoAjuste.L1 = (ushort)(listValues.Average(0) / power.L1);
                    resultadoAjuste.L2 = (ushort)(listValues.Average(1) / power.L2);
                    resultadoAjuste.L3 = (ushort)(listValues.Average(2) / power.L3);
                    return true;
                });

            return resultadoAjuste;
        }

        public TriLineMeasure CalculateOffsetFactors(int delFirst, int initCount, int samples, int interval, TriLineValue power, bool IsReactive)
        {
            TriLineMeasure resultadoAjuste = new TriLineMeasure();

            var gainPower = IsReactive == false ? ReadGainActive() : ReadGainReactive();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var points = IsReactive == false ? ReadPointsActive() : ReadPointsReactive();
                    var lecturas = new List<double>();
                    lecturas.Add(points.L1);
                    lecturas.Add(points.L2);
                    lecturas.Add(points.L3);
                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    resultadoAjuste.L1 = (int)(((listValues.Average(0) / gainPower.L1) - power.L1) * gainPower.L1);
                    resultadoAjuste.L2 = (int)(((listValues.Average(1) / gainPower.L2) - power.L2) * gainPower.L2);
                    resultadoAjuste.L3 = (int)(((listValues.Average(2) / gainPower.L3) - power.L3) * gainPower.L3);

                    return true;
                });

            return resultadoAjuste;
        }

        public Dictionary<DisplayConfigurations, Display> displayConfigurations = new Dictionary<DisplayConfigurations, Display> 
        { 
            { DisplayConfigurations.TODOS_SEGMENTOS_DESACTIVADOS, new Display{ Parte1 = 0x0000, Parte2 = 0x0000, Parte3 = 0x0000, Parte4 = 0x0000, Parte5 = 0x0000, Parte6 = 0x0000, Parte7 = 0x0000, Parte8 = 0x0000 } },
            { DisplayConfigurations.TODOS_SEGMENTOS_ACTIVADOS, new Display{ Parte1 = 0xFFFF, Parte2 = 0xFFFF, Parte3 = 0xFFFF, Parte4 = 0xFFFF, Parte5 = 0xFFFF, Parte6 = 0xFFFF, Parte7 = 0xFFFF, Parte8 = 0xFFFF } },
            { DisplayConfigurations.SEGMENTOS_NUMEROS_IMPARES, new Display{ Parte1 = 0x2525, Parte2 = 0x2525, Parte3 = 0x2525, Parte4 = 0x2500, Parte5 = 0x0000, Parte6 = 0x0000, Parte7 = 0x0000, Parte8 = 0x0000 } },
            { DisplayConfigurations.SEGMENTOS_NUMEROS_PARES, new Display{ Parte1 = 0x5A5A, Parte2 = 0x5A5A, Parte3 = 0x5A5A, Parte4 = 0x5A00, Parte5 = 0x0000, Parte6 = 0x0000, Parte7 = 0x0000, Parte8 = 0x0000 } },
            { DisplayConfigurations.SEGMENTOS_NUMEROS, new Display{ Parte1 = 0xFFFF, Parte2 = 0xFFFF, Parte3 = 0xFFFF, Parte4 = 0xFF00, Parte5 = 0x0000, Parte6 = 0x0000, Parte7 = 0x0000, Parte8 = 0x0000 } },
            { DisplayConfigurations.SEGMENTOS_SIMBOLOS_IMPARES, new Display{ Parte1 = 0x0000, Parte2 = 0x0000, Parte3 = 0x0000, Parte4 = 0x0025, Parte5 = 0x2525, Parte6 = 0x2525, Parte7 = 0x2525, Parte8 = 0x2525 } },
            { DisplayConfigurations.SEGMENTOS_SIMBOLOS_PARES, new Display{ Parte1 = 0x0000, Parte2 = 0x0000, Parte3 = 0x0000, Parte4 = 0x005A, Parte5 = 0x5A5A, Parte6 = 0x5A5A, Parte7 = 0x5A5A, Parte8 = 0x5A5A } },
            { DisplayConfigurations.SEGMENTOS_SIMBOLOS, new Display{ Parte1 = 0x0000, Parte2 = 0x0000, Parte3 = 0x0000, Parte4 = 0x00FF, Parte5 = 0xFFFF, Parte6 = 0xFFFF, Parte7 = 0xFFFF, Parte8 = 0xFFFF } },
            { DisplayConfigurations.SEGMENTOS_PARES, new Display{ Parte1 = 0x5A5A, Parte2 = 0x5A5A, Parte3 = 0x5A5A, Parte4 = 0x5A5A, Parte5 = 0x5A5A, Parte6 = 0x5A5A, Parte7 = 0x5A5A, Parte8 = 0x5A5A, Parte9 = 0x5A5A, Parte10 = 0x5A5A } },
            { DisplayConfigurations.SEGMENTOS_IMPARES, new Display{ Parte1 = 0x2525, Parte2 = 0x2525, Parte3 = 0x2525, Parte4 = 0x2525, Parte5 = 0x2525, Parte6 = 0x2525, Parte7 = 0x2525, Parte8 = 0x2525, Parte9 = 0x2525, Parte10 = 0x2525 } }
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.DISPLAY)]
        public struct Display
        {
            public UInt16 Parte1;
            public UInt16 Parte2;
            public UInt16 Parte3;
            public UInt16 Parte4;
            public UInt16 Parte5;
            public UInt16 Parte6;
            public UInt16 Parte7;
            public UInt16 Parte8;
            public UInt16 Parte9;
            public UInt16 Parte10;
        };
    }
}
