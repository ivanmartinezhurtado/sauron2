﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.IO;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(PCIE1762HDescription))]
    public partial class UC_PCIE1762H : UserControl
    {
        private MonitorInputs m = new MonitorInputs();
        private delegate void ReadInputsDelegate(byte input, bool state);

        private Dictionary<byte, bool> Inputs = new Dictionary<byte, bool>();
        private Dictionary<byte, InputControl> ControlInputs = new Dictionary<byte, InputControl>();

        private PCIE1762H board;
        private Exception instrumentException;

        public Type InstrumentType
        {
            get
            {
                return typeof(PCIE1762H);
            }
        }

        public PCIE1762H Instrument
        {
            get
            {
                if (board == null)
                {
                    instrumentException = null;
                    try
                    {      
                        board = new PCIE1762H();                    
                    }
                    catch (Exception ex)
                    {
                        instrumentException = ex;
                    }
                }
                return board;
            }
            set
            {
                if (board != null)
                {
                    board.Dispose();
                    board = null;
                }
                board = value;
            }
        }

        public UC_PCIE1762H()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            try
            {
                Instrument.LogEnabled = false;

                if(instrumentException != null)
                {
                    Logger.Error("Error cargando la targeta PCIE 1762H", instrumentException);
                    MessageBox.Show(instrumentException.Message, "Error IO PCIE 1762H", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                InitCard();
            }
            catch (IOException ex)
            {
                Logger.Error("Error cargando la targeta PCIE 1762H", ex);
                MessageBox.Show(ex.DecodedMessage, "Error IO PCIE 1762H", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void InitCard()
        {
            for (byte i = 0; i < Instrument.DI.Count; i++)
            {
                InputControl item = new InputControl();
                //item.IO = pci;
                item.NumIO = i;
                item.UpdateUI(Instrument.DI[i]);
                Inputs.Add(i, Instrument.DI[i]);
                ControlInputs.Add(i, item);
                InputPanel.Controls.Add(item);
            }

            for (byte i = 0; i < Instrument.DO.Count; i++)
            {
                OutputControl item = new OutputControl();
                item.IO = Instrument;
                item.NumIO = i;
                item.Width = 60;
                item.Height = 28;
                panelOutputs.Controls.Add(item);
            }
            m.Inputs = Inputs;
            m.IO = Instrument;
            m.Interval = 1000;
            m.ValueCahnged += new EventHandler<InputsEventArgs>(m_ValueReaded);
            m.Start();
        }

        void m_ValueReaded(object sender, InputsEventArgs e)
        {
            if (this.InvokeRequired)
                this.Invoke(new ReadInputsDelegate(UpdatePanel), (byte)e.Value, (bool)e.State);
            else
                UpdatePanel((byte)e.Value, (bool)e.State);
        }

        private void UpdatePanel(byte input, bool state)
        {
            ControlInputs[input].UpdateUI(state);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            m.Stop();

            if(board != null)
                board.Dispose();

            base.Dispose(disposing);
        }
    }

    public class PCIE1762HDescription : ControlDescription
    {
        public override string Name { get { return "PCIE 1762H"; } }
        public override string Category { get { return "In/Out"; } }
        public override Image Icon { get { return Properties.Resources.input; } }
        public override Image IconBig { get { return Properties.Resources.PCIE_1762H; } }
        public override Type SubcomponentType { get { return typeof(PCIE1762H); } }
    }
}
