﻿using Instruments.IO;
using Instruments.Utility;
using System;
using System.ComponentModel;

namespace Instruments.Towers
{
    [InstrumentsVersion(1.06)]
    public class TowerBoard : TowerBase<TowerBoard>, IDisposable, ITower
    {
        private const double VoltageFactorInputMux = 21.4;

        public override IMux MUX
        {
            get
            {
                if (mux == null)
                    try
                    {
                        _logger.InfoFormat("Inicializmos MUX M9120A de Keysight");
                        mux = new M9120A();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Error no se instanciado la MUX Keyisght por {0}", ex.Message));
                    }

                return mux;
            }
            set
            {
                mux = value;
            }
        }

        [Browsable(false)]
        public override double MeasureMultimeter(InputMuxEnum input, MagnitudToMeasure magnitud, int waitTimeMeasure = 500, Action preCondition = null, Action postCondition = null, bool whitOutIO = false)
        {
            if (MUX.GetType() != typeof(M9120A))
                throw new Exception("Error no se ha encontrado la MX M9120A de Keysight");

            var result = base.MeasureMultimeter(input, magnitud, waitTimeMeasure, preCondition, postCondition, whitOutIO);

            return ResultWithFactor(input, result);
        }

        public double ResultWithFactor(InputMuxEnum input, double result)
        {
            switch (input)
            {
                case InputMuxEnum.IN14:
                    result *= VoltageFactorInputMux;
                    break;
            }
            return result;
        }
    }
}
