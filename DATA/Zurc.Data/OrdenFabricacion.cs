namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("VOrdenFabricacion")]
    public partial class OrdenFabricacion
    {
        [Key]
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }
        public DateTime? FechaTeoricaInicio { get; set; }
        public DateTime? FechaRealInicio { get; set; }
        public int ArticuloId { get; set; }
        public int Version { get; set; }
        public int Unidades { get; set; }
        public int UnidadesPendientes { get; set; }
        public string Observaciones { get; set; }
        public string Usuario { get; set; }
        public string Lote { get; set; }
    }
}
