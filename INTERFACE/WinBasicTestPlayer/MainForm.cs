﻿using Dezac.Tests.Services;
using Dezac.Tests.Services.Logger;
using System;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using WinTestPlayer.Services;
using WinTestPlayer.Utils;
using WinTestPlayer.Views;

namespace WinTestPlayer
{
    public partial class MainForm : RadForm
    {
        protected readonly AppService app;
        protected readonly ShellService shell;

        private TestView testView;
        private Control currentView;

        private string Caption = "";

        public MainForm()
        {
            InitializeComponent();
        }

        public MainForm(AppService appService, ShellService shell)
            : this()
        {
            app = appService;
            this.shell = shell;

            this.Text = string.Format("TSD - Test Studio v.{0}", Application.ProductVersion);
            Caption = string.Format("TSD - Test Studio v.{0}", Application.ProductVersion);

            app.SC = SynchronizationContext.Current;
            app.Runner.AllTestStarting += OnTestStart;
            app.Runner.TestEnd += OnTestEnded;
            app.Runner.AllTestEnded += OnAllTestEnded;
            app.CloseForm += (s, ee) => { OnCloseForm(); };

            statusBar.Visible = false;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.shell.Init(this);
            this.slideMenu.Collapse();

            ThemeResolutionService.LoadPackageResource("WinTestPlayer.Resources.ExamplesBlue.tssp");
            ThemeResolutionService.RegisterThemeFromStorage(ThemeStorageType.Resource, "WinTestPlayer.Resources.ExamplesBlue_QuickStart_ControlsExpander.xml");
            ThemeResolutionService.ApplyThemeToControlTree(this.slideMenu, "ExamplesBlue");

            this.slideMenu.ControlsList.SelectedIndexChanged += ControlsList_SelectedIndexChanged;

            Tools.DelayCall(500, () =>
            {
                DoSelectOrden(true);
            });
        }

        public void DoSelectOrden(bool exit)
        {
            this.slideMenu.Collapse();
            try
            {
                var ctrl = new LoginEmpleadoView(false);
                if (shell.ShowDialog(Caption, ctrl, MessageBoxButtons.OKCancel) != DialogResult.OK)
                {
                    if (exit)
                        Close();
                    return;
                }
                app.Login(ctrl.Codigo);

                var labelOrden = ConfigurationManager.AppSettings["LabelOrden"];
                if (!string.IsNullOrEmpty(labelOrden))
                {
                    var orden = new SelectOrdenView(labelOrden);
                    if (shell.ShowDialog(Caption, orden, MessageBoxButtons.OKCancel) != DialogResult.OK)
                    {
                        if (exit)
                            Close();
                        return;
                    }
                    app.LoadLote(orden.Codigo, labelOrden);
                }

                var labelproducto = ConfigurationManager.AppSettings["LabelProducto"];
                if (!string.IsNullOrEmpty(labelproducto))
                {
                    var prod = new SelectOrdenView(labelproducto);
                    if (shell.ShowDialog(Caption, prod, MessageBoxButtons.OKCancel) != DialogResult.OK)
                    {
                        if (exit)
                            Close();
                        return;
                    }
                    app.LoadCodigo(prod.Codigo, labelproducto);
                }


                var labelcliente = ConfigurationManager.AppSettings["LabelCliente"];
                if (!string.IsNullOrEmpty(labelcliente))
                {
                    var clie = new SelectOrdenView(labelcliente);
                    if (shell.ShowDialog(Caption, clie, MessageBoxButtons.OKCancel) != DialogResult.OK)
                    {
                        if (exit)
                            Close();
                        return;
                    }
                    app.LoadCliente(clie.Codigo, labelcliente);
                }

                var cant = new SelectOrdenView("Cantidad");
                if (shell.ShowDialog(Caption, cant, MessageBoxButtons.OKCancel) != DialogResult.OK)
                {
                    if (exit)
                        Close();
                    return;
                }
                app.LoadCantidad(cant.Codigo, "Cantidad");

                PopulateControlsList();
            }
            catch (Exception ex)
            {
                shell.MsgBox(ex.Message, "Excepción al cargar los datos de la orden de fabricación", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                DoSelectOrden(true);
            }
        }

        public void PopulateControlsList()
        {
            this.slideMenu.ControlsList.Items.Clear();
            RadListElement listElement = this.slideMenu.ControlsList;
            int Index = 0;

            listElement.Items.Add(new RadListDataItem { Text = "Inicio", Image = Properties.Resources.Play_Normal_icon__1_, Tag = new Action(DoTestOrden) });
            listElement.Items.Add(new RadListDataItem { Text = "Salir", Image = Properties.Resources.Action_exit_icon, Tag = new Action(ExitTest) });

            this.slideMenu.ControlsList.SelectedIndex = Index;
            this.slideMenu.AllowExpand = true;
        }

        public void DoTestOrden()
        {
            app.LoadSequence();
            DoTest();
        }

        public void DoTest()
        {
            this.slideMenu.Collapse();

            if (app.Model.IdEmpleado == null)
                DoSelectOrden(true);
            else
            {
                if (testView == null)
                    testView = DI.Resolve<TestView>();

                LoadView(testView);
            }
        }

        private void LoadView(Control view)
        {
            if (view.Name == "TestView")
            {
                this.WindowState = FormWindowState.Maximized;
                header1.Visible = true;
            }
            else
                header1.Visible = false;


            if (view == currentView)
                return;

            this.slideMenu.Collapse();

            if (currentView != null)
            {
                mainPanel.Controls.Remove(currentView);
                if (currentView != testView)
                    currentView.Dispose();
            }

            view.Dock = DockStyle.Fill;
            mainPanel.Controls.Add(view);

            currentView = view;

            header1.Enabled = mainPanel.Controls.Contains(testView);
        }

        private void ExitTest()
        {
            Close();
            return;
        }

        private void ControlsList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            var item = this.slideMenu.ControlsList.SelectedItem;
            if (item == null)
                return;

            header1.Enabled = mainPanel.Controls.Contains(testView);

            Action action = item.Tag as Action;
            if (action != null)
                action();
        }

        private void OnTestStart(object sender, RunnerTestsInfo e)
        {
            this.ControlBox = false;
            this.slideMenu.AllowExpand = false;
            app.UpdateIndicators(e.Contexts.FirstOrDefault().TestResult);
        }

        private void OnTestEnded(object sender, SequenceContext e)
        {
            bool HasError = true;

            HasError = e.TestResult.StepResults.Where(p => p.Exception != null).Any(p => p.Section != SectionEnum.Init && p.Section != SectionEnum.End);

            if (e.NumInstance != 0)
                LogService.ShowLog(FormatLogViewer.LogsTypesEnum.DebugLog, true, e.NumInstance, e.Services.Get<ITestContext>(), false);

            app.UpdateIndicators(e.TestResult);

            this.slideMenu.AllowExpand = true;
        }

        private void OnAllTestEnded(object sender, RunnerTestsInfo e)
        {
            bool HasError = false;
         
            foreach (var test in e.Contexts)
                if (test.ExecutionResult == TestExecutionResult.Completed)
                    HasError |= test.TestResult.StepResults.Where(p => p.Exception != null).Any();
                else
                    HasError = true;

            Tools.DelayCall(500, () =>
            {
              //  ShowResults(e, HasError);
            });
        }

        //private void ShowResults(RunnerTestsInfo e, bool HasError)
        //{
        //    var result = shell.ShowDialog("TEST FINALIZADO", new TestResultView(e), MessageBoxButtons.YesNoCancel, (c) =>
        //    {
        //        var f = c as DialogView;
        //        f.Yes.Text = "Siguiente";
        //        f.No.Text = "Repetir";

        //        if (!HasError)
        //            f.No.Visible = false;

        //        if (f.No.Visible)
        //            f.No.Focus();
        //        else
        //            f.Yes.Focus();
        //    });

        //    this.ControlBox = true;

        //    if (result == DialogResult.Yes)
        //        app.Play();
        //    else if (result == DialogResult.No)
        //        app.Play(true);
        //}

        private void OnCloseForm()
        {
            ExitTest();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            e.Cancel = app.IsRunning;
        }
    }
}
