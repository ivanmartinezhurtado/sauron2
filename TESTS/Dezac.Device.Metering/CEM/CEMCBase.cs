﻿using Comunications.Utility;
using Dezac.Core.Enumerate;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.07)]
    public class CEMBase : DeviceBase
    {
        public CEMBase()
        {
        }

        public CEMBase(int port, int baudRate = 9600)
        {
            SetPort(port, baudRate);
        }

        public void SetPort(int port, int baudRate, byte periferic = 1)
        {
            Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            Frequency = TypeMeterConection.frequency._50Hz;
            Modbus.PerifericNumber = periferic;            
        }
       
        public CEMBase(string host, int port)
        {
            SetHostAndPort(host, port);
        }

        public void SetHostAndPort(string hostname, int port)
        {
            new ModbusDeviceTCP(hostname, port, ModbusTCPProtocol.RTU, _logger);
            Frequency = TypeMeterConection.frequency._50Hz;
        }

        public ModbusDevice Modbus { get; internal set; }

        public string MODELO_HARDWARE { get; set; }

        public TypeMeterConection.frequency Frequency { get; set; }

        //**************************************************

        public void SetFrequency(TypeMeterConection.frequency freq)
        {
            Frequency = freq;
        }

        public ushort ReadDigitalInputsAndKeyboard()
        {
            return Modbus.ReadRegister((ushort)Registers.DIGITAL_INPUTS_KEYBOARD);
        }

        public int ReadCRC()
        {
            return Modbus.ReadInt32((ushort)Registers.CRC);
        }

        public void WriteGainOscilator(ushort gain)
        {
            Modbus.Write((ushort)Registers.GAIN_OSCILATOR, gain);
        }

        public ushort ReadGainOscilator()
        {
            return Modbus.ReadRegister((ushort)Registers.GAIN_OSCILATOR);
        }

        public void WriteDigitalOutputs(ushort value)
        {
            var adress = 0x0021;
            //DIGITAL_OUTPUTS = 0x0021; //SOY CEM-21 / 31  y esta direccion de memoria cambia
            //MODELO_HARDWARE == 0 modelo CEM-C10 / CEM-C20 / CEM-C30
            //MODELO_HARDWARE == 1 modelo CEM-C21 /CEM-31

            if (MODELO_HARDWARE.Trim() == "0")
                adress = 48;

            Modbus.Write((ushort)adress, value);
        }


        public void WriteKindImpulseOutput(int value)
        {
            Modbus.Write((ushort)Registers.KIND_IMPULSE_OUTPUT, value);
        }

        public ushort ReadKindImpulseOutput()
        {
            return Modbus.ReadRegister((ushort)Registers.KIND_IMPULSE_OUTPUT);
        }

        public void WriteWeightimpulseLed(ushort value)
        {
            Modbus.Write((ushort)Registers.WEIGHT_IMPULSE_LED, value);
        }

        public void WriteDigitalInputImpulsesActivate()
        {
            Modbus.Write((ushort)Registers.ACTIVATE_INPUT_IMPULSE, (ushort)1);
        }

        public int ReadNumImpulseInput()
        {
            return Modbus.ReadInt32((ushort)Registers.READ_NUM_IMPULSE_INPUT);
        }

        public ushort ReadWeightimpulseLed()
        {
            return Modbus.ReadRegister((ushort)Registers.WEIGHT_IMPULSE_LED);
        }

        public void WriteWeightimpulseOutput(ushort value)
        {
            Modbus.Write((ushort)Registers.WEIGHT_IMPULSE_OUTPUT, value);
        }

        public ushort ReadWeightimpulseOutput()
        {
            return Modbus.ReadRegister((ushort)Registers.WEIGHT_IMPULSE_OUTPUT);
        }

        public void WriteModel(string value)
        {
            Modbus.WriteString((ushort)Registers.MODEL, value);
        }

        public string ReadModel()
        {
            return Modbus.ReadString((ushort)Registers.MODEL, 6);
        }

        public ushort ReadPPM()
        {
            return Modbus.ReadRegister((ushort)Registers.PPM);
        }

        public void WritePPM(int value)
        {
            Modbus.Write((ushort)Registers.PPM, value);
        }

        public int ReadNumSerie()
        {
            return Modbus.ReadInt32((ushort)Registers.NUM_SERIE);
        }

        public void WriteNumSerie(UInt32 value)
        {
            Modbus.WriteInt32((ushort)Registers.NUM_SERIE, (int)value);
        }

        public string ReadNumSerieASCII()
        {
            return Modbus.ReadString((ushort)Registers.NUM_SERIE_14DIGITOS, 7);
        }

        public void WriteNumSerieASCII(string value)
        {
            Modbus.WriteString((ushort)Registers.NUM_SERIE_14DIGITOS, value);
        }

        public int ReadBastidor()
        {
            return Modbus.ReadInt32((ushort)Registers.BASTIDOR);
        }

        public void WriteBastidor(int value)
        {
            Modbus.WriteInt32((ushort)Registers.BASTIDOR, value);
        }

        public FirmwareVersion ReadSoftwareVersion()
        {
            return Modbus.Read<FirmwareVersion>();
        }

        public int ReadCRCFirmware()
        {
            return Modbus.ReadInt32((ushort)Registers.CRC);

        }

        public ushort ReadErrorCode()
        {
            return Modbus.ReadRegister((ushort)Registers.ERROR_CODE);
        }

        public void WriteErrorCode(ushort value)
        {
            Modbus.Write((ushort)Registers.ERROR_CODE, value);
        }

        public Energy ReadEnergy()
        {
            return Modbus.Read<Energy>();
        }

        public void WriteReset()
        {
            Modbus.WriteSingleCoil((ushort)Registers.RESET, true);
        }

        public void WriteFlagTest()
        {
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, true);
        }

        public void WriteEspecialProgramation()
        {
            Modbus.WriteSingleCoil((ushort)Registers.ESCPECIAL_PROG, true);
        }

        //**********************************************
        public T ReadVoltage<T>() where T : struct
        {
            return Modbus.Read<T>((ushort)Registers.VOLTAGE);
        }

        public T ReadCurrent<T>() where T : struct
        {
            return Modbus.Read<T>((ushort)Registers.CURRENT);
        }

        [Browsable(false)]
        public T ReadPointsVoltage<T>() where T : struct
        {
            return Modbus.Read<T>((ushort)Registers.POINTS_VOLTAGE);
        }

        [Browsable(false)]
        public T ReadPointsActive<T>() where T : struct
        {
            return Modbus.Read<T>((ushort)Registers.POINTS_ACTIVA);
        }

        [Browsable(false)]
        public T ReadPointsReactive<T>()where T : struct
        {
            return Modbus.Read<T>((ushort)Registers.POINTS_REACTIVA);
        }

        [Browsable(false)]
        public T ReadOffsetActive<T>() where T : struct
        {
            return Modbus.Read<T>((ushort)Registers.GAIN_OFFSET_ACTIVE);
        }

        [Browsable(false)]
        public T ReadOffsetReactive<T>() where T : struct
        {
            return Modbus.Read<T>((ushort)Registers.GAIN_OFFSET_REACTIVE);
        }

        [Browsable(false)]
        protected T ReadGainGap<T>() where T : struct
        {
            if (Frequency== TypeMeterConection.frequency._50Hz)
                return Modbus.Read<T>((ushort)Registers.GAIN_DESFASE);
            else
                return Modbus.Read<T>((ushort)Registers.GAIN_DESFASE_60HZ);
        }

        public void WriteGainsGap<T>(T gain) where T : struct
        {
            if (Frequency == TypeMeterConection.frequency._50Hz)
                Modbus.Write<T>((ushort)Registers.GAIN_DESFASE, gain);
            else
                Modbus.Write<T>((ushort)Registers.GAIN_DESFASE_60HZ, gain);
        }

        [Browsable(false)]
        public T ReadGainActive<T>()where T : struct
        {
            if (Frequency== TypeMeterConection.frequency._50Hz)
                return Modbus.Read<T>((ushort)Registers.GAIN_ACTIVE);
            else
                return Modbus.Read<T>((ushort)Registers.GAIN_ACTIVE_60HZ);
        }

        [Browsable(false)]
        public T ReadGainReactive<T>()where T : struct
        {
            if (Frequency == TypeMeterConection.frequency._50Hz)
                return Modbus.Read<T>((ushort)Registers.GAIN_REACTIVE);
            else
                return Modbus.Read<T>((ushort)Registers.GAIN_REACTIVE_60HZ);
        }

        public void WriteGainsActive<T>(T gain)where T : struct
        {
            if (Frequency == TypeMeterConection.frequency._50Hz)
                Modbus.Write<T>((ushort)Registers.GAIN_ACTIVE, gain);
            else
                Modbus.Write<T>((ushort)Registers.GAIN_ACTIVE_60HZ, gain);
        }

        public void WriteGainsReactive<T>(T gain) where T : struct
        {
            if (Frequency == TypeMeterConection.frequency._50Hz)
                Modbus.Write<T>((ushort)Registers.GAIN_REACTIVE, gain);
            else
                Modbus.Write<T>((ushort)Registers.GAIN_REACTIVE_60HZ, gain);
        }

        public T ReadGainVoltage<T>() where T : struct
        {
            return Modbus.Read<T>((ushort)Registers.GAIN_VOLTAGE);
        }

        public void WriteGainsVoltage<T>(T gain) where T : struct
        {
                Modbus.Write<T>((ushort)Registers.GAIN_VOLTAGE, gain);
        }

        public void WriteGainOffsetActive<T>(T gain)where T : struct
        {
            Modbus.Write<T>((ushort)Registers.GAIN_OFFSET_ACTIVE, gain);        
        }

        public void WriteGainOffsetReactive<T>(T gain) where T : struct
        {
            Modbus.Write<T>((ushort)Registers.GAIN_OFFSET_REACTIVE, gain);
        }

        //****************************************************
  
        #region Estructuras y diccionarios

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.VERSION_SOFT)]
        public struct FirmwareVersion
        {
            public UInt16 Major;
            public UInt16 Minor;
            public UInt16 Revision;
        };
  
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 1800)]
        public struct Energy
        {
            public Int32 ActiveExport;
            public Int32 ActiveImport;
            public Int32 ReactiveQ1;
            public Int32 ReactiveQ2;
            public Int32 ReactiveQ3;
            public Int32 ReactiveQ4;
        };
        public struct TriLineMeasure
        {
            public Int32 L1;
            public Int32 L2;
            public Int32 L3;
        }

        public struct TriLineGain
        {
            public UInt16 L1; 
            public UInt16 L2;
            public UInt16 L3;
        }

        public enum Registers
        {
            NUM_SERIE = 61440,
            NUM_SERIE_14DIGITOS = 0xF008,
            BASTIDOR = 61504,
            VERSION_SOFT = 80,
            ERROR_CODE = 61536,
            MODEL = 61456,
            CRC = 61728, //0xF120
            RESET = 61440, //61440
            FLAG_TEST = 8,
            ESCPECIAL_PROG = 0x0151,

            VOLTAGE = 1842,
            CURRENT = 1848,
            POWER_ACTIVE = 1862,
            POWER_REACTIVE = 1870,

            POINTS_VOLTAGE = 62225,
            POINTS_ACTIVA = 62241,
            POINTS_REACTIVA = 62257,

            GAIN_VOLTAGE = 62481,
            GAIN_ACTIVE = 62497,
            GAIN_REACTIVE = 62513,
            GAIN_DESFASE = 62561,
            GAIN_OFFSET_ACTIVE = 62529,
            GAIN_OFFSET_REACTIVE = 62545,
            GAIN_ACTIVE_60HZ = 62577,
            GAIN_REACTIVE_60HZ = 62593,
            GAIN_DESFASE_60HZ = 62609,
            GAIN_OSCILATOR = 0xF050,

            DIGITAL_INPUTS_KEYBOARD = 32,
            KIND_IMPULSE_OUTPUT = 128,
            WEIGHT_IMPULSE_OUTPUT = 129,
            PPM = 61520,
            WEIGHT_IMPULSE_LED = 61712,
            DISPLAY = 61584,

            READ_NUM_IMPULSE_INPUT = 0x182,
            ACTIVATE_INPUT_IMPULSE = 1108
        };

        public enum DisplayConfigurations
        {
            TODOS_SEGMENTOS_ACTIVADOS,
            TODOS_SEGMENTOS_DESACTIVADOS,
            SEGMENTOS_NUMEROS_PARES,
            SEGMENTOS_NUMEROS_IMPARES,
            SEGMENTOS_NUMEROS,
            SEGMENTOS_SIMBOLOS_PARES,
            SEGMENTOS_SIMBOLOS_IMPARES,
            SEGMENTOS_SIMBOLOS,
            SEGMENTOS_PARES,
            SEGMENTOS_IMPARES
        }
        
        #endregion
        
        public override void Dispose()
        {
            if(Modbus != null)
                Modbus.Dispose();
        }

    }
}
