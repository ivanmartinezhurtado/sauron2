﻿using System;
using System.Windows.Forms;

namespace Dezac.Tests.UserControls
{
    public delegate void DialgoResultEventHandler(object sender, DialogResultEventArgs e);

    public interface IDialogView
    {
        event DialgoResultEventHandler CloseDialog;

        bool ValidateView(DialogResult result);
    }

    public interface IFocusable
    {
    }

    public class DialogViewBase : UserControl, IDialogView
    {
        public event DialgoResultEventHandler CloseDialog;

        public void CloseView(DialogResult result)
        {
            if (CloseDialog != null)
                CloseDialog(this, new DialogResultEventArgs(result));
        }

        public virtual bool ValidateView(DialogResult result)
        {
            return true;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // DialogViewBase
            // 
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Name = "DialogViewBase";
            this.ResumeLayout(false);

        }
    }

    public class DialogResultEventArgs : EventArgs
    {
        public DialogResultEventArgs(DialogResult result)
        {
            Result = result;
        }

        public DialogResult Result { get; private set; }
    }
}
