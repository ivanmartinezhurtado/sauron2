﻿using Autofac;
using Dezac.Tests.Extensions;
using Dezac.Tests.Services;
using System.IO;
using System.Reflection;
using WinTaskRunner.Design;
using WinTaskRunner.Design.Services;
using WinTaskRunner.Utils;
using WinTaskRunner.Views;

namespace WinTaskRunner.Services
{
    public class DI
    {
        public static IContainer Container { get; private set; }

        public static IContainer Build()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<EventBus>().SingleInstance();
            builder.RegisterType<AssemblyResolverService>().SingleInstance();
            builder.RegisterType<ShellService>().As<IShell>().SingleInstance();
            builder.RegisterType<RunnerService>().SingleInstance();
            builder.RegisterType<AppService>().SingleInstance();
            builder.RegisterType<ResultsParamView>().SingleInstance();
            builder.RegisterType<Toolbox>();
            builder.RegisterType<ComponentsView>();
            builder.RegisterType<InstrumentsView>();
            builder.RegisterType<InspectorViewer>();
            builder.RegisterType<VariablesView>().SingleInstance();
            builder.RegisterType<LogViewer>();
            builder.RegisterType<EditorView>();
            builder.RegisterType<ModbusRegisterView>();
            builder.RegisterType<PropertyView>();
            builder.RegisterType<DocumentService>().As<IDocumentService>().SingleInstance();
            builder.RegisterType<StepChartView>();
            builder.RegisterType<InstanceResolveService>();
            builder.RegisterType<MainForm>();

            builder.RegisterType<DezacRunnerExtensions>().As<IRunnerExtensions>();

            AppConfig appConfig = AppProfile.DeserializeFile<AppConfig>(AppProfile.GetConfigFile());

            foreach (string name in appConfig.LoadExtensions)
                if (File.Exists(name))
                {
                    var assembly = Assembly.LoadFrom(name);
                    builder.RegisterAssemblyModules(assembly);
                }

            Container = builder.Build();

            ServiceContainer.Default = new ServiceResolver(Container);

            return Container;
        }

        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }
    }

    public class ServiceResolver : IServiceResolver
    {
        private IContainer Container;

        public ServiceResolver(IContainer container)
        {
            Container = container;
        }

        public T Resolve<T>()
        {
            return Container.Resolve<T>();
        }
    }
}
