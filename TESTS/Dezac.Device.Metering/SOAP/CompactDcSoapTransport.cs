﻿using log4net;
using System;

namespace Dezac.Device.Metering.SOAP
{
    public class CompactDcSoapTransport
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(CompactDcSoapTransport));

        private static uint id = 1;

        private string txtTfStartA = "20100527083345S";

        private string txtTfFinA = "20100527083345S";

        public string IP {get; set;}

        public string Url { get { return "http://" + IP + ":8080"; } }

        public int TimeOut {get; set;}

        public uint Prioridad { get; set; } // 1 por defecto

        public CompactDcSoapTransport()
        {
            TimeOut = 12000;
            Prioridad = 1;
            IP = "100.0.0.1";
        }

        //SynchReport Request
        public string SynchReport(string txtIdRptA, string txtMetersA = "")
        {
            string Response = "";

            try
            {
                using (Web_References.WS_DC serviceWeb = new Web_References.WS_DC())
                {
                    serviceWeb.Url = Url;
                    serviceWeb.Timeout = TimeOut;

                    logger.InfoFormat("SynchReport TX -->  {0}  {1}", txtIdRptA, txtMetersA);

                     Response = serviceWeb.Request(id++, txtIdRptA, txtTfStartA, txtTfFinA, txtMetersA, Prioridad);

                    logger.InfoFormat("SynchReport RX  <-- {0}", Response);
                }
            }catch(Exception e)
            {
                logger.ErrorFormat("{0} ->> {1}", e.GetType().Name, e);
                throw;
            }

            return Response;
        }

        public void Order(string txtOrder)
        {
            bool response = false;

            try
            {
                using (Web_References.WS_DC serviceWeb = new Web_References.WS_DC())
                {
                    serviceWeb.Url = Url;
                    serviceWeb.Timeout = TimeOut;

                    logger.InfoFormat("Order -->  {0} ", txtOrder);

                    response = serviceWeb.Order(id++, 0, txtOrder, Prioridad);
                }
            }
            catch (Exception e)
            {
                logger.ErrorFormat("{0} -> {1}", e.GetType().Name, e);
                throw;
            }

            if (!response)
                throw new Exception("El servicio Web ha devuelto error al enviar el Order");
        }
    }
}

