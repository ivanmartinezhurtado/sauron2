﻿using Dezac.Core.Utility;
using Dezac.Device.Metering;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.Windows.Forms;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.03)]
    public class CompactDispenserTest : TestBase
    {
        public COMPACT_DISPENSER compact;
        public Tower3 tower;

        public void ModbusLibraryTest()
        {
            using (compact = new COMPACT_DISPENSER(4))
            {

                var TriReading = compact.ReadActivePower50HzGain();
                TriReading = compact.ReadActivePower50HzOffset();
                TriReading = compact.ReadActivePower60HzGain();
                //var intReading = compact.ReadCalibrationState(); // NO FUNCIONA
                var converterReading = compact.ReadConverterPoints();
                var electricalVariablesReading = compact.ReadElectricalVariables();
               var intReading = compact.ReadFabricationNumber();
                //intReading = compact.ReadHSIInternalOscillatorCalibration(); // NO FUNCIONA
                intReading = compact.ReadModbusAddress();
                intReading = compact.ReadModel();
                //intReading = compact.ReadOutputTyoe(); // NO FUNCIONA
                //intReading = compact.ReadOutputWeight();// NO FUNCIONA
                var ushortReading = compact.ReadPhaseGap50Hz();
                ushortReading = compact.ReadPhaseGap60Hz();
                TriReading = compact.ReadReactivePower50HzGain();
                TriReading = compact.ReadReactivePower50HzOffset();
                TriReading = compact.ReadReactivePower60HzGain();
                intReading = compact.ReadSerialNumber();
                intReading = compact.ReadSerialNumberReadOnly();
                //var transformationReading = compact.ReadTransformationRelation();// NO FUNCIONA
                TriReading = compact.ReadVoltageGain();
                compact.WriteActivePower50HzGains(compact.ReadActivePower50HzGain());
                //compact.WriteActivePower50HzOffset(compact.ReadReactivePower50HzOffset());// NO FUNCIONA
                compact.WriteActivePower60HzGains(compact.ReadActivePower60HzGain());
                //compact.WriteCalibrationState(compact.ReadCalibrationState());// NO FUNCIONA
                //compact.WriteFabricationNumber(compact.ReadFabricationNumber());
                //compact.WriteHSIInternalOscillatorCalibration(compact.ReadHSIInternalOscillatorCalibration());
                //compact.WriteModel(compact.ReadModel());// NO FUNCIONA
                //compact.WriteOutputType(compact.ReadOutputTyoe());// NO FUNCIONA
                //compact.WriteOutputWeight(compact.ReadOutputWeight());
                compact.WritePhaseGap50Hz(compact.ReadPhaseGap50Hz());
                compact.WritePhaseGap60Hz(compact.ReadPhaseGap60Hz());
                compact.WriteReactivePower50HzGains(compact.ReadReactivePower50HzGain());
                compact.WriteReactivePower50HzOffset(compact.ReadReactivePower50HzOffset());
                compact.WriteReactivePower60HzGains(compact.ReadReactivePower60HzGain());
                compact.WriteSerialNumber(compact.ReadSerialNumber());
                compact.WriteVoltageGains(compact.ReadVoltageGain());
                compact.WriteDisplay(COMPACT_DISPENSER.LCDConfigurations.ALL);
                compact.WriteDisplay(COMPACT_DISPENSER.LCDConfigurations.IMPAIR_PINS);
                compact.WriteDisplay(COMPACT_DISPENSER.LCDConfigurations.PAIR_PINS);
                compact.WriteDisplay(COMPACT_DISPENSER.LCDConfigurations.NONE);
            }

        }

        public void TestInitialization(byte port = 0)
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            
            compact = new COMPACT_DISPENSER(port.Equals(0) ? Comunicaciones.SerialPort : port);
        }

        public void TestSecurityBlock()
        {
            internalMTESetPoint(220, 0, 0, 50);

            var voltageReading = tower.CvmminiMTE.ReadAllVariables().Phases.L1.Tension;

            Assert.AreNotEqual(voltageReading, 0, Error().HARDWARE.MANGUERA.DETECCION_INSTRUMENTO("Error, no se detecta la manguera de seguridad conectada, por favor conecte la manguera"));

            internalMTEShutdown();
        }

        public void TestConsumption()
        {
            internalMTESetPoint(Consignas.GetDouble(Params.V.Null.EnVacio.Name, 220, ParamUnidad.V), Consignas.GetDouble(Params.I.Null.EnVacio.Name, 0, ParamUnidad.A), 0, 50);

            Delay(2000, "Esperando encendido equipo");
            var readings = tower.CvmminiMTE.ReadAllVariables();

            Assert.AreBetween(Params.KW.Null.EnVacio.Name, readings.Phases.L1.PotenciaActiva, Params.KW.Null.EnVacio.Min(), Params.KW.Null.EnVacio.Max(), Error().UUT.CONSUMO.MARGENES("Error, consumo de potencia activa fuera de márgenes"), ParamUnidad.W);
            Assert.AreBetween(Params.KVAR.Null.EnVacio.Name, readings.Phases.L1.PotenciaReactiva, Params.KVAR.Null.EnVacio.Min(), Params.KVAR.Null.EnVacio.Max(), Error().UUT.CONSUMO.MARGENES("Error, consumo de potencia activa fuera de márgenes"), ParamUnidad.Var);
        }
        
        public void TestComunications()
        {
            var version = string.Empty;
            try
            {
                version = compact.ReadFirmwareVersion();
            }
            catch (TimeoutException)
            {
                throw new Exception("Error de comunicaciones, el equipo no comunica");
            }
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error de firmware, la version del equipo no es la esperada"), ParamUnidad.SinUnidad);

            if (Model.IdFase == "100")
            {
                CodeUtils.Safe(() => compact.WriteProductionStatus(9999), "Error, no se ha logrado establecer comunicación con el equipo.");

                compact.WriteProductionStatus(5999);
            }
            else
            {
                var productionStatusReading = compact.ReadProductionStatus();

                if (productionStatusReading == 0)
                {
                    compact.WriteProductionStatus(9999);
                    compact.WriteProductionStatus(5000);
                }
                else
                    Assert.AreEqual(productionStatusReading, 5000, Error().PROCESO.TRAZABILIDAD.FASES_TEST_INCORRECTAS("Error de trazabilidad, este producto no ha pasado la fase de ajuste"));
            }
        }

        public void TestKeyboard()
        {
            tower.IO.DO.On(23);

            tower.LAMBDA.Initialize();
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(24, 0);

            tower.IO.DO.OnWait(150, 17);
            // tower.IO.DO.On(KEY_OUT);

            SamplerWithCancel((p) =>
            {
                var input = compact.ReadInput();

                return input.Equals(1);
            }, "Error de entrada optica - pulsacion larga", 6, 150);

            SamplerWithCancel((p) =>
                {
                    var input = compact.ReadInput();

                    return input.Equals(0);
                }, "Error de entrada optica - pulsacion larga", 5, 500);

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            tower.IO.DO.Off(17, 23);
        }

        public void TestSetupDefault()
        {
            var voltageGain = (ushort)Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_V + "_DEFECTO", 3843, ParamUnidad.Puntos);
            var activePowerGain = (ushort)Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_KW + "_DEFECTO", 34500, ParamUnidad.Puntos);
            var reactivePowerGain = (ushort)Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_KVAR + "_DEFECTO", 34500, ParamUnidad.Puntos);
            var offsetGain = (ushort)Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_OFFSET + "_DEFECTO", 0, ParamUnidad.Puntos);
            var phaseGapGain = (short)Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_DESFASE + "_DEFECTO", -33, ParamUnidad.Puntos);

            compact.WriteInformationReset();

            Delay(8000, "esperando encendido de equipo");

            compact.WriteActivePower50HzGains(activePowerGain);
            compact.WriteActivePower60HzGains(activePowerGain);
            compact.WriteReactivePower50HzGains(reactivePowerGain);
            compact.WriteReactivePower60HzGains(reactivePowerGain);
            compact.WriteVoltageGains(voltageGain);
            compact.WriteActivePower50HzOffset(offsetGain);
            compact.WriteReactivePower50HzOffset(offsetGain);
            compact.WritePhaseGap50Hz(phaseGapGain);
            compact.WritePhaseGap60Hz(phaseGapGain);

            compact.WriteProductionNumber((int)TestInfo.NumBastidor);
        }

        public void TestDisplay()
        {
            foreach(COMPACT_DISPENSER.LCDConfigurations configuration in Enum.GetValues(typeof(COMPACT_DISPENSER.LCDConfigurations)))
            {
                compact.WriteDisplay(configuration);

                var dialogResult = Shell.ShowDialog("TEST DISPLAY", () => new ImageView("Comprobar que el display muestra los mismos segmentos que en la imagen", string.Format("{0}\\{1}\\{2}_{3}.jpg", PathCameraImages.TrimEnd('\\'), compact.GetType().Name, "DISPLAY", configuration.ToString())), System.Windows.Forms.MessageBoxButtons.YesNo);

                Assert.AreEqual(dialogResult, DialogResult.Yes, Error().UUT.CONFIGURACION.DISPLAY(string.Format("Error en el display al comprobar la configuración siguiente: {0}", compact.LCDConfigurationsTranslation[configuration])));
            }

            Resultado.Set("DISPLAY", "OK", ParamUnidad.SinUnidad);
        }

        public void TestImpulseLed()
        {
            compact.WriteOutputToActivate(COMPACT_DISPENSER.OutputActivations.OUTPUT_LED);

            var dialogResult = Shell.ShowDialog("Prueba led de impulsos", () => new ImageView("se enciende el led destacado?", string.Format("{0}\\{1}\\{2}.jpg", PathCameraImages.TrimEnd('\\'), compact.GetType().Name, "LED_IMPULSOS")), MessageBoxButtons.YesNo);

            Assert.AreEqual(dialogResult, DialogResult.Yes, Error().PROCESO.MATERIAL.LED("Error, no se enciende el led de impulsos del equipo"));

            Resultado.Set("LED_IMPULSOS", "OK", ParamUnidad.SinUnidad);

            compact.WriteOutputToActivate(COMPACT_DISPENSER.OutputActivations.NONE);
        }
        
        public void TestBicolorLed()
        {
            compact.WriteOutputToActivate(COMPACT_DISPENSER.OutputActivations.OUTPUT_BICOLOR_GREEN_LED);

            var dialogResult = Shell.ShowDialog("Prueba led de impulsos", () => new ImageView("Retirar el cabezal de comunicaciones y comprobar que se enciende el led destacado, acto seguido volver a poner el cabezal de comunicaciones en su sitio", string.Format("{0}\\{1}\\{2}.jpg", PathCameraImages.TrimEnd('\\'), compact.GetType().Name, "LED_VERDE")), MessageBoxButtons.YesNo);

            Assert.AreEqual(dialogResult, DialogResult.Yes, Error().PROCESO.MATERIAL.LED("Error, no se enciende el led verde del equipo"));

            compact.WriteOutputToActivate(COMPACT_DISPENSER.OutputActivations.OUTPUT_BICOLOR_RED_LED);

            dialogResult = Shell.ShowDialog("Prueba led de impulsos", () => new ImageView("Retirar el cabezal de comunicaciones y comprobar que se enciende el led destacado, acto seguido volver a poner el cabezal de comunicaciones en su sitio", string.Format("{0}\\{1}\\{2}.jpg", PathCameraImages.TrimEnd('\\'), compact.GetType().Name, "LED_ROJO")), MessageBoxButtons.YesNo);

            Assert.AreEqual(dialogResult, DialogResult.Yes, Error().PROCESO.MATERIAL.LED("Error, no se enciende el led rojo del equipo"));

            Resultado.Set("LED_BICOLOR", "OK", ParamUnidad.SinUnidad);

            compact.WriteOutputToActivate(COMPACT_DISPENSER.OutputActivations.NONE);
        }

        public void TestRelay()
        {
            tower.IO.DO.On(18, 19);

            compact.WriteOutputToActivate(COMPACT_DISPENSER.OutputActivations.OUTPUT_RELAY_OPEN);
            compact.WriteOutputToActivate(COMPACT_DISPENSER.OutputActivations.OUTPUT_BICOLOR_GREEN_LED);
            
            Delay(1000, "Espera para las entradas de la torre");

            Assert.IsFalse(tower.IO.DI.Read(7).Value, Error().UUT.CONFIGURACION.RELE("Error, no se detecta la apertura del rele de corte de fase"));
            Assert.IsFalse(tower.IO.DI.Read(8).Value, Error().UUT.CONFIGURACION.RELE("Error, no se detecta la apertura del rele de corte de neutro"));

            compact.WriteOutputToActivate(COMPACT_DISPENSER.OutputActivations.OUTPUT_RELAY_CLOSE);
            compact.WriteOutputToActivate(COMPACT_DISPENSER.OutputActivations.OUTPUT_BICOLOR_GREEN_LED);

            Delay(1000, "Espera para las de entradas de la torre");

            Assert.IsTrue(tower.IO.DI.Read(7).Value, Error().UUT.CONFIGURACION.RELE("Error, no se detecta el cierre del rele de corte de fase"));
            Assert.IsTrue(tower.IO.DI.Read(8).Value, Error().UUT.CONFIGURACION.RELE("Error, no se detecta el cierre del rele de corte de neutro"));

            Resultado.Set("RELAY_OUTPUT", "OK", ParamUnidad.SinUnidad);

            tower.IO.DO.Off(18, 19);
        }

        public void TestMeasureDefault()
        {
            internalMTESetPoint(Consignas.GetDouble(Params.V.Null.Verificacion.TestPoint("GANANCIA_DEFECTO").Name, 220, ParamUnidad.V),
                Consignas.GetDouble(Params.I.Null.Verificacion.TestPoint("GANANCIA_DEFECTO").Name, 5, ParamUnidad.A), 0, 50);

            Delay(7000, "Esperando encendido del equipo");

            var readings = compact.ReadElectricalVariables().ActivePower;

            Assert.AreBetween("MEDIDA_DEFECTO", readings, Params.KW.Null.Verificacion.TestPoint("GANANCIA_DEFECTO").Min(), Params.KW.Null.Verificacion.TestPoint("GANANCIA_DEFECTO").Max(), Error().UUT.MEDIDA.MARGENES("Error en el sistema de medida, la potencia leida con la ganancia por defecto de encuentra fuera de márgenes"), ParamUnidad.W);
        }

        public void TestMemory()
        {

            internalMTESetPoint(Consignas.GetDouble(Params.V.Null.Verificacion.TestPoint("GANANCIA_DEFECTO").Name, 220, ParamUnidad.V),
                Consignas.GetDouble(Params.I.Null.Verificacion.TestPoint("GANANCIA_DEFECTO").Name, 5, ParamUnidad.A), 0, 50);

            Delay(8000, "esperando encendido de equipo");

            var readingsBeforeShutdown = compact.ReadElectricalVariables().ActiveEnergyImported;

            internalMTEShutdown();

            Delay(5000, "esperando apagado de equipo");

            internalMTESetPoint(Consignas.GetDouble(Params.V.Null.Verificacion.TestPoint("GANANCIA_DEFECTO").Name, 220, ParamUnidad.V),
                Consignas.GetDouble(Params.I.Null.Verificacion.TestPoint("GANANCIA_DEFECTO").Name, 5, ParamUnidad.A), 0, 50);

            Delay(8000, "esperando encendido de equipo");

            SamplerWithCancel((p) =>
            {
                var readingsAfterShutdown = compact.ReadElectricalVariables().ActiveEnergyImported;

                return readingsAfterShutdown >= readingsBeforeShutdown;
            }, "Error, no se registra un incremento de la energia activa importada", 10, 3000);
        }

        public void TestVoltageAdjust()
        {
            var calculatedVoltageFactor = internalVoltageAdjust();

            compact.WriteProductionStatus(9999);
            if (Model.IdFase == "100")
                compact.WriteProductionStatus(5999);

            compact.WriteVoltageGains(calculatedVoltageFactor);
        }

        public void TestPhaseGapAdjust50Hz()
        {
            short defaultValue = -33;
            compact.WritePhaseGap50Hz(defaultValue);

            var calculatedPhaseGap = internalPhaseGapAdjust("50HZ");

            compact.WritePhaseGap50Hz((short)(calculatedPhaseGap + defaultValue));
        }

        public void TestPhaseGapAdjust60Hz()
        {
            short defaultValue = -33;
            compact.WritePhaseGap60Hz(defaultValue);

            var calculatedPhaseGap = internalPhaseGapAdjust("60HZ");

            compact.WritePhaseGap60Hz((short)(calculatedPhaseGap + defaultValue));
        }

        public void TestPowerGainsAdjust50Hz()
        {
            var calculatedGains = internalPowerGainsAdjust("50HZ");

            compact.WriteActivePower50HzGains(calculatedGains);
            compact.WriteReactivePower50HzGains(calculatedGains);
        }

        public void TestPowerGainsAdjust60Hz()
        {
            var calculatedGains = internalPowerGainsAdjust("60HZ");

            compact.WriteActivePower60HzGains(calculatedGains);
            compact.WriteReactivePower60HzGains(calculatedGains);
        }

        public void TestVoltageVerification()
        {
            AdjustValueDef voltageVerification = new AdjustValueDef(Params.V.Null.Verificacion.Name, 0, 0, 0, 0, Params.V.Null.Verificacion.Tol(), ParamUnidad.V);

            Delay(8000, "Espera medida equipo");

            TestCalibracionBase(voltageVerification, () =>
            {
                return compact.ReadElectricalVariables().Voltage / 10D;
            },
            () =>
            {
                return tower.PowerSourceIII.ReadVoltage().L1;
            }, 5, 7, 1100, "Error en la verificación de tensión, la tensión medida esta fuera de márgenes del patron");
        }

        public void TestPoint_MarchaVacio()
        {
            internalMTEShutdown();

            tower.IO.DO.Off(18, 19);

            var voltageSetPoint = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 220, ParamUnidad.V);
            var currentSetPoint = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 5, ParamUnidad.V);

            internalMTESetPoint(voltageSetPoint * 1.2, currentSetPoint * 0, 0, 50);

            Delay(5000, "Esperando encendido del equipo");

            var reading = compact.ReadConverterPoints().ActivePower;
            var powerActiveReading = compact.ReadActivePower50HzGain();

            AdjustValueDef adjustMarchaVacio = new AdjustValueDef() { Name = Params.KW.Null.Verificacion.TestPoint("MARCHA_VACIO").Name, Min = 0, Max = Params.KW.Null.Verificacion.TestPoint("MARCHA_VACIO").Max(), Value = reading, Unidad = ParamUnidad.Puntos };

             TestMeasureBase(adjustMarchaVacio, 
              (s) => 
              { return compact.ReadConverterPoints().ActivePower; }, 0, 5, 1100);
        }

        public void TestPoint_Arranque()
        {
            var voltageSetPoint = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 220, ParamUnidad.V);
            var currentSetPoint = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 5, ParamUnidad.V);

            internalVerification("ARRANQUE", voltageSetPoint, currentSetPoint * 0.0004D, 0);
        }

        public void TestPoint_Imin()
        {
            var voltageSetPoint = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 220, ParamUnidad.V);
            var currentSetPoint = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 5, ParamUnidad.V);

            internalVerification("CORRIENTE_MINIMA", voltageSetPoint, currentSetPoint * 0.005D, 0);
        }
        
        public void TestPoint_IbCosPhi()
        {
            var voltageSetPoint = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 220, ParamUnidad.V);
            var currentSetPoint = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 5, ParamUnidad.V);
            var phaseGapSetPoint = Consignas.GetDouble(Params.PHASE.Null.Verificacion.Name, 45, ParamUnidad.Grados);

            internalVerification("CORRIENTE_BASE_DESFASE", voltageSetPoint, currentSetPoint, phaseGapSetPoint);        
        }

        public void TestPoint_IMax()
        {
            var voltageSetPoint = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 220, ParamUnidad.V);
            var currentSetPoint = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 5, ParamUnidad.V);

            internalVerification("CORRIENTE_MAXIMA", voltageSetPoint, currentSetPoint * 2, 0);
        }

        public void TestClearInfo()
        {
            compact.WriteInformationReset();

            if (Model.IdFase == "100")
                compact.WriteProductionStatus(5000);

        }

        public void TestResetDevice()
        {
            int serialNumber = CaptureSerialNumberFromReader();

            TestInfo.NumSerie = serialNumber.ToString();
            
            compact.WriteProductionStatus(9999);

            internalMTEShutdown();

            internalMTESetPoint(220, 0, 0, 50);

            compact.WriteSerialNumber(serialNumber);

            compact.WriteInformationReset();

            Delay(8000, "esperando encendido de equipo");

            compact.WriteProductionStatus(0);
        }

        public void TestFinish()
        {
            if (compact != null)
                compact.Dispose();

            if (tower != null)
            {
                tower.IO.DO.Off(17, 18, 19, 23);

                internalMTEShutdown();

                tower.ShutdownSources();

                tower.Dispose();
            }
        }

        private ushort internalVoltageAdjust()
        {
            var voltageSetPoint = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 220, ParamUnidad.V);
            var currentSetPoint = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var phaseGapSetPoint = Consignas.GetDouble(Params.PHASE.Null.Ajuste.Name, 0, ParamUnidad.Grados);

            internalMTESetPoint(voltageSetPoint, currentSetPoint, phaseGapSetPoint, 50);

            Delay(8000, "Espera lectura potencias");

            AdjustValueDef adjustValue = new AdjustValueDef() { Name = Params.GAIN_V.Null.Ajuste.Name, Min = Params.GAIN_V.Null.Ajuste.Min(), Max = Params.GAIN_V.Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad };

            var adjustResult = TestMeasureBase(adjustValue, (s) =>
            {
                var converterPoints = compact.ReadConverterPoints().Voltage;
                return Math.Sqrt(converterPoints) / 2.2D;
            }, 0, 5, 500);

            return (ushort)adjustResult.Value;

        }

        private short internalPhaseGapAdjust(string point)
        {
            var voltageSetPoint = Consignas.GetDouble(Params.V.Null.AjusteDesfase.Name, 220, ParamUnidad.V);
            var currentSetPoint = Consignas.GetDouble(Params.I.Null.AjusteDesfase.Name, 5, ParamUnidad.A);
            var phaseGapSetPoint = Consignas.GetDouble(Params.PHASE.Null.AjusteDesfase.Name, 45, ParamUnidad.Grados);
            var frequencySetPoint = Consignas.GetDouble(Params.FREQ.Null.AjusteDesfase.TestPoint(point).Name, 50, ParamUnidad.Hz);

            internalMTESetPoint(voltageSetPoint, currentSetPoint, phaseGapSetPoint, frequencySetPoint);

            Delay(6000, "Espera lectura potencias");

            var adjustFactor = Configuracion.GetDouble(Params.PARAMETER("PHASE_GAP_FACTOR_" + point, ParamUnidad.SinUnidad).Null.Name, -22.2, ParamUnidad.SinUnidad);

            AdjustValueDef adjustValue = new AdjustValueDef() { Name = Params.GAIN_DESFASE.Null.AjusteDesfase.TestPoint(point).Name, Min = Params.GAIN_DESFASE.Null.AjusteDesfase.TestPoint(point).Min(), Max = Params.GAIN_DESFASE.Null.AjusteDesfase.TestPoint(point).Max(), Unidad = ParamUnidad.SinUnidad };

            var adjustResult = TestMeasureBase(adjustValue, (s) =>
            {
                var converterPoints = compact.ReadConverterPoints();

                return (short)(adjustFactor * (60 - (360 / (2 * Math.PI)) * Math.Atan2(converterPoints.ReactivePower, converterPoints.ActivePower)));
            }, 0, 5, 500);

            return (short)adjustResult.Value;

        }

        private ushort internalPowerGainsAdjust(string point)
        {
            var voltageSetPoint = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 220, ParamUnidad.V);
            var currentSetPoint = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var phaseGapSetPoint = Consignas.GetDouble(Params.PHASE.Null.Ajuste.Name, 0, ParamUnidad.Grados);
            var frequencySetPoint = Consignas.GetDouble(Params.FREQ.Null.Ajuste.TestPoint(point).Name, 50, ParamUnidad.Hz);

            internalMTESetPoint(voltageSetPoint, currentSetPoint, phaseGapSetPoint, frequencySetPoint);

            Delay(6000, "Espera lectura potencias");

            AdjustValueDef adjustValue = new AdjustValueDef() { Name = Params.GAIN_KW.Null.Ajuste.TestPoint(point).Name, Min = Params.GAIN_KW.Null.Ajuste.TestPoint(point).Min(), Max = Params.GAIN_KW.Null.Ajuste.TestPoint(point).Max(), Unidad = ParamUnidad.SinUnidad };

            var adjustResult = TestMeasureBase(adjustValue, (s) =>
                {
                    var converterPoints = compact.ReadConverterPoints();

                    return (ushort)(converterPoints.ActivePower / (voltageSetPoint * currentSetPoint));
                }, 0, 5, 500);

            return (ushort)adjustResult.Value;

        }

        private void internalVerification(string testPoint, double voltage, double current, double phaseGap)
        {
            AdjustValueDef valuesToCheck = new AdjustValueDef(Params.KW.Null.Verificacion.TestPoint(testPoint).Name, 0, 0, 0, 0, Params.KW.Null.Verificacion.TestPoint(testPoint).Tol(), ParamUnidad.SinUnidad);
            var testPointMultiplier = Configuracion.GetDouble(Params.PARAMETER("MULTIPLICADOR_VERIFICACION_" + testPoint, ParamUnidad.SinUnidad).Name, 0, ParamUnidad.SinUnidad);
            
            internalMTESetPoint(voltage, current, phaseGap, 50);

            Delay(6000, "Espera lectura potencias");

            TestCalibracionBase(valuesToCheck, 
                () =>
                {
                    var points = compact.ReadConverterPoints().ActivePower / (double)compact.ReadActivePower50HzGain();

                    return points;
                },
                () =>
                {
                    var voltagePatternReading = tower.PowerSourceIII.ReadVoltage().L1;
                    var currentPatternReading = tower.PowerSourceIII.ReadCurrent().L1;

                    return voltagePatternReading * currentPatternReading * Math.Cos(phaseGap.ToRadians());
                }, 5, 7, 1100,string.Format("Error al verificar el punto {0}, puntos de potencia del convertidor fuera de márgenes especificado", testPoint));
        }

        private int CaptureSerialNumberFromReader()
        {
            int serialNumberCaptured = 0;
            var serialNumberTemp = Shell.ShowDialog<int>("ENTRADA DEL NUMERO DE SERIE", () =>
            {
                return new InputKeyBoard("Leer el numero de serie impreso en el equipo");
            }
            , MessageBoxButtons.OKCancel, (c, d) =>
            {
                if (d != DialogResult.OK)
                    return 0;

                if (!CheckSerialNumberValidity(((InputKeyBoard)c).TextValue, out serialNumberCaptured))
                    throw new Exception("El numero de serie leido no es valido");
                return serialNumberCaptured;
            });

            return serialNumberTemp;
        }

        private bool CheckSerialNumberValidity(string serialNumber, out int serialNumberOut)
        {
            var serialNumberHeader = Identificacion.CABECERA_NUMERO_SERIE;
            var serialNumberLength = Identificacion.LONGITUD_NUMERO_SERIE;

            if (serialNumber.Length != serialNumberLength)
            {
                serialNumberOut = 0;
                return false;
            }
            if (!serialNumber.Substring(0, 3).Equals(serialNumberHeader.ToString()))
            {
                serialNumberOut = 0;
                return false;
            }
            if (!int.TryParse(serialNumber, out serialNumberOut))
            {
                serialNumberOut = 0;
                return false;
            }

            return true;
        }

        private void internalMTESetPoint(double voltage, double current, double gap, double frequency)
        {
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(TriLineValue.Create(voltage, 0, 0), TriLineValue.Create(current, 0, 0), frequency, TriLineValue.Create(gap, 0, 0), TriLineValue.Create(0, 120, 240));
        }

        private void internalMTEShutdown()
        {
            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }
    }
}
