﻿using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions.InstrumentsTools
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(ClearSignalGeneratorOutputActionDescription))]
   
    public class ShutdownSignalGenerator : ActionBase
    {
        public int Channel { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE");

            try
            {
                tower.SignalGenerator.Off(Channel);
            }
            catch (Exception e)
            {
                if (tower.SignalGenerator is IDisposable)
                    ((IDisposable)tower.SignalGenerator).Dispose();
                throw e;
            }
        }
    }

    public class ClearSignalGeneratorOutputActionDescription : ActionDescription
    {
        public override string Name { get { return "ShutdownSignalGeneratorOutput"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.ksicon16; } }
    }
}