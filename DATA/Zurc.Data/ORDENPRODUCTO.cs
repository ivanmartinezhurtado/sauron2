namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrdenProducto")]
    public partial class OrdenProducto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int OrdenId { get; set; }
        public DateTime Fecha { get; set; }
        public int? MatriculaId { get; set; }
        public string NumSerie { get; set; }
        public int? NumCaja { get; set; }
    }
}
