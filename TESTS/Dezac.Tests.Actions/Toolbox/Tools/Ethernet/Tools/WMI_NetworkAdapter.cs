﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Management;
using Dezac.Core.Utility;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(WMI_NetworkAdapterDescription))]
    public class WMI_NetworkAdapter : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre de la targeta de red")]
        public string NetWorkAdapter { get; set; }

        [Category("3. Configuration")]
        [Description("Habilitamos o deshabilitamos la targeta de red")]
        public bool Status { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            string comportInfo = string.Empty;


            var wmiQuery = new SelectQuery("SELECT * FROM Win32_NetworkAdapter WHERE NetConnectionId = '" + NetWorkAdapter + "'");
            var searchProcedure = new ManagementObjectSearcher(wmiQuery);

            foreach (ManagementObject item in searchProcedure.Get())
            {
                if (Status)
                    item.InvokeMethod("Enable", null);
                else
                    item.InvokeMethod("Disable", null);

                System.Threading.Thread.Sleep(2000);
            }

            testBase.SetVariable(NetWorkAdapter, Status ? "ENABLED" : "DISABLED");
        }
    }

    public class WMI_NetworkAdapterDescription : ActionDescription
    {
        public override string Name { get { return "WMINetworkAdapter"; } }
        public override string Category { get { return "Tools/Ethernet/Tools"; } }
        public override string Description { get { return "Este action sirve para adaptarse a una red"; } }
        public override Image Icon { get { return Properties.Resources.Tools_icon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}