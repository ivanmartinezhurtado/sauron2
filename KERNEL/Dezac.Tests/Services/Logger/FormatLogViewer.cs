﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace Dezac.Tests.Services.Logger
{
    public class FormatLogViewer
    {
        EventInfo root;
        static object syncObj = new object();
        public void BuildXml(string fileName, IEnumerable<LoggingEventWrapper> events, int? numInstance, LogsTypesEnum logType)
        {
            if (events == null || !events.Any())
                return;

            root = new EventInfo { Type = "test", Text = "Test" };
            events
                .Where(p => numInstance == null || p.NumInstance == numInstance)
                .OrderBy(p => p.TimeStamp)
                .ToList()
                .ForEach(p =>
                {
                    if (logType == LogsTypesEnum.RawByTimeLog)
                        AddEventPlain(p);
                    else
                        AddEvent(p);
                });

            using(var fs = File.Create(fileName))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(EventInfo));
                serializer.Serialize(fs, root);
            }
        }

        public string BuildXml(IEnumerable<LoggingEventWrapper> events, int? numInstance, LogsTypesEnum logType)
        {
            if (events == null || !events.Any())
                return null;

            root = new EventInfo { Type = "test", Text = "Test" };
            events
                .Where(p => numInstance == null || p.NumInstance == numInstance)
                .OrderBy(p => p.TimeStamp)
                .ToList()
                .ForEach(p =>
                {
                    if (logType == LogsTypesEnum.RawByTimeLog)
                        AddEventPlain(p);
                    else
                        AddEvent(p);
                });

            using (StringWriter textWriter = new StringWriter())
            {
                var settings = new XmlWriterSettings { CheckCharacters = false };

                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(EventInfo));
                    serializer.Serialize(xmlWriter, root);

                    return textWriter.ToString();
                }
            }
        }

        public void BuildHtml(Stream stream, LogsTypesEnum logType, IEnumerable<LoggingEventWrapper> events, XsltArgumentList argsList, int? numInstance)
        {
            lock (syncObj)
            {
                string xml = BuildXml(events, numInstance, logType);

                if (xml == null)
                    return;

                var myXPathDoc = new XPathDocument(new StringReader(xml));
                var myXslTrans = new XslCompiledTransform();
                var fileXmlToHtmlPathFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, XSLFilesDict[logType]);
                myXslTrans.Load(fileXmlToHtmlPathFile);
                using (var myWriter = new XmlTextWriter(stream, Encoding.UTF8))
                {
                    myXslTrans.Transform(myXPathDoc, argsList, myWriter);
                }
            }
        }

        public void BuildHtml(string fileName, LogsTypesEnum logType, IEnumerable<LoggingEventWrapper> events, XsltArgumentList argsList, int? numInstance)
        {
            lock (syncObj)
            {
                string xml = BuildXml(events, numInstance, logType);

                if (xml == null)
                    return;

                var myXPathDoc = new XPathDocument(new StringReader(xml));
                var myXslTrans = new XslCompiledTransform();
                var fileXmlToHtmlPathFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, XSLFilesDict[logType]);
                myXslTrans.Load(fileXmlToHtmlPathFile);
                using (var myWriter = new XmlTextWriter(fileName, Encoding.UTF8))
                {
                    myXslTrans.Transform(myXPathDoc, argsList, myWriter);
                }
            }
        }

        public void AddEvent(LoggingEventWrapper ev)
        {
            if (root == null)
                root = new EventInfo { Type = "test", Text = "Test", };

            var parent = GetParent(ev.StepPath);

            var item = new EventInfo
            {
                Type = "msg",
                Level = ev.Level.ToString(),
                LoggerName = ev.LoggerName.Replace(".", "-"),
                StepPath = ev.StepPath, 
                Text = ev.RenderedMessage,
                TimeStamp = ev.TimeStamp.ToString("HH:mm:ss.fff"),
                Depth = parent.Depth + 1,
                ThreadName = ev.ThreadName };

            if (ev.Result != null)
            {              
                parent.ExecutionTime = ev.ExecutionTime.TotalMilliseconds.ToString();
                item.Type = "struct";

                parent.Result = ev.Result;
            }

            parent.Events.Add(item);
        }
        public void AddEventPlain(LoggingEventWrapper ev)
        {
            if (root == null)
                root = new EventInfo { Type = "test", Text = "Test", };
            if (root.Events.Count == 0)
            {
                root.Events.Add(new EventInfo { Id = "Init", Text = "Init", Type = "step", Depth = 1, StepPath = "root" });
                root.Events.Add(new EventInfo { Id = "Main", Text = "Main", Type = "step", Depth = 1, StepPath = "root" });
                root.Events.Add(new EventInfo { Id = "End", Text = "End", Type = "step", Depth = 1, StepPath = "root" });
            }

            EventInfo parent = null;
            if (ev.StepPath.Contains("Init"))
                parent = root.Events.Where(e => e.Id == "Init").FirstOrDefault();
            if (ev.StepPath.Contains("Main"))
                parent = root.Events.Where(e => e.Id == "Main").FirstOrDefault();
            if (ev.StepPath.Contains("End"))
                parent = root.Events.Where(e => e.Id == "End").FirstOrDefault();

            var item = new EventInfo
            {
                Type = "msg",
                Level = ev.Level.ToString(),
                LoggerName = ev.LoggerName.Replace(".", "-"),
                StepPath = ev.StepPath,
                Text = ev.RenderedMessage,
                TimeStamp = ev.TimeStamp.ToString("HH:mm:ss.fff"),
                Depth = 2,
                ThreadName = ev.ThreadName
            };

            parent.Events.Add(item);
        }

        private List<EventInfo> GetStepList(EventInfo _event)
        {
            var eventsList = new List<EventInfo>();
            eventsList.Add(_event);
            foreach (EventInfo ev in _event.Events)
                eventsList.AddRange(GetStepList(ev));
            return eventsList;
        }

        public EventInfo GetParent(string path)
        {
            if (path == null || path == "(null)")
                return root;

            string[] tokens = path.Split(new string[] { " >" }, StringSplitOptions.None);

            EventInfo parent = root;

            foreach (var id in tokens)
            {
                var child = parent.Events.Where(p => p.Id == id).FirstOrDefault();
                if (child == null)
                {
                    child = new EventInfo { Id = id, Text = id, Type = "step", Depth = parent.Depth + 1 , StepPath = path};
                    parent.Events.Add(child);
                }

                parent = child;
            }

            return parent;
        }

        public enum LogsTypesEnum
        {
            RawByTimeLog,
            DebugLog,
            CheckReport,
        }

        private static readonly Dictionary<LogsTypesEnum, string> XSLFilesDict = new Dictionary<LogsTypesEnum, string>
        {
            {LogsTypesEnum.RawByTimeLog, "Services\\Logger\\RawByTimeLog.xsl"},
            {LogsTypesEnum.DebugLog, "Services\\Logger\\DebugLog.xsl"},
            {LogsTypesEnum.CheckReport, "Services\\Logger\\CheckReport.xsl"}
        };
    }
}
