﻿using Dezac.Core.Exceptions;
using Dezac.Tests;
using Dezac.Tests.Actions;
using Dezac.Tests.Extensions;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TaskRunner;
using Zurc.Data;

namespace Zurc.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(ImportTestResultsActionDescription))]
    public class ImportTestResultsAction : ActionBase
    {
        [Description("Identificador de la fase de test")]
        public string Fase { get; set; }
        [Description("Lista de parámetros a recuperar")]
        public string[] Parametros { get; set; }
        [Description("Añadir prefijo a los resultados importados")]
        public string Prefix { get; set; }
        [Description("Flag para activar la grabacion del valor y sus unidades como resultados del test")]
        public bool AddToResult { get; set; }
        [Description("Matricula donde obtener los resultados")]
        public string Matricula { get; set; }

        public override void Execute()
        {
            InternalExecute();
        }

        private void InternalExecute()
        {
            if (Parametros == null || !Parametros.Any())
                return;

            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            if (data == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.VARIABLE_ES_NULLA("NO EXISTE CONTEXTO DE TEST").Throw();

            if (string.IsNullOrEmpty(Matricula))
                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("FALTA EL Nº DE BASTIDOR").Throw();

            var matricula = ResolveValue(Matricula);

            using (var svc = new ZurcService())
            {
                var testFase = svc.GetLastTestData(Fase, Convert.ToInt32(matricula));
                if (testFase != null)
                {
                    foreach (var name in Parametros)
                    {
                        var valor = testFase.Valores.Where(p => p.Parametro.Nombre == name).FirstOrDefault();
                        if (valor != null)
                        {
                            var key = $"{Prefix}{name}";
                            SetVariable(key, valor.Valor);

                            if (AddToResult)
                                data.Resultados.Set(key, valor.Valor, (Dezac.Tests.Model.ParamUnidad)valor.Parametro.UnidadId);
                        }
                        else
                            Logger.Info($"Parámetro {name} no encontrado en test anterior!");
                    }
                }
                else
                    Logger.Info($"No se han encontrado resultados de test anterior para la matrícula {matricula} y fase {Fase}!");
            }
        }
    }

    public class ImportTestResultsActionDescription : ActionDescription
    {
        public override string Description { get { return "Action to retrieve previous test results values"; } }
        public override string Name { get { return "ImportTestResultsAction"; } }
        public override string Category { get { return "ZurcTest/Database"; } }
        public override Image Icon { get { return Properties.Resources.DatabaseParameters; } }
    }
}
