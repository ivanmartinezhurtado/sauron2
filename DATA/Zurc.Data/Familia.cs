namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Familia")]
    public partial class Familia
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(100)]
        public string Descripcion { get; set; }
        [DefaultValue(false)]
        public bool BloqueoTest { get; set; }
        [DefaultValue(false)]
        public bool Baja { get; set; }

        [StringLength(10)]
        public string EmpleadoId { get; set; }

        public virtual List<Articulo> Articulos { get; set; }
        public virtual List<GrupoParametrizacion> GruposParametrizacion { get; set; }
    }
}
