﻿namespace WinTaskRunner.Views
{
    partial class PlotForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlotForm));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.cmdTree = new Telerik.WinControls.UI.CommandBarButton();
            this.cmdViewTable = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.cmdClear = new Telerik.WinControls.UI.CommandBarButton();
            this.cmdLegend = new Telerik.WinControls.UI.CommandBarButton();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.treeView = new Telerik.WinControls.UI.RadTreeView();
            this.panelTree = new Telerik.WinControls.UI.SplitPanel();
            this.splitV = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.splitH = new Telerik.WinControls.UI.RadSplitContainer();
            this.panelGrid = new Telerik.WinControls.UI.SplitPanel();
            this.grid = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTree)).BeginInit();
            this.panelTree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitV)).BeginInit();
            this.splitV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitH)).BeginInit();
            this.splitH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).BeginInit();
            this.panelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(702, 30);
            this.radCommandBar1.TabIndex = 28;
            this.radCommandBar1.Text = "radCommandBar1";
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.cmdTree,
            this.cmdViewTable,
            this.cmdClear,
            this.cmdLegend});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // cmdTree
            // 
            this.cmdTree.DisplayName = "commandBarButton1";
            this.cmdTree.Image = global::WinTaskRunner.Properties.Resources.Schema;
            this.cmdTree.Name = "cmdTree";
            this.cmdTree.Text = "commandBarButton1";
            this.cmdTree.Click += new System.EventHandler(this.cmdTree_Click);
            // 
            // cmdViewTable
            // 
            this.cmdViewTable.DisplayName = "commandBarToggleButton1";
            this.cmdViewTable.Image = global::WinTaskRunner.Properties.Resources.table;
            this.cmdViewTable.Name = "cmdViewTable";
            this.cmdViewTable.Text = "Ver";
            this.cmdViewTable.Click += new System.EventHandler(this.cmdViewTable_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.DisplayName = "commandBarButton1";
            this.cmdClear.Image = global::WinTaskRunner.Properties.Resources.copyformat16;
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Text = "Clear";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // cmdLegend
            // 
            this.cmdLegend.DisplayName = "commandBarButton1";
            this.cmdLegend.Image = ((System.Drawing.Image)(resources.GetObject("cmdLegend.Image")));
            this.cmdLegend.Name = "cmdLegend";
            this.cmdLegend.Text = "Legend";
            this.cmdLegend.Click += new System.EventHandler(this.cmdLegend_Click);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.chart);
            this.splitPanel2.Location = new System.Drawing.Point(170, 0);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(532, 411);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2621776F, 0F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(179, 0);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // chart
            // 
            this.chart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chart.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            this.chart.BackSecondaryColor = System.Drawing.Color.PowderBlue;
            chartArea1.AxisX.LabelStyle.Format = "HH:mm:ss";
            chartArea1.AxisY.IsStartedFromZero = false;
            chartArea1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            chartArea1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chartArea1.BackSecondaryColor = System.Drawing.Color.PowderBlue;
            chartArea1.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea1);
            this.chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "ChartLegend";
            this.chart.Legends.Add(legend1);
            this.chart.Location = new System.Drawing.Point(0, 0);
            this.chart.Name = "chart";
            this.chart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Berry;
            this.chart.Size = new System.Drawing.Size(532, 411);
            this.chart.TabIndex = 18;
            // 
            // treeView
            // 
            this.treeView.CheckBoxes = true;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(166, 411);
            this.treeView.SpacingBetweenNodes = -1;
            this.treeView.TabIndex = 2;
            this.treeView.Text = "radTreeView1";
            this.treeView.NodeCheckedChanged += new Telerik.WinControls.UI.TreeNodeCheckedEventHandler(this.treeView_NodeCheckedChanged);
            // 
            // panelTree
            // 
            this.panelTree.Controls.Add(this.treeView);
            this.panelTree.Location = new System.Drawing.Point(0, 0);
            this.panelTree.Name = "panelTree";
            // 
            // 
            // 
            this.panelTree.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.panelTree.Size = new System.Drawing.Size(166, 411);
            this.panelTree.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.2621776F, 0F);
            this.panelTree.SizeInfo.SplitterCorrection = new System.Drawing.Size(-179, 0);
            this.panelTree.TabIndex = 0;
            this.panelTree.TabStop = false;
            this.panelTree.Text = "splitPanel1";
            // 
            // splitV
            // 
            this.splitV.Controls.Add(this.splitPanel3);
            this.splitV.Controls.Add(this.panelGrid);
            this.splitV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitV.Location = new System.Drawing.Point(0, 30);
            this.splitV.Name = "splitV";
            this.splitV.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.splitV.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitV.Size = new System.Drawing.Size(702, 528);
            this.splitV.TabIndex = 31;
            this.splitV.TabStop = false;
            this.splitV.Text = "radSplitContainer2";
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.splitH);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel3.Size = new System.Drawing.Size(702, 411);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.2843511F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 149);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // splitH
            // 
            this.splitH.Controls.Add(this.panelTree);
            this.splitH.Controls.Add(this.splitPanel2);
            this.splitH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitH.Location = new System.Drawing.Point(0, 0);
            this.splitH.Name = "splitH";
            // 
            // 
            // 
            this.splitH.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitH.Size = new System.Drawing.Size(702, 411);
            this.splitH.TabIndex = 33;
            this.splitH.TabStop = false;
            this.splitH.Text = "radSplitContainer3";
            // 
            // panelGrid
            // 
            this.panelGrid.Controls.Add(this.grid);
            this.panelGrid.Location = new System.Drawing.Point(0, 415);
            this.panelGrid.Name = "panelGrid";
            // 
            // 
            // 
            this.panelGrid.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.panelGrid.Size = new System.Drawing.Size(702, 113);
            this.panelGrid.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2843511F);
            this.panelGrid.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -149);
            this.panelGrid.TabIndex = 1;
            this.panelGrid.TabStop = false;
            this.panelGrid.Text = "splitPanel4";
            // 
            // grid
            // 
            this.grid.BackColor = System.Drawing.Color.AliceBlue;
            this.grid.Cursor = System.Windows.Forms.Cursors.Default;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.grid.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grid.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.grid.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.grid.MasterTemplate.AllowAddNewRow = false;
            this.grid.MasterTemplate.AllowDeleteRow = false;
            this.grid.MasterTemplate.AllowEditRow = false;
            this.grid.MasterTemplate.AutoGenerateColumns = false;
            this.grid.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "Source";
            gridViewTextBoxColumn1.HeaderText = "Source";
            gridViewTextBoxColumn1.Name = "Source";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn1.Width = 98;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "Name";
            gridViewTextBoxColumn2.HeaderText = "Serie";
            gridViewTextBoxColumn2.Name = "Serie";
            gridViewTextBoxColumn2.Width = 205;
            gridViewTextBoxColumn3.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn3.EnableExpressionEditor = false;
            gridViewTextBoxColumn3.FieldName = "Time";
            gridViewTextBoxColumn3.HeaderText = "Timestamp";
            gridViewTextBoxColumn3.IsAutoGenerated = true;
            gridViewTextBoxColumn3.Name = "Time";
            gridViewTextBoxColumn3.Width = 273;
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.FieldName = "Value";
            gridViewTextBoxColumn4.HeaderText = "Valor";
            gridViewTextBoxColumn4.IsAutoGenerated = true;
            gridViewTextBoxColumn4.Name = "Valor";
            gridViewTextBoxColumn4.Width = 109;
            this.grid.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.grid.MasterTemplate.EnableAlternatingRowColor = true;
            this.grid.MasterTemplate.EnableGrouping = false;
            this.grid.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grid.Size = new System.Drawing.Size(702, 113);
            this.grid.TabIndex = 30;
            this.grid.Text = "radGridView1";
            this.grid.ThemeName = "ControlDefault";
            // 
            // PlotForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 558);
            this.Controls.Add(this.splitV);
            this.Controls.Add(this.radCommandBar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PlotForm";
            this.Text = "Plot View";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PlotForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTree)).EndInit();
            this.panelTree.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitV)).EndInit();
            this.splitV.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitH)).EndInit();
            this.splitH.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).EndInit();
            this.panelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarToggleButton cmdViewTable;
        private Telerik.WinControls.UI.CommandBarButton cmdClear;
        private Telerik.WinControls.UI.CommandBarButton cmdLegend;
        private Telerik.WinControls.UI.CommandBarButton cmdTree;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart;
        private Telerik.WinControls.UI.RadTreeView treeView;
        private Telerik.WinControls.UI.SplitPanel panelTree;
        private Telerik.WinControls.UI.RadSplitContainer splitV;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadSplitContainer splitH;
        private Telerik.WinControls.UI.SplitPanel panelGrid;
        private Telerik.WinControls.UI.RadGridView grid;
    }
}
