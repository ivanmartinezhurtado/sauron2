﻿using Dezac.Core.Utility;
using Dezac.Device;
using Dezac.Device.Protection;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.PowerSource;
using Instruments.Towers;
using System;


namespace Dezac.Tests.Protection
{
    [TestVersion(1.01)]
    public class Fusible_CBT_Test : TestBase
    {
        public const byte IN_DEVICE_PRESENCE = 9;

        public const byte OUT_EV_GENERAL = 4;
        public const byte OUT_PUNTA_TOROIDAL = 5;
        public const byte OUT_ALIMENTACION_VBTI = 15;
        public const byte OUT_RESET = 17;
        public const byte OUT_SIMULACION_FUSIBLE = 18;
        public const byte OUT_PILOTO_ERROR = 21;
        public const byte _24V = 14;

        private VBTi vbti;
        public VBTi Vbti
        {
            get
            {
                if (vbti == null || !vbti.Modbus.IsConnected)
                {
                    this.SamplerWithCancel((p) =>
                    {
                        vbti = new VBTi(1004, "192.168.1.105", ModbusTCPProtocol.TCP);
                        return true;
                    }, "", 3, 2000, 100, false, false);

                    vbti.Modbus.TimeOut = 5000;
                    vbti.Modbus.PerifericNumber = 2; // Comunicaciones.Periferico2;
                    vbti.Modbus.UseFunction06 = true;
                }
                return vbti;
            }
        }

        private Tower3 tower;
        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }
        }

        public void TestInitialization()
        {
            Tower.IO.DO.On(_24V);

            SamplerWithCancel((p) =>
            {
                if (!Tower.IO.DI[IN_DEVICE_PRESENCE])
                    return false;

                return true;
            }, "Error No se ha detectado el equipo o util abierto", 5, 500, 1500, false, false);

            
            Tower.IO.DO.On(OUT_EV_GENERAL, OUT_PUNTA_TOROIDAL);
            Tower.IO.DO.Off(OUT_PILOTO_ERROR);

            Tower.IO.DO.Off(OUT_RESET);
            Tower.IO.DO.Off(OUT_SIMULACION_FUSIBLE);
            Tower.IO.DO.OnWait(4000, OUT_ALIMENTACION_VBTI);
            
            Tower.MTEPS.Output120A = true;
            Tower.IO.DO.On(45);
        }

        public void TestFusible()
        {
            Tower.IO.DO.Off(OUT_RESET);  

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 0, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 0, ParamUnidad.Grados);

            ConsignarMTE(adjustVoltage, adjustCurrent, powerFactor, 50);

            this.SamplerWithCancel((p) =>
            {
                Vbti.WriteReset();
                return true;
            }, "Error: El equipo VBTi no comuncia");

            this.SamplerWithCancel((p) =>
            {
                return Vbti.ReadNumeroFusiblesConectado() == 0;

            }, "Error: No se ha podido borrar el mapa de fusibles conectados", 2, 1000, 100, false, false);

            RewriteCBTRegister();

            var measure = new VBTi.MeasureStuctre();       

            this.SamplerWithCancel((p) =>
            {
                Tower.IO.DO.Off(OUT_SIMULACION_FUSIBLE);

                Delay(500, "Conmutación rele");

                measure = Vbti.ReadMeasureCurrent();

                return measure.status == VBTi.FuseStatus.FUSED;             
            }, "Error: no se ha podido leer el Fusible CBT en estado simulado fundido", 10, 1000, 100, false, false);

            var IAdjustBBDD = new AdjustValueDef(Params.I.Null.Ajuste.Name, 0, Params.I.Null.Ajuste.Min(), Params.I.Null.Ajuste.Max(), 0, 0, ParamUnidad.A);

            Tower.IO.DO.PulseOn(2000, OUT_RESET);

            Tower.IO.DO.On(OUT_SIMULACION_FUSIBLE);

            Delay(500, "Conmutación rele");

            TestMeasureBase(IAdjustBBDD, (step) =>
            {
                this.SamplerWithCancel((p) =>
                {
                    RewriteCBTRegister();

                    measure = Vbti.ReadMeasureCurrent();

                    if (measure.status != VBTi.FuseStatus.OK)
                    {
                        Logger.WarnFormat(String.Format("Reintento: El fusible da un error ({0}) en la medida)", measure.status.ToString()));
                        return false;
                    }
                    return true;
                }, String.Format("Error: El fusible da un error ({0}) en la medida)", measure.status.ToString()), 60, 4000, 2000, false, false);

                var IAdjust = measure.current;

                return IAdjust;
            }, 0, 5, 1000);
        }

        public void TestVerification()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 0, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 100, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, 0, ParamUnidad.Grados);

            ConsignarMTE(adjustVoltage, adjustCurrent, powerFactor, 50);

            var IVerifBBDD = new AdjustValueDef(Params.I.Null.Verificacion.Name, 0, Params.I.Null.Verificacion.Min(), Params.I.Null.Verificacion.Max(), 0, 0, ParamUnidad.A);

            Tower.IO.DO.PulseOn(2000, OUT_RESET);

            RewriteCBTRegister();

            TestMeasureBase(IVerifBBDD, (step) =>
            {
                var measure = new VBTi.MeasureStuctre();
                this.SamplerWithCancel((p) =>
                {
                    RewriteCBTRegister();

                    measure = Vbti.ReadMeasureCurrent();

                    if (measure.status != VBTi.FuseStatus.OK)
                    {
                        Logger.WarnFormat(String.Format("Reintento: El fusible da un error ({0}) en la medida)", measure.status.ToString()));
                        return false;
                    }
                    return true;
                }, String.Format("Error: El fusible da un error ({0}) en la medida)", measure.status.ToString()), 60, 4000, 2500, false, false);

                var IAdjust = measure.current;

                return IAdjust;

            }, 0, 25, 3000);
        }

        public void TestFinish()
        {
            if (Tower.MTEPS != null)
            {
                Tower.MTEPS.ApplyOffAndWaitStabilisation();
                Tower.MTEPS.Dispose();
            }

            if (vbti != null)
                vbti.Dispose();

            if (Tower.IO != null)
            {
                Tower.IO.DO.OffWait(200, OUT_PUNTA_TOROIDAL, OUT_ALIMENTACION_VBTI, OUT_RESET, OUT_SIMULACION_FUSIBLE);

                Tower.IO.DO.Off(OUT_EV_GENERAL, _24V);

                if (this.IsTestError)
                    Tower.IO.DO.On(OUT_PILOTO_ERROR);

                Tower.IO.Dispose();
            }

          
        }

        //****************************************************

        private void ConsignarMTE(double tension, double corriente, double pf, double frecuencia)
        {
            var voltage = TriLineValue.Create(tension, 0, 0);
            var current = TriLineValue.Create(corriente, 0, 0);
            var gap = TriLineValue.Create(pf, 0, 0);

            Tower.MTEPS.ApplyPresetsAndWaitStabilisation(voltage, current, frecuencia, gap, TriLineValue.Create(0, 120, 240), false);
        }

        private void RewriteCBTRegister()
        {
            this.SamplerWithCancel((p) =>
            {
                var fuseRegister = Vbti.ReadRegisterFusibleIdentification();

                if (fuseRegister.SerialNumber.ToString() != TestInfo.NumSerie)
                    Vbti.WriteCBTRegister(UInt32.Parse(TestInfo.NumSerie));

                return true;
            }, "Error: No se ha podido verificar el registro del fusible CBT en el VBTi", 2, 1000, 100, false, false);
        }

    }
}