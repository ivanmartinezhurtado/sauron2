﻿namespace Dezac.Instruments.Controls
{
    partial class UC_MTX
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnV = new System.Windows.Forms.Label();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.btnA = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnModeAC = new System.Windows.Forms.Button();
            this.btnModeDC = new System.Windows.Forms.Button();
            this.btnModeACDC = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.cmbCom = new System.Windows.Forms.ComboBox();
            this.lblCom = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnV
            // 
            this.btnV.BackColor = System.Drawing.Color.Transparent;
            this.btnV.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnV.Location = new System.Drawing.Point(315, 126);
            this.btnV.Name = "btnV";
            this.btnV.Size = new System.Drawing.Size(29, 29);
            this.btnV.TabIndex = 0;
            this.btnV.Click += new System.EventHandler(this.btnReadVoltage_Click);
            this.btnV.MouseEnter += new System.EventHandler(this.btnV_MouseEnter);
            this.btnV.MouseLeave += new System.EventHandler(this.btnV_MouseLeave);
            // 
            // txtResult
            // 
            this.txtResult.BackColor = System.Drawing.SystemColors.WindowText;
            this.txtResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResult.ForeColor = System.Drawing.Color.LimeGreen;
            this.txtResult.Location = new System.Drawing.Point(48, 46);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.ReadOnly = true;
            this.txtResult.Size = new System.Drawing.Size(173, 155);
            this.txtResult.TabIndex = 1;
            this.txtResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnA
            // 
            this.btnA.BackColor = System.Drawing.Color.Transparent;
            this.btnA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA.Location = new System.Drawing.Point(319, 89);
            this.btnA.Name = "btnA";
            this.btnA.Size = new System.Drawing.Size(26, 28);
            this.btnA.TabIndex = 2;
            this.btnA.Click += new System.EventHandler(this.btnReadCurrent_Click);
            this.btnA.MouseEnter += new System.EventHandler(this.btnV_MouseEnter);
            this.btnA.MouseLeave += new System.EventHandler(this.btnV_MouseLeave);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Location = new System.Drawing.Point(341, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 21);
            this.label1.TabIndex = 3;
            this.label1.Click += new System.EventHandler(this.btnReadTemperature_Click);
            this.label1.MouseEnter += new System.EventHandler(this.btnV_MouseEnter);
            this.label1.MouseLeave += new System.EventHandler(this.btnV_MouseLeave);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Location = new System.Drawing.Point(378, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 21);
            this.label2.TabIndex = 4;
            this.label2.Click += new System.EventHandler(this.btnReadCapacitance_Click);
            this.label2.MouseEnter += new System.EventHandler(this.btnV_MouseEnter);
            this.label2.MouseLeave += new System.EventHandler(this.btnV_MouseLeave);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label3.Location = new System.Drawing.Point(380, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 21);
            this.label3.TabIndex = 5;
            this.label3.Click += new System.EventHandler(this.btnReadResistance_Click);
            this.label3.MouseEnter += new System.EventHandler(this.btnV_MouseEnter);
            this.label3.MouseLeave += new System.EventHandler(this.btnV_MouseLeave);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label4.Location = new System.Drawing.Point(342, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 21);
            this.label4.TabIndex = 6;
            this.label4.Click += new System.EventHandler(this.btnReadFrequency_Click);
            this.label4.MouseEnter += new System.EventHandler(this.btnV_MouseEnter);
            this.label4.MouseLeave += new System.EventHandler(this.btnV_MouseLeave);
            // 
            // btnModeAC
            // 
            this.btnModeAC.BackColor = System.Drawing.Color.Transparent;
            this.btnModeAC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnModeAC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModeAC.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnModeAC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModeAC.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnModeAC.Location = new System.Drawing.Point(220, 161);
            this.btnModeAC.Name = "btnModeAC";
            this.btnModeAC.Size = new System.Drawing.Size(23, 37);
            this.btnModeAC.TabIndex = 7;
            this.btnModeAC.Tag = "";
            this.btnModeAC.Text = "AC";
            this.btnModeAC.UseVisualStyleBackColor = false;
            this.btnModeAC.Click += new System.EventHandler(this.btnMode_Click);
            // 
            // btnModeDC
            // 
            this.btnModeDC.BackColor = System.Drawing.Color.Transparent;
            this.btnModeDC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModeDC.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnModeDC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModeDC.Location = new System.Drawing.Point(220, 125);
            this.btnModeDC.Name = "btnModeDC";
            this.btnModeDC.Size = new System.Drawing.Size(23, 35);
            this.btnModeDC.TabIndex = 8;
            this.btnModeDC.Tag = "DC";
            this.btnModeDC.Text = "DC";
            this.btnModeDC.UseVisualStyleBackColor = false;
            this.btnModeDC.Click += new System.EventHandler(this.btnMode_Click);
            // 
            // btnModeACDC
            // 
            this.btnModeACDC.BackColor = System.Drawing.Color.Transparent;
            this.btnModeACDC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModeACDC.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnModeACDC.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModeACDC.Location = new System.Drawing.Point(220, 86);
            this.btnModeACDC.Name = "btnModeACDC";
            this.btnModeACDC.Size = new System.Drawing.Size(23, 37);
            this.btnModeACDC.TabIndex = 9;
            this.btnModeACDC.Tag = "ACDC";
            this.btnModeACDC.Text = "ACDC";
            this.btnModeACDC.UseVisualStyleBackColor = false;
            this.btnModeACDC.Click += new System.EventHandler(this.btnMode_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.Transparent;
            this.btnReset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReset.Location = new System.Drawing.Point(436, 156);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(17, 23);
            this.btnReset.TabIndex = 10;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // cmbCom
            // 
            this.cmbCom.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCom.FormattingEnabled = true;
            this.cmbCom.Location = new System.Drawing.Point(419, 223);
            this.cmbCom.Name = "cmbCom";
            this.cmbCom.Size = new System.Drawing.Size(89, 21);
            this.cmbCom.TabIndex = 11;
            this.cmbCom.SelectedIndexChanged += new System.EventHandler(this.cmbCom_SelectedIndexChanged);
            // 
            // lblCom
            // 
            this.lblCom.AutoSize = true;
            this.lblCom.BackColor = System.Drawing.Color.Transparent;
            this.lblCom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCom.Location = new System.Drawing.Point(373, 226);
            this.lblCom.Name = "lblCom";
            this.lblCom.Size = new System.Drawing.Size(47, 17);
            this.lblCom.TabIndex = 12;
            this.lblCom.Text = "COM:";
            // 
            // UC_MTX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::Dezac.Instruments.Properties.Resources.MTX3293;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.lblCom);
            this.Controls.Add(this.cmbCom);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnModeACDC);
            this.Controls.Add(this.btnModeDC);
            this.Controls.Add(this.btnModeAC);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnA);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.btnV);
            this.Name = "UC_MTX";
            this.Size = new System.Drawing.Size(518, 248);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label btnV;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Label btnA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnModeAC;
        private System.Windows.Forms.Button btnModeDC;
        private System.Windows.Forms.Button btnModeACDC;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ComboBox cmbCom;
        private System.Windows.Forms.Label lblCom;
    }
}
