﻿using System.Windows;

namespace WinTaskRunner.Views
{
    public partial class AddSequenceVersionView : Window
    {
        public MessageBoxResult Result { get; private set; } = MessageBoxResult.None;
        public string Description { get; private set; }

        public AddSequenceVersionView(string sequenceDescription)
        {
            InitializeComponent();
            labelSequenceName.Content = sequenceDescription;
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.OK;
            Description = textBoxDescription.Text;
            Close();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Cancel;
            Close();
        }
    }
}
