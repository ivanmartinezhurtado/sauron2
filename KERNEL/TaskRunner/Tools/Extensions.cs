﻿using System;

namespace TaskRunner.Tools
{
    public static class Extensions
    {
        public static Exception GetInnerException(this Exception ex)
        {
            if (ex == null)
                return null;

            while (ex.InnerException != null)
                ex = ex.InnerException;

            return ex;
        }
    }
}
