﻿using Dezac.Core.Utility;
using System.Threading;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.01)]
    public class CBT : DeviceBase
    {
        #region Instanciacion y destrucción de la clase del equipo

        public CBT()
        {
        }

        public CBT(int port, int baudrate)
        {           
            SetPort(port, baudrate);
        }

        public void SetPort(int port, int baudrate, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudrate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);      
            else
                Modbus.PortCom = port;

            Modbus.UseFunction06 = true;
            Modbus.PerifericNumber = periferic;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #endregion

        #region Operaciones de lectura

        public string ReadFirmwareVersion()             
        {
            LogMethod();
            var version = Modbus.ReadRegister((ushort)Registers.MODELO_VERSION);
            return version.ToString();
        }

        public uint ReadNumSerie()
        {
            LogMethod();
            ushort[] num = { Modbus.ReadRegister((ushort)Registers.NUM_SERIE_HI), Modbus.ReadRegister((ushort)Registers.NUM_SERIE_LOW) };
            return num.JoinRegisters<uint>();
        }

        public uint ReadNumBastidor()
        {
            LogMethod();
            ushort[] num = { Modbus.ReadRegister((ushort)Registers.NUM_BASTIDOR_HI), Modbus.ReadRegister((ushort)Registers.NUM_BASTIDOR_LOW) };
            return num.JoinRegisters<uint>();
        }

        public double ReadCurrent()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CURRENT, (ushort)0x1111);
            Thread.Sleep(500);
            double current = Modbus.ReadRegister((ushort)Registers.CURRENT);
            return current / 10.0;
        }

        public ushort ReadVh()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.V_H, (ushort)0x1111);
            Thread.Sleep(500);
            ushort voltage = Modbus.ReadRegister((ushort)Registers.V_H);
            return voltage;
        }

        public double ReadVcpu()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.V_CPU, (ushort)0x1111);
            Thread.Sleep(500);
            double voltage = Modbus.ReadRegister((ushort)Registers.V_CPU);
            return voltage / 100.0;
        }

        public ushort ReadVfuse()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.V_FUSE, (ushort)0x1111);
            Thread.Sleep(500);
            ushort voltage = Modbus.ReadRegister((ushort)Registers.V_FUSE);
            return voltage;
        }

        public ushort ReadAlarms()
        {
            LogMethod();
            var alarm = Modbus.ReadRegister((ushort)Registers.ALARMA);
            return alarm;
        }

        public ushort ReadClockConstant()
        {
            LogMethod();
            var cte = Modbus.ReadRegister((ushort)Registers.CTE_CRISTAL);
            return cte;
        }

        #endregion

        #region Operaciones de escritura

        public void WriteNumSerie(uint num)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.NUM_SERIE_HI, num.ToUshorts()[0]);
            Thread.Sleep(500);
            Modbus.Write((ushort)Registers.NUM_SERIE_LOW, num.ToUshorts()[1]);
        }

        public void WriteNumBastidor(uint num)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.NUM_BASTIDOR_HI, num.ToUshorts()[0]);
            Thread.Sleep(500);
            Modbus.Write((ushort)Registers.NUM_BASTIDOR_LOW, num.ToUshorts()[1]);
        }

        public void WriteReset()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RESET, 0x1111);
        }

        public void WriteClockConstant(ushort cte)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CTE_CRISTAL, cte);
        }

        #endregion

        #region Enums

        public enum Registers
        {
            NUM_SERIE_HI = 0x0000,
            NUM_SERIE_LOW = 0x0001,
            NUM_BASTIDOR_HI = 0X0002,
            NUM_BASTIDOR_LOW = 0x0003,
            ERROR_IDP = 0x0004,
            CTE_CRISTAL = 0x0005,
            RESET = 0x0010,
            ACTUALIZA_CLOCK =0x0011,
            CURRENT = 0x0012,
            V_H = 0x0013,
            V_CPU = 0x0014,
            V_FUSE = 0x015,
            MODELO_VERSION = 0x0016,
            ALARMA = 0x0017,
        }


        #endregion

        #region Structures 

        #endregion

        #region Private functions

        #endregion

    }
}
