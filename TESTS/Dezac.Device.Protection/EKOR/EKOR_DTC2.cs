﻿using Comunications.Message;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Globalization;
using System.Text;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.05)]
    public class EKOR_DTC2 : DeviceBase
    {

        public CirbusDeviceSerialPort Cirbus { get; internal set; }

        public EKOR_DTC2()
        {
        }

        public EKOR_DTC2(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 4800, byte periferic = 0)  //port9=Cirbus Port5=cirbusTTL Port6=modbus
        {
            if (Cirbus == null)
                Cirbus = new CirbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Cirbus.PortCom = port;

            Cirbus.DtrEnable = true;
            Cirbus.RtsEnable = true;

            Cirbus.PerifericNumber = periferic;
            Cirbus.TimeOut *= 2;
            //Cirbus.
        }

        // COMANDOS IDENTIFICACION DEL EQUIPO
        private const string READVERSIONFIRMWARE = "VER";
        private const string READVERSIONDEVELOPMENT = "VDP";
        private const string WRITEBASTIDOR = "ENS{0}";
        private const string READBASTIDOR = "LNS";
        private const string READCPU = "CPU";

        //COMANDOS DE COMPROVACION DEL EQUIPO
        private const string TESTDISPLAY = "TDI";
        private const string TESTDISPLAY2 = "TD2";
        private const string READKEYBOARD = "TEC";
        private const string READINPUT = "ENT";
        private const string WRITEOUTPUT = "SAL{0}";
        private const string READMEASUREDCURRENTDISPLAY = "LPU";
        private const string READMEASUREDVOLTAGEDISPLAY = "LEV";
        private const string READSUMATORYCICLES = "LSC 0032";
        private const string STANDBYE = "STB";

        // COMANDOS CALIBRACIÓN
        private const string WRITECURRENTFACTORSCALIBRATION = "EFC{0}";
        private const string READCURRENTFACTORSCALIBRATION = "LFC{0}";
        private const string WRITEVOLTAGEFACTORSCALIBRATION = "EFV{0}";
        private const string READVOLTAGEFACTORSCALIBRATION = "LFV{0}";

        private const string WRITECALIBRATIONPASATAPAS = "EPT{0:0000000} {1:0000000} {2:0000000}"; // 3 CANALS (V1, V2, V3)
        private const string READCALIBRATIONPASATAPAS = "LPT";

        private const string CLOCKCALIBRATION = "CLK DE2ACFAB S015";
        private const string GENERALCLOCKCALIBRATION = "CLK DE2ACFAB S{0:000}";    // genera una senyal de pulsos inhabilitando comunicaciones
        private const string SENDTEMPERATUREANDFREQUENCY = "CLK F{0} T{0}";
        private const string READCALIBRATIONCLOCK = "CLK I";
        private const string READTEMPERATURE = "CLK T";
        private const string READCOUNTTICKSLASTTEST = "CLK N";

        // COMANDOS PARAMETRIZACIÓN DEL EQUIPO
        private const string WRITEINSTALLERSETTINGS = "EAI{0}";
        private const string WRITEUSERSETTINGS = "EAU{0}";
        private const string WRITECLOCKSETTINGS = "ERL{0:00}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}";
        private const string READINSTALLERSETTINGS = "LAI";
        private const string READUSERSETTINGS = "LAU";
        private const string READCLOCKSETTINGS = "LRL";

        private const string GETDIRECTIONALPHASE = "GCF";  // Obtiene angulo entre Vh y Ih
        private const string READPHASEMEASUREDANGLE = "GF{0}";
        private const string GETMAXMINDIRECTIONALPHASE = "GFI";   // Obtiene angulo max y min entre Vh y Ih 
        private const string DIRECTIONALPHASECOMPROVATION = "CFI";  // Comprueba que el ángulo esté entre -180º i 180º
        private const string POWERFACTORS = "EFP";
        private const string READDIRECTIONALPHASE = "LAD";  // COMPROVAR QUE ESTÁ OPERATIVO
        private const string WRITE_ANGLE_DEFAULT = "ECA 0000 0000 0000";


        // COMANDOS DE DISPAROS I TABLAS DE LA VERDAD
        private const string READHISTORICTRIGGER = "HIS";
        private const string VALIDATETRIGGER = "VDI";
        private const string COUNTERANDTRIPDELETE = "BCO";
        private const string REMOVECOUNTERS = "CLM";

        private const string INITIALIZEPOINTER = "IPL";
        private const string WRITELOGICPROGRAMMINGTABLE = "EPL{0}";
        private const string READLOGICPROGRAMMINGTABLE = "LPL";
        private const string RECORDLOGICPROGRAMMINGTABLE = "GPL";
        private const string READEEPROM = "LEE";

        // OTROS COMANDOS
        private const string BUFFERCOUNTER = "DB1";
        private const string DATACRASH = "DB2";
        private const string PRIORITYSYSTEMHANDLERS = "DB3";

        private const string IMPXML = "XML IMP {0}";
        private const string EXPXML = "XML EXP {0}";

        private const string READFLASHSTATUS = "FAT I";
        private const string FORMATFLASH = "FAT FORMAT";
        private const string FLASHIMP = "FAT IMP {0}";
        private const string FLASHEXP = "FAT EXP {0}";

        /*******************************************************************************************************************************************/

        public string ReadVersion()
        {
            LogMethod();
            return Cirbus.Read(READVERSIONDEVELOPMENT);
        }

        public string ReadFirmware()
        {
            LogMethod();
            return Cirbus.Read(READVERSIONFIRMWARE, true);
        }

        public string ReadCPU()
        {
            LogMethod();
            return Cirbus.Read(READCPU, true);
        }

        public void ReadyDisplay()
        {
            LogMethod();
            Cirbus.Write(TESTDISPLAY);
        }

        public void ReadyDisplay2()              // Preguntar si esta comprobación se debe hacer o no...
        {
            LogMethod();
            Cirbus.Write(TESTDISPLAY2);
        }

        public string ReadUserSettings()
        {
            LogMethod();
            return Cirbus.Read(READUSERSETTINGS, listErrorCirbus);
        }

        public UserSettings ReadUserSettingsStruct()
        {
            LogMethod();
            var x = Cirbus.Read<UserSettings>(READUSERSETTINGS, listErrorCirbus);

            var settingsPrueba = new UserSettings();
            settingsPrueba.ProgrammerByTablePower = 255;
            settingsPrueba.ProgrammerByTableVoltage = 255;
            settingsPrueba.CurvePhase = x.CurvePhase;
            settingsPrueba.CurveHomopolar = x.CurveHomopolar;
            settingsPrueba.NominalCurrent = x.NominalCurrent;
            settingsPrueba.InstantTriggerPhase = x.InstantTriggerPhase;
            settingsPrueba.InstantTriggerHomopolar = x.InstantTriggerHomopolar;
            settingsPrueba.OverloadPhase = x.OverloadPhase;
            settingsPrueba.OverloadHomopolar = x.OverloadHomopolar;
            settingsPrueba.KPhase = x.KPhase;
            settingsPrueba.KHomopolar = x.KHomopolar;
            settingsPrueba.ShortcircuitPhase = x.ShortcircuitPhase;
            settingsPrueba.ShortcircuitHomopolar = x.ShortcircuitHomopolar;
            settingsPrueba.TimeShortCircuitPhase = x.TimeShortCircuitPhase;
            settingsPrueba.TimeShortCircuitHomopolar = x.TimeShortCircuitHomopolar;
            settingsPrueba.Password = x.Password;

            var trama = settingsPrueba.ToCirbus;
            var readTrama = ReadUserSettings();
            if (trama.ToUpper() == readTrama.ToUpper().TrimEnd())
                throw new Exception("Funciona");


            return x;
        }

        public InstallerSettings ReadInstallerSettingsStruct()
        {
            LogMethod();

            return Cirbus.Read<InstallerSettings>(READINSTALLERSETTINGS, listErrorCirbus);     
        }

        public string ReadInstallerSettings()
        {
            LogMethod();
            return Cirbus.Read(READINSTALLERSETTINGS);
        }

        public string ReadBastidor()
        {
            LogMethod();
            return Cirbus.Read(READBASTIDOR, listErrorInputs);
        }

        public double ReadHomopolarPhase()
        {
            LogMethod();
            return Cirbus.Read<double>(GETMAXMINDIRECTIONALPHASE);
        }

        public double ReadDirectionalPhase()
        {
            LogMethod();
            return Cirbus.Read<double>(GETDIRECTIONALPHASE);
        }

        public string ReadFLASHInformation()
        {
            LogMethod();
            return Cirbus.Read(READFLASHSTATUS, listErrorFAT);
        }

        public string ValidateTrigger()
        {
            LogMethod();
            return Cirbus.Read(VALIDATETRIGGER, true);
        }

        public enumInputs ReadInputs(int numInputsEkor)
        {
            LogMethod();
            var input = Cirbus.Read(READINPUT, listErrorInputs);
            Inputs inputsReading = new Inputs(Convert.ToInt32(input, 16), numInputsEkor);

            return inputsReading.StateInputs();
        }

        public int ReadInputsCI()
        {
            LogMethod();
            var input = Cirbus.Read(READINPUT, listErrorInputs);
            return int.Parse(input, NumberStyles.HexNumber);
        }

        public Keyboard ReadKeyboard()
        {
            LogMethod();
            var keyboardReading = Cirbus.Read(READKEYBOARD);

            var result = Convert.ToInt32(keyboardReading.Replace("ACK", string.Empty), 16);

            if (Enum.IsDefined(typeof(Keyboard), result))
                return (Keyboard)result;
            else
                return Keyboard.NO_BUTTON_OR_INPUT;
        }

        public MeasurePointsStructure? ReadRmsMeasurePoints()   // Comando LSC se debe leer dos veces, en la primera tenemos que obtener ACK (OK) y en la segunda se obtienen los valores de los puntos
        {
            LogMethod();

            var resp = Cirbus.Read(READSUMATORYCICLES);

            if (resp.Contains("ACK"))
            {
                System.Threading.Thread.Sleep(1000);
                return Cirbus.Read<MeasurePointsStructure>(READSUMATORYCICLES);
            }
            else
                return null;
        }

        public FactorsCalibrationCurrents ReadCurrentFactorCalibration()
        {
            LogMethod();
            var resp = Cirbus.Read<FactorsCalibrationCurrents>(READCURRENTFACTORSCALIBRATION, listErrorCirbus);
            return resp;
        }

        public FactorsCalibrationVoltages ReadVoltageFactorCalibration()
        {
            LogMethod();
            var resp = Cirbus.Read<FactorsCalibrationVoltages>(READVOLTAGEFACTORSCALIBRATION, listErrorCirbus);
            return resp;
        }

        public Currents ReadMeasurementsCurrentDisplay()
        {
            LogMethod();
            var resp = Cirbus.Read<Currents>(READMEASUREDCURRENTDISPLAY, listErrorCirbus);
            //return resp / FactorTransformationCurrent; 
            return resp;
        }

        public FourLineValue ReadCurrentDisplayInAmpers(Scale scale)
        {
            var factorsCalibrations = scale == Scale.PRECISION ? ReadCurrentFactorCalibration().FactorsScalePrecision : ReadCurrentFactorCalibration().FactorsScaleCC;
            var currentPoints = scale == Scale.PRECISION ? ReadMeasurementsCurrentDisplay().ScalePrecision : ReadMeasurementsCurrentDisplay().ScaleCC;
            var currentFondo = scale == Scale.PRECISION ? 5 : 26;

            var L1 = ((double)factorsCalibrations.L1 * (double)currentPoints.L1 * (double)currentFondo) / (30000D * 65536D);
            var L2 = (double)(factorsCalibrations.L2 * (double)currentPoints.L2 * (double)currentFondo) / (30000D * 65536D);
            var L3 = (double)(factorsCalibrations.L3 * (double)currentPoints.L3 * (double)currentFondo) / (30000D * 65536D);

            var LH = (double)(factorsCalibrations.LH * (double)currentPoints.LH * (double)currentFondo) / (30000D * 65536D);

            return FourLineValue.Create(L1, L2, L3, LH);
        }

        public Voltage ReadMeasurementsVolageDisplay()
        {
            LogMethod();
            var resp = Cirbus.Read<Voltage>(READMEASUREDVOLTAGEDISPLAY, listErrorCirbus);
            //return resp / FactorTransformationVoltage;
            return resp;
        }

        public TriLineValue ReadVoltageDisplayInVolts(double Vref)
        {
            var measures = ReadMeasurementsVolageDisplay();

            var L1 = Vref * ((double)measures.L1CAL / 1172);
            var L2 = Vref * ((double)measures.L2CAL / 1172);
            var L3 = Vref * ((double)measures.L3CAL / 1172);

            return TriLineValue.Create(L1, L2, L3);
        }

        public MeasureAnglePhase ReadMeasurementsAnglePhase()
        {
            LogMethod();
            var phaseAngle = new MeasureAnglePhase();
            phaseAngle.L1 = Cirbus.Read<MeasureAngle>(string.Format(READPHASEMEASUREDANGLE, 1));
            phaseAngle.L2 = Cirbus.Read<MeasureAngle>(string.Format(READPHASEMEASUREDANGLE, 2));
            phaseAngle.L3 = Cirbus.Read<MeasureAngle>(string.Format(READPHASEMEASUREDANGLE, 3));
            return phaseAngle;
        }

        public MeasureAngleHomopolar ReadMeasurementsAnglePhaseHomopolar()
        {
            LogMethod();
            return Cirbus.Read<MeasureAngleHomopolar>(string.Format(READPHASEMEASUREDANGLE, "I"));
        }

        public DateTime ReadDateTime()
        {
            LogMethod();
            var resp = Cirbus.Read(READCLOCKSETTINGS, listErrorClock);
            return FormatDateTime(resp);
        }

        private DateTime FormatDateTime(string ekorFormatDateTime)
        {
            var arrayFechaHora = ekorFormatDateTime.Split(' ');

            var Fecha = arrayFechaHora[0].Replace(':', '/');
            var Hora = arrayFechaHora[1].Replace('/', ':');

            return Convert.ToDateTime(string.Format("{0} {1}", Fecha, Hora));
        }

        public InfoTriggers ReadInfoTriggers()
        {
            LogMethod();

            var installerAdjust = ReadInstallerSettingsStruct();
            var relTrigger = installerAdjust.Model == Model._300 ? 30000D : 100000D;

            var historic = Cirbus.Read(READHISTORICTRIGGER, listErrorCirbus);

            var infoTrigger = new InfoTriggers();
            infoTrigger.currentTrigguer = Convert.ToInt64(historic.Substring(0, 8), 16) / relTrigger;
            infoTrigger.timeTrigger = Convert.ToInt32(historic.Substring(16, 4), 16) / 100D;

            if (infoTrigger.currentTrigguer == 0 && infoTrigger.timeTrigger == 0)
            {
                infoTrigger.currentTrigguer = Convert.ToInt64(historic.Substring(36, 8), 16) / relTrigger;
                infoTrigger.timeTrigger = Convert.ToInt32(historic.Substring(52, 4), 16) / 100D;
            }

            return infoTrigger;
        }

        public void WriteClearKeyboard()
        {
            LogMethod();
            Cirbus.Read(READKEYBOARD);
        }

        public void WriteRemoveCounter()
        {
            LogMethod();
            Cirbus.Write(COUNTERANDTRIPDELETE);
        }

        public void RemoveMaximums()
        {
            LogMethod();
            Cirbus.Write(REMOVECOUNTERS);
        }

        public void WriteUserSettings(string userSettings)
        {
            LogMethod();
            Cirbus.Write(string.Format(WRITEUSERSETTINGS, userSettings));
        }

        public void WriteInstallerSettings(string installerSettings)
        {
            LogMethod();
            Cirbus.Write(string.Format(WRITEINSTALLERSETTINGS, installerSettings));
        }

        public void WritePowerFactors(string powerFactors)
        {
            LogMethod();
            Cirbus.Write(POWERFACTORS, powerFactors);
        }

        public void WriteFrameNumber(string frameNumber)
        {
            LogMethod();

            Cirbus.Write(string.Format(WRITEBASTIDOR, frameNumber));
        }

        public void WriteBastidor(string serialNumber)
        {
            LogMethod();
            Cirbus.Write(WRITEBASTIDOR, serialNumber);
        }

        public void WriteDate(DateTime datetime)
        {
            LogMethod();
            Cirbus.Write(string.Format(WRITECLOCKSETTINGS, datetime.Day, datetime.Month, datetime.ToString("yy"), datetime.Hour, datetime.Minute, datetime.Second));
        }

        public void WritePasatapas(int valorPasatapas)
        {
            LogMethod();
            Cirbus.Write(string.Format(WRITECALIBRATIONPASATAPAS, valorPasatapas, valorPasatapas, valorPasatapas));
        }

        public void WriteStandbye()
        {
            LogMethod();
            Cirbus.Write(STANDBYE);
        }

        public void WriteOutput(enumOutputsRPGRPT output, stateOutputs state)
        {
            LogMethod();
            Cirbus.Write(WRITEOUTPUT, ((int)output).ToString() + ((int)state).ToString());
        }

        public void WriteOutput(enumOutputsCI output)
        {
            LogMethod();
            Cirbus.Write(WRITEOUTPUT, ((byte)output).ToString("x4"));
        }

        public void WriteAngleCalibrationInitialization()
        {
            LogMethod();
            Cirbus.Write(WRITE_ANGLE_DEFAULT);
        }

        public void WriteVoltageFactorCalibration(FactorsCalibrationVoltages FactoresCalibracion)
        {
            LogMethod();

            Cirbus.Write(WRITEVOLTAGEFACTORSCALIBRATION, FactoresCalibracion.ToCirbus);
        }

        public void WriteCurrentFactorCalibration(FactorsCalibrationCurrents FactoresCalibracion)
        {
            LogMethod();
            Cirbus.Write(WRITECURRENTFACTORSCALIBRATION, FactoresCalibracion.ToCirbus);
        }

        public void WriteCurrentFactorCalibration(Scale scale, FasesEscala fasesValues)
        {
            LogMethod();

            var factorsCalibrations = ReadCurrentFactorCalibration();

            if (scale == Scale.PRECISION)
                factorsCalibrations.FactorsScalePrecision = fasesValues;
            else
                factorsCalibrations.FactorsScaleCC = fasesValues;


            Cirbus.Write(WRITECURRENTFACTORSCALIBRATION, factorsCalibrations.ToCirbus);
        }

        public void WriteClockCalibration()
        {
            LogMethod();
            Cirbus.Write(CLOCKCALIBRATION);
            Cirbus.Read("ACK");
        }

        public void StartSignalClok(int secondDuration)
        {
            LogMethod();
            Cirbus.Retries = 0;
            Cirbus.WithTimeOut((p) => { p.Write(GENERALCLOCKCALIBRATION, listErrorClock, secondDuration); });
            Cirbus.Retries = 3;
        }

        public FactorCalibrationCalculate CalculateReadRmsMeasure(int delFirst, int initCount, int samples, int interval, Func<RmsMesures, FactorCalibrationCalculate> calculateFactors)
        {
            var FCcalculated = new FactorCalibrationCalculate();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var readRms = ReadRmsMeasurePoints();
                    if (!readRms.HasValue)
                        throw new Exception("Error. No se han obtenido los valores medidos del sumatorio de ciclos de la trama LSC 0032 para poder calcular los valores eficaces de la calibración");

                    var points = readRms.Value;
                    var lecturas = new List<double>();

                    RmsMesures resultadoAjuste = new RmsMesures();
                    resultadoAjuste.RMS_IL1 = Math.Sqrt(Math.Abs((points.Pt_I1 / 16D) - Math.Pow((points.Pt_Off_I1 / 16D), 2)));
                    resultadoAjuste.RMS_IL2 = Math.Sqrt(Math.Abs((points.Pt_I2 / 16D) - Math.Pow((points.Pt_Off_I2 / 16D), 2)));
                    resultadoAjuste.RMS_IL3 = Math.Sqrt(Math.Abs((points.Pt_I3 / 16D) - Math.Pow((points.Pt_Off_I3 / 16D), 2)));
                    resultadoAjuste.RMS_ILH = Math.Sqrt(Math.Abs((points.Pt_IH / 16D) - Math.Pow((points.Pt_Off_IH / 16D), 2)));

                    _logger.DebugFormat("L1 I: {0}  OFF: {1}  RMS: {2}", points.Pt_I1, points.Pt_Off_I1, resultadoAjuste.RMS_IL1);
                    _logger.DebugFormat("L2 I: {0}  OFF: {1}  RMS: {2}", points.Pt_I2, points.Pt_Off_I2, resultadoAjuste.RMS_IL2);
                    _logger.DebugFormat("L3 I: {0}  OFF: {1}  RMS: {2}", points.Pt_I3, points.Pt_Off_I3, resultadoAjuste.RMS_IL3);
                    _logger.DebugFormat("LH I: {0}  OFF: {1}  RMS: {2}", points.Pt_IH, points.Pt_Off_IH, resultadoAjuste.RMS_ILH);

                    lecturas.Add(resultadoAjuste.RMS_IL1);
                    lecturas.Add(resultadoAjuste.RMS_IL2);
                    lecturas.Add(resultadoAjuste.RMS_IL3);
                    lecturas.Add(resultadoAjuste.RMS_ILH);

                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    RmsMesures resultadoAjuste = new RmsMesures();
                    resultadoAjuste.RMS_IL1 = listValues.Average(0);
                    resultadoAjuste.RMS_IL2 = listValues.Average(1);
                    resultadoAjuste.RMS_IL3 = listValues.Average(2);
                    resultadoAjuste.RMS_ILH = listValues.Average(3);
                    FCcalculated = calculateFactors(resultadoAjuste);
                    return true;
                });

            return FCcalculated;
        }

        public void FLASHFormat()
        {
            LogMethod();
            Cirbus.Transport.ReadTimeout = 6000;
            Cirbus.Write(FORMATFLASH);
            Cirbus.Transport.ReadTimeout = 2000;
        }

        public string FLASHImport(FileTypeExportImport file)
        {
            LogMethod();
            return Cirbus.Read(FLASHIMP, listErrorFAT, file.ToString());
        }

        public string FLASHExport(FileTypeExportImport file)
        {
            LogMethod();
            return Cirbus.Read(FLASHEXP, listErrorFAT, file.ToString());
        }

        public string ImportXML(FileTypeExportImport file)
        {
            LogMethod();
            return Cirbus.Read(IMPXML, listErrorXML, file.ToString());
        }

        public string ExportXML(FileTypeExportImport file)
        {
            LogMethod();
            return Cirbus.Read(EXPXML, listErrorXML, file.ToString());
        }

        public Tuple<bool, FactorsCalibrationVoltages> AdjustVoltage(int delFirst, int initCount, int samples, int interval, double inyectedVoltage, double voltageRef, Func<FactorsCalibrationVoltages, bool> adjustValidation)
        {
            var result = ReadVoltageFactorCalibration();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 100,
                () =>
                {
                    var voltage = ReadMeasurementsVolageDisplay();  //Modificar estructura según devuelva el equipo, al parecer devolvera la tensión de cada linea(TriLineValue)

                    
                    return new double[] { voltage.L1, voltage.L2, voltage.L3 };
                },
                (listValues) =>
                {

                    result.L1 = Convert.ToInt32((((1172 / voltageRef) * inyectedVoltage) * Math.Pow(2, 15)) / listValues.Average(0));
                    result.L2 = Convert.ToInt32((((1172 / voltageRef) * inyectedVoltage) * Math.Pow(2, 15)) / listValues.Average(1));
                    result.L3 = Convert.ToInt32((((1172 / voltageRef) * inyectedVoltage) * Math.Pow(2, 15)) / listValues.Average(2));

                    return adjustValidation(result);
                });

            return Tuple.Create(adjustResult.Item1, result);
        }

        public Tuple<bool, FasesEscala> AdjustCurrents(Scale scale, int delFirst, int initCount, int samples, int interval, TriLineValue inyectedcurrent, double currentFondo, Func<FasesEscala, bool> adjustValidation)
        {
            var result = scale == Scale.PRECISION ? ReadCurrentFactorCalibration().FactorsScalePrecision : ReadCurrentFactorCalibration().FactorsScaleCC;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 100,
                () =>
                {
                    var currents = scale == Scale.PRECISION ? ReadMeasurementsCurrentDisplay().ScalePrecision : ReadMeasurementsCurrentDisplay().ScaleCC;

                    return new double[] { currents.L1, currents.L2, currents.L3 };
                },
                (listValues) =>
                {
                    result.L1 = Convert.ToInt32(((inyectedcurrent.L1 / currentFondo) * 30000 * 65536) / listValues.Average(0));
                    result.L2 = Convert.ToInt32(((inyectedcurrent.L2 / currentFondo) * 30000 * 65536) / listValues.Average(1));
                    result.L3 = Convert.ToInt32(((inyectedcurrent.L3 / currentFondo) * 30000 * 65536) / listValues.Average(2));

                    return adjustValidation(result);
                });

            return Tuple.Create(adjustResult.Item1, result);
        }

        public Tuple<bool, FasesEscala> AdjustHomopolarsCurrents(Scale scale, int vueltasTrafo, int delFirst, int initCount, int samples, int interval, double inyectedcurrent, double currentFondo, Func<FasesEscala, bool> adjustValidation)
        {
            var result = scale == Scale.PRECISION ? ReadCurrentFactorCalibration().FactorsScalePrecision : ReadCurrentFactorCalibration().FactorsScaleCC;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 100,
                () =>
                {
                    List<double> varlist;

                    var currents = scale == Scale.PRECISION ? ReadMeasurementsCurrentDisplay().ScalePrecision : ReadMeasurementsCurrentDisplay().ScaleCC;

                    varlist = new List<double>()
                    {
                        currents.LH
                    };

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    result.LH = Convert.ToInt32(((inyectedcurrent * vueltasTrafo / currentFondo) * 30000 * 65536) / listValues.Average(0));

                    return adjustValidation(result);
                });

            return Tuple.Create(adjustResult.Item1, result);
        }

        public void RecoveryDefaultSettings(int port, int baudrate, byte periferico, System.IO.Ports.Parity parity = System.IO.Ports.Parity.None)
        {
            LogMethod();

            Cirbus.ClosePort();

            using (ModbusDeviceSerialPort Modbus = new ModbusDeviceSerialPort(port, baudrate, parity, System.IO.Ports.StopBits.One, _logger))
            {
                Modbus.UseFunction06 = true;
                Modbus.PerifericNumber = periferico;
                Modbus.Write((ushort)RegistersModbus.KEY_PSWI, (ushort)0x270F);
                Modbus.Write((ushort)RegistersModbus.PROTOCOL, (ushort)1);
                Modbus.Write((ushort)RegistersModbus.KEY_PSWI, (ushort)0x270F);
            };

            Cirbus.OpenPort();
        }

        public override void Dispose()
        {
            Cirbus.Dispose();
        }

        #region Enums and Dictionaries

        public enum Scale
        {
            PRECISION,
            CORTOCIRCUITO
        }

        public enum EkorPlusFamilyCalibration             // Diferenciamos los grupos grandes, los cuales se calibraran de manera distinta
        {
            CAPACITIVO = 0,
            RESISTIVO = 1
        }


        public enum EkorPlusFamilySubmodel          // Comentar a Ferran si es necesario, en el caso del RPA no se testea el puerto RS232 frontal, sinó que hay un USB
        {
            NO_RPA = 0,
            RPA = 1
        }

        public enum FileTypeExportImport
        {
            CAL,
            HIS,
            INS,
            USR,
            LOG,
            ALL,
        }

        public enum EkorModelType
        {
            RPG_RPT,
            CI,
            UNKNOW
        }

        [Flags]
        public enum Keyboard
        {
            [Description("Ninguna tecla pulsada")]
            NO_BUTTON_OR_INPUT = 0x0000,
            [Description("DERECHA")]
            RIGHT_BUTTON = 0x0001,
            [Description("ABAJO")]
            DOWN_BUTTON = 0x0002,
            [Description("IZQUIERDA")]
            LEFT_BUTTON = 0x0004,
            [Description("ARRIBA")]
            UP_BUTTON = 0x0008,
            [Description("ARRIBA/ABAJO")]
            UP_DOWN_BUTTON = 0x000A,
            [Description("SET")]
            SET_BUTTON = 0X0010,
            [Description("ESC")]
            ESC_BUTTON = 0x0020,
        }

        [Flags]
        public enum enumInputs
        {
            NOTHING = 0x00,   // Valores fijos de las entradas
            IN1 = 0x01,
            IN2 = 0x02,
            IN3 = 0x04,
            IN4 = 0x08,
            IN5 = 0x10,
            IN6 = 0x20,
            IN7 = 0x40,
            IN8 = 0x80,
            IN9 = 0x100,
            IN10 = 0x200
        }

        [Flags]
        public enum enumInputsCI
        {
            //NOTHING = 0x001f,   // Valores fijos de las entradas
            //IN1 = 0x000f,
            //IN2 = 0x0017,
            //IN3 = 0x001b,
            //IN4 = 0x001d,
            //IN5 = 0x001e,
            //IN6 = 0x003f,
            //IN7 = 0x005f,
            //IN8 = 0x009f,
            //IN9 = 0x011f,
            //IN10 = 0x021f,

            [Description("Ninguna entrada digital activada")]
            NOTHING = 0x001f,
            [Description("Entrada digital 1")]
            IN1 = NOTHING - (1 << 4),
            [Description("Entrada digital 2")]
            IN2 = NOTHING - (1 << 3),
            [Description("Entrada digital 3")]
            IN3 = NOTHING - (1 << 2),
            [Description("Entrada digital 4")]
            IN4 = NOTHING - (1 << 1),
            [Description("Entrada digital 5")]
            IN5 = NOTHING - 1,            
            [Description("Entrada digital 6")]
            IN6 = (1 << 5) + NOTHING,
            [Description("Entrada digital 7")]
            IN7 = (1 << 6) + NOTHING,
            [Description("Entrada digital 8")]
            IN8 = (1 << 7) + NOTHING,
            [Description("Entrada digital 9")]
            IN9 = (1 << 8) + NOTHING,
            [Description("Entrada digital 10")]
            IN10 = (1 << 9) + NOTHING,
        }

        [Flags]
        public enum enumOutputsRPGRPT
        {
            OUT1 = 1,
            OUT2 = 2,
            OUT3 = 3,
            OUT4 = 4,
            OUT5 = 5,
            OUT6 = 6,
            OUT7 = 7
        }

        [Flags]
        public enum enumOutputsCI
        {
            NOTHING = 0X0002,
            OUT1 = 0X0003,
            OUT2 = 0X0000,
            OUT3 = 0X0006,
            OUT4 = 0X000A,
            OUT5 = 0X0012,
            OUT6 = 0X0022,
            OUT7 = 0X0042
        }

        public enum stateOutputs
        {
            ON = 0,
            OFF = 1
        }

        private Dictionary<byte, string> listErrorXML = new Dictionary<byte, string>()
        {
            { 2, "Error no detecta flash o bloqueo USB" },
            { 3, "Error al importar/exportar un fichero" },
            { 4, "Error al grabar en la EEPROM" },
            { 5, "Valores de los ajustes fuera de límites o incorrectos" },
            { 6, "Fallo en la sintaxis del comando PRG" },
            { 7, "Fallo CheckSum del comando PRG" },
        };

        private Dictionary<byte, string> listErrorFAT = new Dictionary<byte, string>()
        {
            { 1, "Error al formatear la flash" },
            { 2, "Error no detecta flash o bloqueo USB" },
            { 3, "Error al importar/exportar un fichero" },
            { 4, "Error al grabar en la EEPROM" },
            { 5, "Valores de los ajustes fuera de límites o incorrectos" },
            { 6, "Fallo en la sintaxis del comando FAT" },
            { 7, "Fallo CheckSum del comando FAT" },
        };

        private Dictionary<byte, string> listErrorInputs = new Dictionary<byte, string>()
        {
             { 6, "Error sintaxis del comando" },
             { 7, "Error ChekSum del comando" }
        };

        private Dictionary<byte, string> listErrorCirbus = new Dictionary<byte, string>()
        {
            { 1, "Error comando Cirbus" },
            { 2, "Error comando Cirbus" },
            { 6, "Error sintaxis del comando" },
            { 7, "Error ChekSum del comando" }
        };

        private Dictionary<byte, string> listErrorClock = new Dictionary<byte, string>(){
             { 1, "Falla EEP 1º Pagina" },
             { 2, "Falla EEP 2º Pagina" },
             { 3, "Falla EEP 1º y 2º Pagina" },
             { 4, "Fallo indeterminado" },
             { 5, "Comando calibración sin haber echo el comando Start Test" },
             { 6, "Error sintaxis del comando CLK" },
             { 7, "Error ChekSum del comando CLK" }
        };

        #endregion

        #region Structs

        public struct HistoricTriggers
        {
            public Int64 lastShotValue;
            public Int32 lastShotTime;
            public byte lastShotDay;
            public byte lastShotMonth;
            public byte lastShotYear;
            public byte lastShotHour;
            public byte lastShotMinutes;
            public byte lastShotSeconds;

            public Int32 lastShotAngle;
            public Int32 penultimShotAngle;

            public byte durationLastShotCentesimas;
            public byte durationPenultimoShotCentesimas;

            public Int64 penultimShotValue;
            public Int32 penultimShotTime;
            public byte penultimShotDay;
            public byte penultimShotMonth;
            public byte penultimShotYear;
            public byte penultimShotHour;
            public byte penultimShotMinutes;
            public byte penultimShotSeconds;

        }

        public struct MeasurePointsStructure
        {
            public string respuesta;
            public int Pt_I1;               //P1
            public int Pt_I2;               //P2
            public int Pt_I3;               //P3
            public int Pt_IH;               //P4
            public int Pt_V1;               //P5
            public int Pt_V2;               //P6
            public int Pt_V3;               //P7
            public int Pt_VH;               //P8
            public int Pt_POT1;             //P9
            public int Pt_POT2;             //P10
            public int Pt_POT3;             //P11
            public int Pt_POTH;             //P12
            public int Pt_Off_I1;           //P13
            public int Pt_Off_I2;           //P14
            public int Pt_Off_I3;           //P15
            public int Pt_Off_IH;           //P16
            public int Pt_Off_V1;           //P17
            public int Pt_Off_V2;           //P18
            public int Pt_Off_V3;           //P19
            public int Pt_Off_VH;           //P20
        }

        public struct RmsMesures
        {
            public double RMS_IL1;
            public double RMS_IL2;
            public double RMS_IL3;
            public double RMS_ILH;
            public double RMS_VL1;
            public double RMS_VL2;
            public double RMS_VL3;
        }

        public struct InputsStructure
        {
            public enumInputs input;
            public bool state;
        }


        public struct FasesEscala
        {
            public Int32 L1;
            public Int32 L2;
            public Int32 L3;
            public Int32 LH;
        }

        public struct Voltage
        {

            public Int32 L1;
            public Int32 L2;
            public Int32 L3;

            public Int32 L1CAL;
            public Int32 L2CAL;
            public Int32 L3CAL;


            //public static MedidaFases operator /(MedidaFases y, int x)
            //{
            //    var resultc1 = y.L1 / x;
            //    var resultc2 = y.L2 / x;
            //    var resultc3 = y.L3 / x;
            //    var resultc4 = y.LH / x;

            //    var resultMeasure = new MedidaFases();
            //    resultMeasure.L1 = resultc1;
            //    resultMeasure.L2 = resultc2;
            //    resultMeasure.L3 = resultc3;
            //    resultMeasure.LH = resultc4;
            //    return resultMeasure;
            //}
        }

        public struct Currents
        {
            public FasesEscala ScalePrecision
            {
                get
                {
                    return new FasesEscala { L1 = L1, L2 = L2, L3 = L3, LH = LH };
                }

            }

            public FasesEscala ScaleCC
            {
                get
                {
                    return new FasesEscala { L1 = L1CC, L2 = L2CC, L3 = L3CC, LH = LHCC };
                }
            }

            private Int32 L1; //L1P
            private Int32 L1CC; //L1CC
            private Int32 L2;
            private Int32 L2CC;
            private Int32 L3;
            private Int32 L3CC;
            private Int32 LH;
            private Int32 LHCC;

            private ushort scaleL1;
            private ushort scaleL2;
            private ushort scaleL3;
            private ushort scaleLH;

            //public static MedidaFases operator /(MedidaFases y, int x)
            //{
            //    var resultc1 = y.L1 / x;
            //    var resultc2 = y.L2 / x;
            //    var resultc3 = y.L3 / x;
            //    var resultc4 = y.LH / x;

            //    var resultMeasure = new MedidaFases();
            //    resultMeasure.L1 = resultc1;
            //    resultMeasure.L2 = resultc2;
            //    resultMeasure.L3 = resultc3;
            //    resultMeasure.LH = resultc4;
            //    return resultMeasure;
            //}
        }

        //public struct UserSettings
        //{
        //    //public byte programmerByTablePower;
        //    //public byte programmerByTableVoltage;
        //    //public byte curvePhase;
        //    //public byte curveHomopolar;
        //    //public uint nominalCurrent;
        //    //public byte instantTriggerPhase;
        //    //public byte instantTriggerHomopolar;
        //    //public uint overloadPhase;
        //    //public uint overloadHomopolar;
        //    //public byte K;
        //    //public byte K2;
        //    //public byte shortcircuitPhase;
        //    //public byte shortcircuitHomopolar;
        //    //public byte timeShortCircuitPhase;
        //    //public byte timeShortCircuitHomopolar;
        //    //public uint password;
        //}

        public enum CurveType
        {
            OFF = 0,
            NI = 1,
            VI = 2,
            EI = 3,
            DT = 4
        }

        public enum InstantTrigger
        {
            OFF = 0,
            DT = 1
        }

        public enum valuesK
        {
            [Description("0,05")]
            _005 = 0,
            [Description("0,06")]
            _006 = 1,
            [Description("0,07")]
            _007 = 2,
            [Description("0,08")]
            _008 = 3,
            [Description("0,09")]
            _009 = 4,
            [Description("0,1")]
            _01 = 5,
            [Description("0,2")]
            _02 = 6,
            [Description("0,3")]
            _03 = 7,
            [Description("0,4")]
            _04 = 8,
            [Description("0,5")]
            _05 = 9,
            [Description("0,6")]
            _06 = 10,
            [Description("0,7")]
            _07 = 11,
            [Description("0,8")]
            _08 = 12,
            [Description("0,9")]
            _09 = 13,
            [Description("1")]
            _1 = 14,
            [Description("1,1")]
            _11 = 15,
            [Description("1,2")]
            _12 = 16,
            [Description("1,3")]
            _13 = 17,
            [Description("1,4")]
            _14 = 18,
            [Description("1,5")]
            _15 = 19,
            [Description("1,6")]
            _16 = 20
        }

        public struct UserSettings
        {
            private string programmerByTablePower;
            private string programmerByTableVoltage;
            private string curvePhase;
            private string curveHomopolar;
            private string nominalCurrent;
            private string instantTriggerPhase;
            private string instantTriggerHomopolar;
            private string overloadPhase;
            private string overloadHomopolar;
            private string kPhase;
            private string kHomopolar;
            private string shortcircuitPhase;
            private string shortcircuitHomopolar;
            private string timeShortCircuitPhase;
            private string timeShortCircuitHomopolar;
            private string password;

            public byte ProgrammerByTablePower
            {
                get { return Convert.ToByte(programmerByTablePower, 16); }
                set { programmerByTablePower = string.Format("{0:X}", value); }
            }
            public byte ProgrammerByTableVoltage
            {
                get { return Convert.ToByte(programmerByTablePower, 16); }
                set { programmerByTableVoltage = string.Format("{0:X}", value); }
            }
            public CurveType CurvePhase
            {
                get { return (CurveType)Convert.ToByte(curvePhase, 16); }
                set { curvePhase = string.Format("{0:X}", (ushort)value); }
            }
            public CurveType CurveHomopolar
            {
                get { return (CurveType)Convert.ToByte(curveHomopolar, 16); }
                set { curveHomopolar = string.Format("{0:X}", (ushort)value); }
            }
            public uint NominalCurrent
            {
                get { return Convert.ToUInt32(nominalCurrent, 16); }
                set { nominalCurrent = string.Format("{0:X}", value); }
            }
            public InstantTrigger InstantTriggerPhase
            {
                get { return (InstantTrigger)Convert.ToByte(instantTriggerPhase, 16); }
                set { instantTriggerPhase = string.Format("{0:X}", (ushort)value); }
            }
            public InstantTrigger InstantTriggerHomopolar
            {
                get { return (InstantTrigger)Convert.ToByte(instantTriggerHomopolar, 16); }
                set { instantTriggerHomopolar = string.Format("{0:X}", (ushort)value); }
            }
            public uint OverloadPhase
            {
                get { return Convert.ToUInt32(overloadPhase, 16); }
                set { overloadPhase = string.Format("{0:X}", value); }
            }
            public uint OverloadHomopolar
            {
                get { return Convert.ToUInt32(overloadHomopolar, 16); }
                set { overloadHomopolar = string.Format("{0:X}", value); }
            }
            public double KPhase
            {
                get
                {
                    var k = (valuesK)Convert.ToUInt16(kPhase, 16);
                    return Convert.ToDouble(k.GetDescription());
                }
                set
                {
                    foreach(valuesK valorEnumerado in Enum.GetValues(typeof(valuesK)))
                        if (valorEnumerado.GetDescription() == value.ToString())
                            kPhase = string.Format("{0:X}",(int)valorEnumerado);
                    if (kPhase == null)
                        throw new Exception("Valor inválido para la constante K de Fase");
                }
            }
            public double KHomopolar
            {
                get
                {
                    var k = (valuesK)Convert.ToUInt16(kHomopolar, 16);
                    return Convert.ToDouble(k.GetDescription());
                }
                set
                {
                    foreach (valuesK valorEnumerado in Enum.GetValues(typeof(valuesK)))
                        if (valorEnumerado.GetDescription() == value.ToString())
                            kHomopolar = string.Format("{0:X}", (int)valorEnumerado);
                    if (kHomopolar == null)
                        throw new Exception("Valor inválido para la constante K de Fase Homopolar");
                }
            }
            public byte ShortcircuitPhase
            {
                get { return Convert.ToByte(shortcircuitPhase, 16); }
                set { shortcircuitPhase = string.Format("{0:X}", value); }
            }
            public byte ShortcircuitHomopolar
            {
                get { return Convert.ToByte(shortcircuitHomopolar, 16); }
                set { shortcircuitHomopolar = string.Format("{0:X}", value); }
            }
            public byte TimeShortCircuitPhase
            {
                get { return Convert.ToByte(timeShortCircuitPhase, 16); }
                set { timeShortCircuitPhase = string.Format("{0:X}", value); }
            }
            public byte TimeShortCircuitHomopolar
            {
                get { return Convert.ToByte(timeShortCircuitHomopolar, 16); }
                set { timeShortCircuitHomopolar = string.Format("{0:X}", value); }
            }
            public uint Password
            {
                get { return Convert.ToUInt32(password, 16); }
                set { password = string.Format("{0:X}", value); }
            }
                                                                                                                                                
            public string ToCirbus
            {
                get
                {
                    return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14} {15}", programmerByTablePower.PadLeft(2, '0'), programmerByTableVoltage.PadLeft(2, '0'), curvePhase.PadLeft(2, '0'), curveHomopolar.PadLeft(2, '0'), nominalCurrent.PadLeft(4, '0'), instantTriggerPhase.PadLeft(2, '0'), instantTriggerHomopolar.PadLeft(2, '0'), overloadPhase.PadLeft(4, '0'), overloadHomopolar.PadLeft(4, '0'), kPhase.PadLeft(2, '0'), kHomopolar.PadLeft(2, '0'), shortcircuitPhase.PadLeft(2, '0'), shortcircuitHomopolar.PadLeft(2, '0'), timeShortCircuitPhase.PadLeft(2, '0'), timeShortCircuitHomopolar.PadLeft(2, '0'), password.PadLeft(4, '0'));
                }
            }

        }

        public struct InstallerSettings
        {
            private string setup { get; set; }               // InstallerSettings Familia Ekor +   ---> COMPLETO
            private string password { get; set; }
            private string iMaxRPTA { get; set; }
            private string iMinRPTA { get; set; }
            private string saturacion { get; set; }
            private string model { get; set; }
            private string frecuency { get; set; }
            private string trafoHomopolar { get; set; }
            private string rele { get; set; }
            private string nrot { get; set; }
            private string setBits { get; set; }

            public ushort periferic { get { return (ushort)((Convert.ToUInt16(setup, 16) & 0xF800) >> 11); } }
            public Protocol protocol { get { return (Protocol)((Convert.ToUInt16(setup, 16) & 0x700) >> 8); } }
            public Paridad parity { get { return (Paridad)((Convert.ToUInt16(setup, 16) & 0x60) >> 5); } }
            public SBits stopBits { get { return (SBits)((Convert.ToUInt16(setup, 16) & 0x10) >> 4); } }
            public NBits nBits { get { return (NBits)((Convert.ToUInt16(setup, 16) & 0x8) >> 3); } }
            public BaudRate baudRate { get { return (BaudRate)(Convert.ToUInt16(setup, 16) & 0x7); } }

            public uint Password { get { return Convert.ToUInt32(password, 16); } }
            public uint IMaxRPTA { get { return Convert.ToUInt32(iMaxRPTA, 16); } }
            public uint IMinRPTA { get { return Convert.ToUInt32(iMinRPTA, 16); } }
            public string Saturacion { get { return saturacion; } }
            public Model Model { get { return (Model)Convert.ToUInt16(model, 16); } }
            public int Frecuency { get { return Convert.ToByte(frecuency, 16) == 0 ? 50 : 60; } }
            public string TrafoHomopolar { get { return Convert.ToByte(trafoHomopolar, 16) == 1 ? "SI" : "NO"; } }
            public CeldaType Rele { get { return (CeldaType)Convert.ToByte(rele, 16); } }
            public string Nrot { get { return nrot; } }
            public string SetBits { get { return setBits; } }


            public string ToCirbus
            {
                get
                {
                    return string.Format("{0:0000} {1:0000} {2:00} {3:00} {4:00} {5:00} {6:0000} {7:00} {8:0000} {9:0000} {10:0000}", setup, password, iMaxRPTA, iMinRPTA, saturacion, model, frecuency, trafoHomopolar, rele, Nrot, setBits);
                }
            }
        }

        public enum Protocol
        {
            MODBUS = 0,
            CIRBUS = 1
        }

        public enum BaudRate
        {
            _4800 = 2
        }

        public enum SBits
        {
            ONE = 0,
            TWO = 1
        }

        public enum NBits
        {
            EIGHT = 0,
            SEVEN = 1
        }

        public enum Paridad
        {
            NONE = 0,
            EVEN = 1,
            ODD = 2
        }

        public enum CeldaType
        {
            RPGM = 0,
            RPTA = 1
        }       

        public enum Model
        {
            _1000 = 0x06,
            _300 = 0x16             
        }

        public struct FactorCalibrationCalculate
        {
            public double FC_IL1;
            public double FC_IL2;
            public double FC_IL3;
            public double FC_ILH;
            public double FC_VL1;
            public double FC_VL2;
            public double FC_VL3;
        }

        public struct FactorsCalibrationCurrents
        {
            public FasesEscala FactorsScalePrecision
            {
                get
                {
                    return new FasesEscala { L1 = FCL1, L2 = FCL2, L3 = FCL3, LH = FCLH };
                }
                set
                {
                    FCL1 = value.L1; FCL2 = value.L2; FCL3 = value.L3; FCLH = value.LH;
                }
            }

            public FasesEscala FactorsScaleCC
            {
                get
                {
                    return new FasesEscala { L1 = FCL1CC, L2 = FCL2CC, L3 = FCL3CC, LH = FCLHCC };
                }
                set
                {
                    FCL1CC = value.L1; FCL2CC = value.L2; FCL3CC = value.L3; FCLHCC = value.LH;
                }
            }

            private Int32 FCL1;
            private Int32 FCL1CC;
            private Int32 FCL2;
            private Int32 FCL2CC;
            private Int32 FCL3;
            private Int32 FCL3CC;
            private Int32 FCLH;
            private Int32 FCLHCC;

            public string ToCirbus
            {
                get
                {
                    return string.Format("{0:000000} {1:000000} {2:000000} {3:000000} {4:000000} {5:000000} {6:000000} {7:000000}", FCL1, FCL1CC, FCL2, FCL2CC, FCL3, FCL3CC, FCLH, FCLHCC);
                }
            }
        }

        public struct FactorsCalibrationVoltages
        {
            public Int32 L1;
            public Int32 L2;
            public Int32 L3;

            public string ToCirbus
            {
                get
                {
                    return string.Format("{0:000000} {1:000000} {2:000000}", L1, L2, L3);
                }
            }
        }

        public struct MedidaFases
        {
            public double L1; //L1P
            public double L1CC; //L1CC
            public double L2;
            public double L2CC;
            public double L3;
            public double L3CC;
            public double LH;
            public double LHCC;

            public byte scaleL1;
            public byte scaleL2;
            public byte scaleL3;
            public byte scaleLH;

            //public static MedidaFases operator /(MedidaFases y, int x)
            //{
            //    var resultc1 = y.L1 / x;
            //    var resultc2 = y.L2 / x;
            //    var resultc3 = y.L3 / x;
            //    var resultc4 = y.LH / x;

            //    var resultMeasure = new MedidaFases();
            //    resultMeasure.L1 = resultc1;
            //    resultMeasure.L2 = resultc2;
            //    resultMeasure.L3 = resultc3;
            //    resultMeasure.LH = resultc4;
            //    return resultMeasure;
            //}
        }

        public struct InfoTriggers
        {
            public double currentTrigguer;
            public double timeTrigger;
        }

        public struct MeasureAnglePhase
        {
            public MeasureAngle L1;
            public MeasureAngle L2;
            public MeasureAngle L3;
        }

        public struct MeasureAngle
        {
            public string command;
            public double I;
            public double V;
            public double P;
            public double Q;
            public double CosPhi;
            public double Anglex10;

            public double Angle
            {
                get
                {
                    return Anglex10 / 10;
                }
            }
            public double Current
            {
                get
                {
                    return (I / 300);
                }
            }
            public double Voltage
            {
                get
                {
                    return (V / 300);
                }
            }
        }

        public struct MeasureAngleHomopolar
        {
            public string command;
            public double P;
            public double Q;
            public double CosPhi;
            public double Anglex10;
            public double Angle
            {
                get
                {
                    return Anglex10 / 10;
                }
            }
        }

        #endregion

        //################################################################################################

        public void WriteInitializePointer()
        {
            LogMethod();
            Cirbus.TimeOut = 5000;
            Cirbus.Write(INITIALIZEPOINTER);
        }

        public string WriteProgrammingLogicTable(TiposTL tf)
        {
            LogMethod();
            var tramas = new StringBuilder();
            var i = 0;

            var TF = new List<string>();

            if (tf == TiposTL.TF1020)
                TF = LogicTable_TF1020;
            else
                TF = LogicTable_TF1000;

            foreach (var frame in TF)
            {
                _logger.DebugFormat("Tabla tipo:{0} -> Trama {1}",tf.ToString(),  i++);
                Cirbus.Write(string.Format(WRITELOGICPROGRAMMINGTABLE, frame));
                tramas.AppendLine(frame);
            }

            return tramas.ToString();
        }

        public void RecordingLogicTable()
        {
            LogMethod();
            //Cirbus.WriteWhitoutResponse(RECORDLOGICPROGRAMMINGTABLE);
            Cirbus.Write(RECORDLOGICPROGRAMMINGTABLE);
        }

        public string ReadLogicTable()
        {
            LogMethod();
            var tramas = new StringBuilder();
            var tramaRx = "";
            do
            {
                tramaRx = Cirbus.Read(READLOGICPROGRAMMINGTABLE);
                tramas.AppendLine(tramaRx.Replace("ACK", "").Substring(4));
            } while (!tramaRx.Contains("800080018002"));

            return tramas.ToString();
        }

        public void ReadEEPROM()
        {
            LogMethod();
            Cirbus.Read(READEEPROM, listErrorCirbus);
        }

        public enum TiposTL
        {
            TF1000,
            TF1020,
        }

        private List<string> LogicTable_TF1000 = new List<string>()
        {
            { "00015446313032303030" },
        };

        private List<string> LogicTable_TF1020 = new List<string>()
        {
            { "00015446313032303030" },
            { "30303030303030303030" },
            { "30303030303030303030" },
            { "2a2a0901000000020001" },
            { "80008001090200000000" },
            { "03018000030280000303" },
            { "80008001090300000000" },
            { "13018000130280001303" },
            { "800080010904ea600000" },
            { "09020903800080010905" },
            { "ea600000090480008001" },
            { "09060000000200018000" },
            { "8001090700c800051104" },
            { "80008001060500c80002" },
            { "01040907800080010907" },
            { "00000004040780008001" },
            { "06050000000201000101" },
            { "80008001050000000000" },
            { "01018000800108000000" },
            { "00020402800080010801" },
            { "00000000030019068000" },
            { "80010802000000020402" },
            { "80008001060300000006" },
            { "00088000800108000000" },
            { "00020000800080010802" },
            { "00000002000280008001" },
            { "08030000000200038000" },
            { "80010700000000001100" },
            { "80008001070100000000" },
            { "11018000800107020000" },
            { "00001102800080010703" },
            { "00000000110380008001" },
            { "07040000000011048000" },
            { "80010705000000000105" },
            { "80008001070600000000" },
            { "01068000800107070000" },
            { "0000010780008001070a" },
            { "00000000030c80008001" },
            { "070c0000000003018000" },
            { "8001070d000000000302" },
            { "80008001070e00000000" },
            { "030380008001070f0000" },
            { "00020402800080010710" },
            { "00000002040380008001" },
            { "07110000000204048000" },
            { "80010712000000020405" },
            { "80008001071300000002" },
            { "04068000800107140000" },
            { "0002040780008001071c" },
            { "ea600000090580008001" },
            { "07200000000008008000" },
            { "80010721000000000801" },
            { "80008001072200000000" },
            { "08028000800107230000" },
            { "00000803800080010708" },
            { "00000004800107090000" },
            { "00048001070b00000004" },
            { "80010715000000048001" },
            { "07160000000480010717" },
            { "00000004800107190000" },
            { "00048001071a00000004" },
            { "8001071b000000048001" },
            { "071d000000048001071e" },
            { "000000048001071f0000" },
            { "000480010b0000000003" },
            { "030c800080018002446e" }
        };

        //################################################################################################

        public class Inputs
        {
            private int numInputs;
            private int responseEkor;

            public Inputs(int responseEkor, int numInputs)
            {
                this.numInputs = numInputs;
                this.responseEkor = responseEkor;
            }

            public enumInputs StateInputs()
            {
                ushort anyInput;

                switch (numInputs)
                {
                    case 2:
                        anyInput = (ushort)valueAnyInputByInputsNumber.TWO_INPUTS;      //MODELO 2 ENTRADAS LSB = IN1, MSB = IN2
                        var bits  = Convert.ToString(responseEkor, 2);
                        if(bits.Length > 1)
                            responseEkor = Convert.ToInt32(bits.Substring(bits.Length - 2, 2), 2);  //BUG equipo, a veces nos devuelve mas bits de los esperados.
                        return (enumInputs)anyInput - responseEkor;
                    case 5:
                        anyInput = (ushort)valueAnyInputByInputsNumber.FIVE_INPUTS;
                        // return (enumInputs)(responseEkor ^= anyInput);     //MODELO 5 ENTRADAS LSB = IN5, MSB = IN1 (Negamos bit a bit para obtener el mismo orden de modelo 2 enrtadas)
                        var arrayBits = new char[numInputs];
                        arrayBits = Convert.ToString(anyInput - responseEkor, 2).ToCharArray();
                        Array.Reverse(arrayBits);
                        var inputsBits = new String(arrayBits).PadRight(numInputs, '0');
                        var activeInput = Convert.ToInt32(inputsBits.ToString(), 2);
                        return (enumInputs)(activeInput);
                    case 8:
                        anyInput = (ushort)valueAnyInputByInputsNumber.EIGHT_INPUTS;
                        var inputsBitsString = Convert.ToString(responseEkor, 2).PadLeft(numInputs, '0');
                        var low = inputsBitsString.Substring(3, 5);
                        var high = Convert.ToString((Convert.ToInt32(inputsBitsString.Substring(0, 3), 2)) ^ 7, 2).PadLeft(3, '0');

                        arrayBits = new char[numInputs];
                        arrayBits = low.ToCharArray();
                        Array.Reverse(arrayBits);
                        low = new String(arrayBits).PadRight(5, '0');

                        var result = Convert.ToInt32(string.Format("{0}{1}", high, low), 2);
                        return (enumInputs)anyInput - result;
                    default:
                        throw new Exception(string.Format("No. existe un modelo de EKOR DTC2 con {0} entradas", numInputs));
                }
            }

            private enum valueAnyInputByInputsNumber
            {
                TWO_INPUTS = 0X03,
                FIVE_INPUTS = 0X1F,
                EIGHT_INPUTS = 0XFF,
                TEN_INPUTS = 0X03FF,
            }
        }

        //################################################################################################

        public void WriteStartBOOT(int port, int baudrate, byte periferico, Parity parity = Parity.None)
        {
            LogMethod();

            using (ModbusDeviceSerialPort Modbus = new ModbusDeviceSerialPort(port, baudrate, parity, System.IO.Ports.StopBits.One, _logger))
            {
                Modbus.UseFunction06 = true;
                Modbus.PerifericNumber = periferico;
                Modbus.Write((ushort)RegistersModbus.START_BOOT, (ushort)0xAA55);
            };
        }

        public void WriteStartBOOT()
        {
            LogMethod();

            SerialPort sp = new SerialPort("COM1", 4800, Parity.None, 8, StopBits.One);

            if (!sp.IsOpen)
                sp.Open();

            byte[] bytesStartBoot = new byte[1] { 0x00 };

            sp.Write(bytesStartBoot, 0, 1);

            Thread.Sleep(200);
            sp.ReadTimeout = 10000;

            byte[] bufferRead = new byte[1];
            sp.Read(bufferRead, 0, 1);

            byte x = bufferRead[0];
        }

        public void Grabacion()
        {
            string fileName = "DRVBOOT.cde";

            SerialPortAdapter sp = new SerialPortAdapter(new SerialPort("COM1", 4800, Parity.Even, 8, StopBits.One));

            byte[] buffer;
            buffer = new byte[1] { 0x55 };

            //SamplerWithCancel((p) =>
            //{
            //    sp.DiscardInBuffer();

            //    sp.Write(buffer, 0, buffer.Length);
            //    Thread.Sleep(500);

            //    sp.Read(buffer, 0, buffer.Length);

            //    return buffer[0] == 0xAA;
            //}, "No hay comunicacion", 10, 500);


            try
            {
                byte[] data = File.ReadAllBytes(fileName);
                int bytesToSend = 128;
                buffer = new byte[bytesToSend];

                int bytesSent = 0;


                while (bytesSent < data.Length - 1)
                {

                    if (bytesSent + bytesToSend > data.Length - 1)
                        bytesToSend = data.Length - bytesSent - 1;

                    for (int x = 0; x < bytesToSend; x++)
                        buffer[x] = data[bytesSent + x];

                    sp.Write(buffer, 0, buffer.Length);
                    Thread.Sleep(1000);

                    bytesSent += buffer.Length;
                }

            }
            catch (FileNotFoundException e)
            {
                throw new Exception(e.Message);
            }
        }

        public enum RegistersModbus
        {
            KEY_PSWI = 0X0501,
            PROTOCOL = 0X0100,
            START_BOOT = 0x0700,
        }
    }
}