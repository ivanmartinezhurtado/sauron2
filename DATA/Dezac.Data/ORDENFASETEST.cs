﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Data
{
    [Table("APP.ORDENFASETEST")]
    public partial class ORDENFASETEST
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMORDEN { get; set; }
     
        [Required]
        [StringLength(10)]
        public string IDFASE { get; set; }

        [Required]
        public int SECUENCIA { get; set; }                       
    }
}
