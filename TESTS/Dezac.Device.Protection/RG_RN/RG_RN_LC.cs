﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading;
using Comunications.Utility;
using Dezac.Core.Utility;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.04)]
    public class RG_RN_LC : DeviceBase
    {
        #region Instanciacion y destrucción de la clase del equipo

        public RG_RN_LC()
        {
        }

        public RG_RN_LC(int port = 3)
        {           
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 19200, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);      
            else
                Modbus.PortCom = port;

            Modbus.UseFunction06 = true;
            Modbus.PerifericNumber = periferic;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #endregion

        #region Operaciones de lectura

        public string ReadFirmwareVersion()
        {
            LogMethod();
            var firmware = Modbus.ReadRegister((ushort)Registers.FIRMWARE_VERSION).ToString().Insert(1, ".");
            return firmware;
        }

        public Inputs ReadInputsState()
        {
            LogMethod();
            return (Inputs)Modbus.ReadRegister((ushort)Registers.INPUTS);
        }

        public ulong ReadNumSerie()
        {
            LogMethod();

            ushort[] num = { 0, 0, 0, 0 };
            num[0] = Modbus.ReadRegister((ushort)Registers.NUM_SERIE2_1);
            num[1] = Modbus.ReadRegister((ushort)Registers.NUM_SERIE2_2);
            num[2] = Modbus.ReadRegister((ushort)Registers.NUM_SERIE2_3);
            num[3] = Modbus.ReadRegister((ushort)Registers.NUM_SERIE2_4);

            return num.JoinRegisters<ulong>();
        }

        public uint ReadNumBastidor()
        {
            LogMethod();
            ushort[] num = { Modbus.ReadRegister((ushort)Registers.NUM_BASTIDOR_HI), Modbus.ReadRegister((ushort)Registers.NUM_BASTIDOR_LOW) };
            return num.JoinRegisters<uint>();
        }

        public ModelConfig ReadModelConfiguration()
        {
            LogMethod();
            return Modbus.Read<ModelConfig>();
        }

        public ErrorCode ReadErrorCode()
        {
            LogMethod();
            return (ErrorCode)Modbus.ReadRegister((ushort)Registers.ERROR_CODE);
        }

        public uint ReadCurrentGain()
        {
            LogMethod();
            ushort[] gain = { Modbus.ReadRegister((ushort)Registers.GAIN_I_HI), Modbus.ReadRegister((ushort)Registers.GAIN_I_LOW) };
            return gain.JoinRegisters<uint>();
        }

        public ushort ReadCurrentSelector()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.CURRENT_SELECTOR);
        }

        public ushort ReadTimeSelector()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.TIME_SELECTOR);
        }

        public double ReadCurrent()
        {
            LogMethod();
            ushort[] currentPoints = { Modbus.ReadRegister((ushort)Registers.CURRENT_HI), Modbus.ReadRegister((ushort)Registers.CURRENT_LOW) };

            var current = ComputeCurrent(currentPoints.JoinRegisters<uint>());
            return current;
        }

        public double ReadTripCurrent()
        {
            LogMethod();
            ushort[] currentPoints = { Modbus.ReadRegister((ushort)Registers.CURRENT_TRIP_HI), Modbus.ReadRegister((ushort)Registers.CURRENT_TRIP_LOW) };

            var current = ComputeTripCurrent(currentPoints.JoinRegisters<uint>());
            return current;
        }

        #endregion

        #region Operaciones de escritura

        public void WriteWorkMode(WorkModes mode)
        {
            LogMethod();
            if (mode == WorkModes.NORMAL)
            {
                var retries = Modbus.Retries;
                Modbus.Retries = 0;
                Modbus.WithTimeOut((m) => { Modbus.Write((ushort)Registers.WORK_MODE, (ushort)mode); });
                Modbus.Retries = retries;
                Thread.Sleep(2500);
            }
            else
                Modbus.Write((ushort)Registers.WORK_MODE, (ushort)mode);
        }

        public void WriteFlagTest()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.WORK_MODE, (ushort)WorkModes.TEST);
        }

        public void WriteNumSerie(ulong num)
        {
            LogMethod();

            var splited = num.ToUshorts();

            Modbus.Write((ushort)Registers.NUM_SERIE2_1, splited[0]);
            Thread.Sleep(100);
            Modbus.Write((ushort)Registers.NUM_SERIE2_2, splited[1]);
            Thread.Sleep(100);
            Modbus.Write((ushort)Registers.NUM_SERIE2_3, splited[2]);
            Thread.Sleep(100);
            Modbus.Write((ushort)Registers.NUM_SERIE2_4, splited[3]);
        }

        public void WriteNumBastidor(uint num)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.NUM_BASTIDOR_HI, (int)num);
        }

        public void WriteLeds(Leds leds)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.LEDS, (ushort)leds);
        }

        public void WriteRelay(Relay relay)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RELAY, (ushort)relay);
        }

        public void WriteTestToroidal()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TEST_TORO, (ushort)0x0008);
        }

        public void WriteModelConfiguration(ModelConfig config)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.MODEL, (ushort)config.model);
        }

        public void WriteCurrentGain(uint gain)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.GAIN_I_HI, (int)gain);
        }

        #endregion

        #region Enums

        public enum Registers
        {
            FIRMWARE_VERSION = 0x0000,
            WORK_MODE = 0x0001,
            TEST_TORO = 0x0002,
            RELAY = 0x0002,
            LEDS = 0x0003,
            INPUTS = 0x0004,
            CURRENT_SELECTOR = 0x0005,
            TIME_SELECTOR = 0x0006,
            GAIN_I_HI = 0x0007,
            GAIN_I_LOW = 0x0008,
            MODEL = 0x0009,
            NUM_BASTIDOR_HI = 0x000C,
            NUM_BASTIDOR_LOW = 0x000D,
            NUM_SERIE2_1 = 0x0012,
            NUM_SERIE2_2 = 0x0013,
            NUM_SERIE2_3 = 0x0014,
            NUM_SERIE2_4 = 0x0015,
            CURRENT_HI = 0x0016,
            CURRENT_LOW = 0x0017,
            CURRENT_TRIP_HI = 0x0018,
            CURRENT_TRIP_LOW = 0x0019, 
            ERROR_CODE = 0x000E,
        }

        public enum WorkModes
        {
            NORMAL = 0x0000,
            TEST = 0x0002,
            ADJUST = 0x0003
        }

        [Flags]
        public enum Inputs
        {
            NO_INPUT = 0x0000,
            [Description("Test")]
            TEST_KEY = 0x0008,
            [Description("Reset")]
            RESET_KEY = 0x0010,
            TORO_ERROR = 0x0020,
        }

        [Flags]
        public enum Leds
        {
            ALL_OFF = 0x0000,
            VERDE_POWER = 0x0100,
            ROJO_TRIP = 0x0200,
        }

        [Flags]
        public enum Relay
        {
            RELAY_OFF = 0x0000,
            RELAY_ON = 0x0001,
        }

        [Flags]
        public enum ErrorCode
        {
            [Description("Sin error")]
            NO_ERROR = 0x0000,
        }

        public enum CurrentSelectorPosition
        {
            [Description("escala 1")]
            ESC_1 = 0,
            [Description("escala 2")]
            ESC_2 = 1,
            [Description("escala 3")]
            ESC_3 = 2,
            [Description("escala 4")]
            ESC_4 = 3,
            [Description("escala 5")]
            ESC_5 = 4,
            [Description("escala 6")]
            ESC_6 = 5,
            [Description("escala 7")]
            ESC_7 = 6,
        }

        public enum TimeSelectorPosition
        {
            [Description("escala Referencia")]
            ESC_REF = 7,
            [Description("escala 1")]
            ESC_1 = 7,
            [Description("escala 2")]
            ESC_2 = 8,
            [Description("escala 3")]
            ESC_3 = 9,
            [Description("escala 4")]
            ESC_4 = 10,
            [Description("escala 5")]
            ESC_5 = 11,
            [Description("escala 6")]
            ESC_6 = 12,
            [Description("escala 7")]
            ESC_7 = 13,
        }

        #endregion

        #region Structures 

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.MODEL)]
        public struct ModelConfig
        {
            public ushort model;
            public string ModelString
            {
                get
                {
                    switch (model)
                    {
                        case 0x000A:
                            return "RGE-R / RN-R";
                        case 0x0014:
                            return "RGE-R1 / RN-R1";
                        case 0x001E:
                            return "RGE-R-SIEMENS";
                        default:
                            return "VALOR_DESCONOCIDO";
                    }
                }
                set
                {
                    switch (value.ToUpper().Trim())
                    {
                        case "RGE-R":
                        case "RN-R":
                            model = 0x000A;
                            break;
                        case "RGE-R1":
                        case "RN-R1":
                            model = 0x0014;
                            break;
                        case "RGE-R-SIEMENS":
                            model = 0x001E;
                            break;
                        default:
                            throw new Exception("Variable introducida para el Modelo es invalida. Los posibles son (RGE-R, RN-R, RGE-R1, RN-R1, RGE-R-SIEMENS)");
                    }
                }
            }
        }

        #endregion

        #region Private functions

        private double ComputeCurrent(uint points)
        {
            var calibrationConst = ReadCurrentGain();

            var current = (Math.Sqrt(points / 64) / Math.Sqrt(calibrationConst / 64.0)) * 1000;

            return current; 
        }

        private double ComputeTripCurrent(uint points)
        {
            var calibrationConst = ReadCurrentGain();

            var current = (Math.Sqrt(points) / Math.Sqrt(calibrationConst / 64.0)) * 100;

            return current;
        }

        #endregion

    }
}
