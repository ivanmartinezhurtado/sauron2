﻿using Dezac.Tests.UserControls;
using System;
using System.Windows.Forms;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    public partial class BoxReportConfigView : DialogViewBase
    {
        protected AppService app;

        public int UnidadesCaja { get; set; }
        public int CajaActual { get; set; }

        public BoxReportConfigView()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (DesignMode)
                return;

            app = DI.Resolve<AppService>();

            if (app.Model == null)
                return;

            if (app.Model.Orden == null)
                return;

            txtUnidadesCaja.Text = app.Model.EquiposPorCaja.ToString();
            txtCajaActual.Text = app.Model.CajaActual.ToString();
        }

        public override bool ValidateView(DialogResult result)
        {
            if (result == DialogResult.Cancel)
                return true;

            if (string.IsNullOrEmpty(txtUnidadesCaja.Text) || string.IsNullOrEmpty(txtCajaActual.Text))
                return false;

            app.Model.EquiposPorCaja = Convert.ToInt32(txtUnidadesCaja.Text);
            app.Model.CajaActual = Convert.ToInt32(txtCajaActual.Text);

            return true;
        }


    }

}
