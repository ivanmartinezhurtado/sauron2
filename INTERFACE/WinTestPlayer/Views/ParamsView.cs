﻿using Dezac.Services;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using System;
using System.Windows.Forms;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    public partial class ParamsView : UserControl
    {
        protected readonly AppService app;

        public ParamsView()
        {
            InitializeComponent();
        }

        public void SetProducto(int idProducto, int version, string idFase)
        {
            var data = new TestContextModel
            {
                NumProducto = idProducto,
                Version = version,
                IdFase = idFase,
            };

            using (IDezacService svc = DI.Resolve<IDezacService>())
            {
                System.Collections.Generic.List<Dezac.Data.T_GRUPOPARAMETRIZACION> parametros = svc.GetGruposParametrizacion(idProducto, version, idFase);
                if (parametros == null)
                    throw new Exception("Grupos de parametrización inexistentes");

                parametros.ForEach(p =>
                {
                    ParamValueCollection list = data.GetGroupList(p.NUMTIPOGRUPO);
                    if (list != null)
                        foreach (Dezac.Data.T_VALORPARAMETRO vp in p.T_VALORPARAMETRO)
                        {
                            var pv = new ParamValue
                            {
                                IdGrupo = vp.NUMGRUPO,
                                IdTipoGrupo = (TipoGrupo)p.NUMTIPOGRUPO,
                                Grupo = list.Name,
                                IdParam = vp.NUMPARAM,
                                ValorInicio = vp.VALORINICIO,
                                Valor = vp.VALOR,
                                IdTipoValor = vp.T_PARAMETRO.NUMTIPOVALOR,
                                IdUnidad = (ParamUnidad)vp.T_PARAMETRO.NUMUNIDAD,
                                Unidad = vp.T_PARAMETRO.T_UNIDAD.UNIDAD,
                                Name = vp.T_PARAMETRO.PARAM,
                                IdCategoria = vp.IDCATEGORIA
                            };

                            list.Add(pv);
                        }
                });
            }

            UpdateUI(data);
        }

        private void UpdateUI(TestContextModel data)
        {
            if (data == null)
                return;

            bsList.Clear();

            data.Configuracion.ForEach(p => bsList.Add(p));
            data.Margenes.ForEach(p => bsList.Add(p));
            data.Identificacion.ForEach(p => bsList.Add(p));
            data.Comunicaciones.ForEach(p => bsList.Add(p));
            data.Consignas.ForEach(p => bsList.Add(p));
            data.Resultados.ForEach(p => bsList.Add(p));
            data.Parametrizacion.ForEach(p => bsList.Add(p));
            data.VectorHardware.ForEach(p => bsList.Add(p));
        }
    }
}
