using log4net;
using Newtonsoft.Json;
using Renci.SshNet;
using Renci.SshNet.Common;
using System;

namespace Comunications.Ethernet
{
    public class SSHClient : IDisposable
    {
        private SshClient _sshClient;
        private ShellStream shell;

        protected static readonly ILog _logger = LogManager.GetLogger(typeof(SSHClient));
        private string HostName { get; set; }
        private string UserName { get; set; }
        private string Password { get; set; }
        private PrivateKeyFile privatKeyFile { get; set; }

        public SSHClient(string hostname, string username, string password, ILog logger = null, string keyFile = null)
        {
            this.HostName = hostname;
            this.UserName = username;
            this.Password = password;
            Logger = logger ?? _logger;

            if (!string.IsNullOrEmpty(keyFile))
                privatKeyFile = new PrivateKeyFile(keyFile);

            if (privatKeyFile == null)
            {
                Logger.InfoFormat("Creando Objecto SSH --> {0} con username {1} y password {2}", HostName, UserName, Password);
                _sshClient = new SshClient(HostName, UserName, Password);
            }
            else
            {
                Logger.InfoFormat("Creando Objecto SSH --> {0} con username {1} y privateKeyFile", HostName, UserName);
                _sshClient = new SshClient(HostName, UserName, privatKeyFile);
            }
            
            //shell = _sshClient.CreateShellStream("tty", 0, 0, 0, 0, 2048);
        }

        protected ILog Logger { get; set; }

        [JsonIgnore]
        public ConnectionInfo ConnectionInfo
        {
            get { return _sshClient.ConnectionInfo; }
        }
        [JsonIgnore]
        protected SshClient Ssh
        {
            get
            {
                if (_sshClient == null)
                {
                    if (privatKeyFile == null)
                    {
                        Logger.InfoFormat("Creando Objecto SSH con username y password");
                        _sshClient = new SshClient(HostName, UserName, Password);
                    }
                    else
                    {
                        Logger.InfoFormat("Creando Objecto SSH con username y privateKeyFile");
                        _sshClient = new SshClient(HostName, UserName, privatKeyFile);
                    }
                }

                Connect();

                return _sshClient;
            }
        }
        [JsonIgnore]
        public ShellStream Shell
        {
            get
            {
                if (shell == null)
                    shell = Ssh.CreateShellStream("tty", 0, 0, 0, 0, 2048);

                return shell;
            }
        }

        public string Run(string cmd, bool ThowExcepction = true)
        {
            try
            {
                Logger.InfoFormat("TX Msg: {0}", cmd);

                var result = Ssh.RunCommand(cmd);

                if (result.ExitStatus != 0 && ThowExcepction)
                    throw new SshException(result.Error);

                if (!string.IsNullOrEmpty(result.Error))
                    Logger.InfoFormat("RX Msg: {0}", result.Error);

                Logger.InfoFormat("RX Msg: {0}", result.Result);

                return result.Result;
            }
            catch (Exception e)
            {
                Logger.ErrorFormat("{0} -> {1}", e.GetType().Name, e);
                Close();
                throw;
            }
        }

        public void Connect()
        {
            try
            {
                if (!_sshClient.IsConnected)
                {
                    _sshClient.Connect();
                    Logger.InfoFormat("Connect OK");
                }
            }
            catch (Exception e)
            {
                Logger.ErrorFormat("{0} -> {1}", e.GetType().Name, e);
                throw;
            }
        }

        public void Close()
        {
            try
            {
                if (_sshClient != null)
                {
                    if (shell != null)
                    {
                        shell.Close();
                        shell.Dispose();
                    }
                    _sshClient.Disconnect();
                    _sshClient.Dispose();
                    _sshClient = null;
                    Logger.InfoFormat("Close OK");
                }
            }
            catch (Exception e)
            {
                Logger.ErrorFormat("{0} -> {1}", e.GetType().Name, e);

                throw;
            }
        }

        public void Dispose()
        {
            Close();
        }
    }
}