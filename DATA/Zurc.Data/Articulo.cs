namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Articulo")]
    public partial class Articulo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [StringLength(20)]
        public string Codigo { get; set; }
        [StringLength(200)]
        public string Descripcion { get; set; }
        public DateTime FechaAlta { get; set; }
        [DefaultValue(false)]
        public bool Baja { get; set; }
        [ForeignKey("Familia")]
        public int? FamiliaId { get; set; }

        public Familia Familia { get; set; }

        public virtual List<ProductoFase> PRODUCTOFASE { get; set; }
    }
}
