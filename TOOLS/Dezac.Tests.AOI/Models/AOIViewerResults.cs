﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.AOI.Models
{
    public class AOIViewerResults
    {
        private const int BORDER = 10;
        private const int BORDER2 = 1;
        private const double ZOOM = 1.2;
        private const int WELDING_IMAGE_SIZE = 50;

        public HObject Image { get; set; }
        public HObject ROIWeldings { get; set; }
        public HObject ROIBoards { get; set; }
        public HalconInspectionResults InspectionResults { get; set; }
        public Size WindowsSize { get; set; }
        public int SelectedWelding { get; set; }

        public HObject ImageMirror { get; set; }
        public HObject ROIWeldingsTrans { get; set; }
        public HObject ROIBoardsTrans { get; set; }
        public HObject ROIKOWeldings { get; set; }
        public HObject ROIResolvedWeldings { get; set; }
        public HObject DrawRectangle { get; set; }
        public HObject WindowRectangle { get; set; }
        public HObject DrawW1Lines { get; set; }
        public HObject DrawW2Lines { get; set; }
        private HObject weldingImages;
        public HObject WeldingImages { get { return weldingImages; }}

        private double windows2X;
        private double windows2Y;
        private List<Point> weldingPositions;

        public string JsonFile { get; set; }

        public string Barcodes
        {
            get
            {
                return string.Join("_", InspectionResults.Barcodes);
            }
        }
        public int TotalFailures
        {
            get
            {
                return InspectionResults.WeldingsResult.Where(i => i != ResultEnum.OK).Count();
            }
        }
        public int RemainingFailures
        {
            get
            {
                return TotalFailures - InspectionResults.WeldingsRepair.Where(i => i != RepairEnum.SIN_REPARACION).Count();
            }
        }
        public int FalseFailures
        {
            get
            {
                return InspectionResults.WeldingsRepair.Where(i => i == RepairEnum.FALSO_FALLO).Count();
            }
        }
        public int RepairedFailures
        {
            get
            {
                return InspectionResults.WeldingsRepair.Where(i => (i != RepairEnum.SIN_REPARACION) && (i != RepairEnum.FALSO_FALLO)).Count();
            }
        }

        public AOIViewerResults(HObject _Image, HObject _ROIWeldings, HObject _ROIBoards, HalconInspectionResults _InspectionResults, Size _WindowsSize, string jsonFile)
        {
            Image = _Image;
            ROIBoards = _ROIBoards;
            ROIWeldings = _ROIWeldings;
            InspectionResults = _InspectionResults;
            WindowsSize = _WindowsSize;
            JsonFile = jsonFile;

            GenerateIconicVariables();
            GenerateROIVisualHelpers();
            ExtractWeldingImages();
        }

        private void GenerateIconicVariables()
        {
            HObject imageRotate, imageMirror, weldingsTrans, boardsTrans;

            HTuple homMat2DIdentity, homMat2DRotate, homMat2DTranslate, homMat2DReflect, imageWidth, imageHeight;

            //Extract image transformed
            HOperatorSet.RotateImage(Image, out imageRotate, 90, "constant");
            HOperatorSet.MirrorImage(imageRotate, out imageMirror, "column");
            ImageMirror = imageMirror;
            HOperatorSet.GetImageSize(imageMirror, out imageWidth, out imageHeight);

            //Extract regions transformed
            HOperatorSet.SetSystem("clip_region", "false");

            HOperatorSet.HomMat2dIdentity(out homMat2DIdentity);
            HOperatorSet.HomMat2dRotate(homMat2DIdentity, 3.1416 / 2, imageWidth / 2, imageHeight / 2, out homMat2DRotate);
            HOperatorSet.HomMat2dTranslate(homMat2DRotate, (imageHeight / 2) - (imageWidth / 2), (imageWidth / 2) - (imageHeight / 2), out homMat2DTranslate);
            HOperatorSet.HomMat2dReflect(homMat2DTranslate, 0, imageWidth / 2, imageHeight, imageWidth / 2, out homMat2DReflect);

            HOperatorSet.AffineTransRegion(ROIWeldings, out weldingsTrans, homMat2DReflect, "nearest_neighbor");
            ROIWeldingsTrans = weldingsTrans;
            HOperatorSet.AffineTransRegion(ROIBoards, out boardsTrans, homMat2DReflect, "nearest_neighbor");
            ROIBoardsTrans = boardsTrans;
            HOperatorSet.SetSystem("clip_region", "true");
        }

        public void GenerateROIVisualHelpers()
        {
            HObject koWeldings, welding, resolvedWeldings;

            //Extract KO(red) regions
            var koWeldingsList = Enumerable.Range(0, InspectionResults.WeldingsResult.Count).Where(i => InspectionResults.WeldingsResult[i] == ResultEnum.KO).ToList();
            HOperatorSet.GenEmptyObj(out koWeldings);
            for (int i = 0; i <= koWeldingsList.Count() - 1; i++)
            {
                HOperatorSet.SelectObj(ROIWeldingsTrans, out welding, (koWeldingsList[i]) + 1);
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.ConcatObj(koWeldings, welding, out ExpTmpOutVar_0);
                    koWeldings.Dispose();
                    koWeldings = ExpTmpOutVar_0;
                }
            }
            ROIKOWeldings = koWeldings;

            //Extract OK(green) regions
            var resolvedWeldingsList = Enumerable.Range(0, InspectionResults.WeldingsRepair.Count)
                .Where(i => InspectionResults.WeldingsResult[i] == ResultEnum.KO && InspectionResults.WeldingsRepair[i] != RepairEnum.SIN_REPARACION).ToList();
            HOperatorSet.GenEmptyRegion(out resolvedWeldings);
            for (int i = 0; i <= resolvedWeldingsList.Count() - 1; i++)
            {
                HOperatorSet.SelectObj(ROIWeldingsTrans, out welding, resolvedWeldingsList[i] + 1);
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.ConcatObj(resolvedWeldings, welding, out ExpTmpOutVar_0);
                    resolvedWeldings.Dispose();
                    resolvedWeldings = ExpTmpOutVar_0;
                }
            }
            ROIResolvedWeldings = resolvedWeldings;
        }

        public void GenerateDrawVisualHelpers(int rowCenter, int columnCenter, int row1, int column1, int row2, int column2) 
        {
            HObject rectangleOut, rectangleIn, drawRectangle, line1, line2, line3, line4, drawW1Lines, drawW2Lines;

            windows2X = columnCenter - (WindowsSize.Width / (2 * ZOOM));
            windows2Y = rowCenter - (WindowsSize.Height / (2 * ZOOM));

            // Draw Rectangle for W1
            HOperatorSet.GenRectangle1(out rectangleOut, rowCenter - (WindowsSize.Height / (2 * ZOOM)), columnCenter - (WindowsSize.Width / (2 * ZOOM)),
                rowCenter + (WindowsSize.Height / (2 * ZOOM)), columnCenter + (WindowsSize.Width / (2 * ZOOM)));
            HOperatorSet.GenRectangle1(out rectangleIn, (rowCenter - (WindowsSize.Height / (2 * ZOOM))) - BORDER, (columnCenter - (WindowsSize.Width / (2 * ZOOM))) - BORDER,
                (rowCenter + (WindowsSize.Height / (2 * ZOOM))) + BORDER, (columnCenter + (WindowsSize.Width / (2 * ZOOM))) + BORDER);
            HOperatorSet.Difference(rectangleOut, rectangleIn, out drawRectangle);
            DrawRectangle = drawRectangle;
            WindowRectangle = rectangleOut;

            //Draw lines for W1
            HOperatorSet.GenRectangle1(out line1, 0, columnCenter - (BORDER / 2), 10000, columnCenter + (BORDER / 2));
            HOperatorSet.GenRectangle1(out line2, rowCenter - (BORDER / 2), 0, rowCenter + (BORDER / 2), 10000);
            HOperatorSet.Union2(line1, line2, out drawW1Lines);
            DrawW1Lines = drawW1Lines;

            //Draw lines for W2
            HOperatorSet.GenRectangle1(out line1, row1 - WindowsSize.Height, column1 - (2 * BORDER2), row2 + WindowsSize.Height, column1 - BORDER2);
            HOperatorSet.GenRectangle1(out line2, row1 - WindowsSize.Height, column2 + BORDER2, row2 + WindowsSize.Height, column2 + (2 * BORDER2));
            HOperatorSet.Union2(line1, line2, out drawW2Lines);
            HOperatorSet.GenRectangle1(out line3, row1 - (2 * BORDER2), column1 - WindowsSize.Width, row1 - BORDER2, column2 + WindowsSize.Width);
            {
                HObject ExpTmpOutVar_0;
                HOperatorSet.Union2(drawW2Lines, line3, out ExpTmpOutVar_0);
                drawW2Lines.Dispose();
                drawW2Lines = ExpTmpOutVar_0;
            }
            HOperatorSet.GenRectangle1(out line4, row2 + BORDER2, column1 - WindowsSize.Width, row2 + (2 * BORDER2), column2 + WindowsSize.Width);
            {
                HObject ExpTmpOutVar_0;
                HOperatorSet.Union2(drawW2Lines, line4, out ExpTmpOutVar_0);
                drawW2Lines.Dispose();
                drawW2Lines = ExpTmpOutVar_0;
            }
            DrawW2Lines = drawW2Lines;
        }

        public void GenerateDrawVisualHelpers(int index)
        {
            HObject weldingSelected;
            SelectedWelding = index;
            HOperatorSet.SelectObj(ROIWeldingsTrans, out weldingSelected, SelectedWelding + 1);
            HTuple area, rowCenter, columnCenter;
            HOperatorSet.AreaCenter(weldingSelected, out area, out rowCenter, out columnCenter);
            HTuple row1, column1, row2, column2;
            HOperatorSet.SmallestRectangle1(weldingSelected, out row1, out column1, out row2, out column2);

            if (index != -1)
                GenerateDrawVisualHelpers((int)rowCenter.D, (int)columnCenter.D, row1.I, column1.I, row2.I, column2.I);
            else
            {
                DrawRectangle = null;
                WindowRectangle = null;
                DrawW1Lines = null;
                DrawW2Lines = null;
            }
        }

        public HObject GetWindows1Image()
        {
            HObject ImageChannelR, ImageChannelG, ImageChannelB, BoardDifference, RegionDilation, RegionDifference;
            HObject RegionUnion, Rectangle, ImageReduced, Board, BoardDilation, RGBImage;

            Board = null; BoardDilation = null; BoardDifference = null;

            HTuple NumberBoards, RowAux, ColumnAux, Row2Aux, Column2Aux, numberAux;

            HOperatorSet.CountObj(ROIBoardsTrans, out NumberBoards);
            ImageChannelG = ImageMirror.CopyObj(1, -1);
            ImageChannelR = ImageMirror.CopyObj(1, -1);
            ImageChannelB = ImageMirror.CopyObj(1, -1);

            // Paint Boards
            for (int i = 0; i <= NumberBoards - 1; i++)
            {
                HOperatorSet.SelectObj(ROIBoardsTrans, out Board, i + 1);
                HOperatorSet.DilationCircle(Board, out BoardDilation, 40);
                HOperatorSet.Difference(BoardDilation, Board, out BoardDifference);
                if (InspectionResults.Boards[i].Classification == ResultEnum.OK)
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(BoardDifference, ImageChannelG, out ExpTmpOutVar_0, 200, "fill");
                    ImageChannelG.Dispose();
                    ImageChannelG = ExpTmpOutVar_0;
                }
                else if (InspectionResults.Boards[i].Classification != ResultEnum.OK)
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(BoardDifference, ImageChannelR, out ExpTmpOutVar_0, 200, "fill");
                    ImageChannelR.Dispose();
                    ImageChannelR = ExpTmpOutVar_0;
                }
            }

            // Paints Weldings
            HOperatorSet.CountObj(ROIKOWeldings, out numberAux);
            if (numberAux > 0)
            {
                HOperatorSet.DilationCircle(ROIKOWeldings, out RegionDilation, 10);
                HOperatorSet.Difference(RegionDilation, ROIKOWeldings, out RegionDifference);
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(ROIKOWeldings, ImageChannelR, out ExpTmpOutVar_0, 255, "fill");
                    ImageChannelR.Dispose();
                    ImageChannelR = ExpTmpOutVar_0;
                }
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(RegionDifference, ImageChannelR, out ExpTmpOutVar_0, 255, "fill");
                    ImageChannelR.Dispose();
                    ImageChannelR = ExpTmpOutVar_0;
                }
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(RegionDifference, ImageChannelG, out ExpTmpOutVar_0, 0, "fill");
                    ImageChannelG.Dispose();
                    ImageChannelG = ExpTmpOutVar_0;
                }
                HOperatorSet.PaintRegion(RegionDifference, ImageMirror, out ImageChannelB, 0, "fill");
                RegionDilation.Dispose();
            }

            HOperatorSet.CountObj(ROIResolvedWeldings, out numberAux);
            if (numberAux > 0)
            {
                HOperatorSet.DilationCircle(ROIResolvedWeldings, out RegionDilation, 10);
                HOperatorSet.Difference(RegionDilation, ROIResolvedWeldings, out RegionDifference);
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(ROIResolvedWeldings, ImageChannelG, out ExpTmpOutVar_0, 255, "fill");
                    ImageChannelG.Dispose();
                    ImageChannelG = ExpTmpOutVar_0;
                }
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(RegionDifference, ImageChannelG, out ExpTmpOutVar_0, 255, "fill");
                    ImageChannelG.Dispose();
                    ImageChannelG = ExpTmpOutVar_0;
                }
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(RegionDifference, ImageChannelR, out ExpTmpOutVar_0, 0, "fill");
                    ImageChannelR.Dispose();
                    ImageChannelR = ExpTmpOutVar_0;
                }
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(RegionDifference, ImageChannelB, out ExpTmpOutVar_0, 0, "fill");
                    ImageChannelB.Dispose();
                    ImageChannelB = ExpTmpOutVar_0;
                }
                RegionDifference.Dispose();
            }

            if (DrawRectangle != null)
            {
                // DrawRectangle
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(DrawRectangle, ImageChannelG, out ExpTmpOutVar_0, 255, "fill");
                    ImageChannelG.Dispose();
                    ImageChannelG = ExpTmpOutVar_0;
                }
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(DrawRectangle, ImageChannelR, out ExpTmpOutVar_0, 255, "fill");
                    ImageChannelR.Dispose();
                    ImageChannelR = ExpTmpOutVar_0;
                }
            }

            // DrawLines
            if (DrawW1Lines != null)
            {
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(DrawW1Lines, ImageChannelG, out ExpTmpOutVar_0, 255, "fill");
                    ImageChannelG.Dispose();
                    ImageChannelG = ExpTmpOutVar_0;
                }
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(DrawW1Lines, ImageChannelR, out ExpTmpOutVar_0, 255, "fill");
                    ImageChannelR.Dispose();
                    ImageChannelR = ExpTmpOutVar_0;
                }
            }

            HOperatorSet.Compose3(ImageChannelR, ImageChannelG, ImageChannelB, out RGBImage);

            //Adap to the new window
            HOperatorSet.Union1(ROIBoardsTrans, out RegionUnion);
            HOperatorSet.SmallestRectangle1(RegionUnion, out RowAux, out ColumnAux, out Row2Aux, out Column2Aux);
            HOperatorSet.GenRectangle1(out Rectangle, RowAux - 200, ColumnAux - 200, Row2Aux + 200, Column2Aux + 200);
            HOperatorSet.ReduceDomain(RGBImage, Rectangle, out ImageReduced);
            //HOperatorSet.CropDomain(ImageReduced, out ImageWindow1);
            //HOperatorSet.ZoomImageSize(ImageReduced, out ImageWindow1, WindowsSize.Width, WindowsSize.Height, "bicubic");

            ImageChannelR.Dispose(); ImageChannelG.Dispose(); ImageChannelB.Dispose(); Board.Dispose(); BoardDilation.Dispose();
            BoardDifference.Dispose(); RGBImage.Dispose(); RegionUnion.Dispose(); Rectangle.Dispose();
            //ImageReduced.Dispose();

            return ImageReduced;
            //return ImageWindow1;
        }

        public HObject GetWindows2Image()
        {
            HObject ImageChannelR, ImageChannelG, ImageChannelB, RGBImage, ImageReduced, ImageWindow2;

            ImageChannelR = ImageMirror.CopyObj(1, -1);
            ImageChannelG = ImageMirror.CopyObj(1, -1);
            ImageChannelB = ImageMirror.CopyObj(1, -1);

            // Draw lines 
            if (DrawW2Lines != null)
            {
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(DrawW2Lines, ImageChannelG, out ExpTmpOutVar_0, 255, "fill");
                    ImageChannelG.Dispose();
                    ImageChannelG = ExpTmpOutVar_0;
                }
                {
                    HObject ExpTmpOutVar_0;
                    HOperatorSet.PaintRegion(DrawW2Lines, ImageChannelR, out ExpTmpOutVar_0, 255, "fill");
                    ImageChannelR.Dispose();
                    ImageChannelR = ExpTmpOutVar_0;
                }
            }

            HOperatorSet.Compose3(ImageChannelR, ImageChannelG, ImageChannelB, out RGBImage);

            HOperatorSet.ReduceDomain(RGBImage, WindowRectangle, out ImageReduced);
            HOperatorSet.CropDomain(ImageReduced, out ImageWindow2);

            return ImageWindow2;
        }

        public void ExtractWeldingImages()
        {
            HTuple number, row1, row2, column1, column2;
            HObject weldingRoi = null, imageAux = null, weldingRoiTrasn =null;

            HOperatorSet.CountObj(ROIWeldings, out number);
            HOperatorSet.GenEmptyObj(out HObject weldingImagesAux);
            weldingPositions = new List<Point>();

            for (int i = 1; i<= number; i++)
            {
                HOperatorSet.SelectObj(ROIWeldings, out weldingRoi, i);
                HOperatorSet.SmallestRectangle1(weldingRoi, out row1, out column1, out row2, out column2);
                HOperatorSet.CropRectangle1(Image, out imageAux, row1, column1, row2, column2 );
                HOperatorSet.ZoomImageSize(imageAux, out imageAux, WELDING_IMAGE_SIZE, WELDING_IMAGE_SIZE, "bicubic");
                HOperatorSet.ConcatObj(weldingImagesAux, imageAux, out weldingImagesAux);
                HOperatorSet.AreaCenter(weldingRoi, out _, out _, out _);

                HOperatorSet.SelectObj(ROIWeldingsTrans, out weldingRoiTrasn, i);
                HOperatorSet.SmallestRectangle1(weldingRoiTrasn, out row1, out column1, out row2, out column2);
                weldingPositions.Add(new Point((int)(column1 + (column2 - column1) / 2 + 50), (int)(row1 + (row2 - row1) / 2 + 50)));
            }

            HOperatorSet.CopyImage(weldingImagesAux, out weldingImages);

            weldingRoi.Dispose(); weldingImagesAux.Dispose(); imageAux.Dispose();
        }

        public int? DetermineSelectedWelding(double xClick, double yClick)
        {
            var x = (xClick * ZOOM) + windows2X;
            var y = (yClick * ZOOM) + windows2Y;

            int? position = null;
            double bestDistance = 100;

            for(int i = 0; i < weldingPositions.Count; i++)
            {
                var distance = Math.Sqrt(Math.Pow((weldingPositions[i].X - x), 2) + Math.Pow((weldingPositions[i].Y - y), 2));
                if (distance < bestDistance)
                {
                    bestDistance = distance;
                    position = i;
                }
            }

            return position;
        }
    }

}
