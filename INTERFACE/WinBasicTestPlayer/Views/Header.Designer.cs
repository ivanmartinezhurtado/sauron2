﻿namespace WinTestPlayer.Views
{
    partial class Header
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Header));
            this.pnlLeft = new System.Windows.Forms.Panel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.labelTitulo = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblDescripcion = new System.Windows.Forms.Label();
            this.btPlay = new Telerik.WinControls.UI.RadButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lblProducto = new System.Windows.Forms.Label();
            this.lblOperario = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelProducto = new System.Windows.Forms.Label();
            this.lblDesc = new System.Windows.Forms.Label();
            this.lblNumOrden = new System.Windows.Forms.Label();
            this.lblOrden = new System.Windows.Forms.Label();
            this.object_9ab53d85_09c9_4d32_bf57_92280561d02c = new Telerik.WinControls.RootRadElement();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPlay)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlLeft
            // 
            this.pnlLeft.BackColor = System.Drawing.Color.DodgerBlue;
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(50, 96);
            this.pnlLeft.TabIndex = 0;
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.White;
            this.radPanel1.Controls.Add(this.labelTitulo);
            this.radPanel1.Controls.Add(this.pictureBox1);
            this.radPanel1.Controls.Add(this.lblDescripcion);
            this.radPanel1.Controls.Add(this.btPlay);
            this.radPanel1.Controls.Add(this.lblProducto);
            this.radPanel1.Controls.Add(this.lblOperario);
            this.radPanel1.Controls.Add(this.label2);
            this.radPanel1.Controls.Add(this.labelProducto);
            this.radPanel1.Controls.Add(this.lblDesc);
            this.radPanel1.Controls.Add(this.lblNumOrden);
            this.radPanel1.Controls.Add(this.lblOrden);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(50, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(1230, 96);
            this.radPanel1.TabIndex = 35;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radPanel1.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // labelTitulo
            // 
            this.labelTitulo.AutoSize = true;
            this.labelTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitulo.ForeColor = System.Drawing.Color.Navy;
            this.labelTitulo.Location = new System.Drawing.Point(830, 31);
            this.labelTitulo.Name = "labelTitulo";
            this.labelTitulo.Size = new System.Drawing.Size(0, 31);
            this.labelTitulo.TabIndex = 55;
            this.labelTitulo.UseWaitCursor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WinTestPlayer.Properties.Resources.circutor_2;
            this.pictureBox1.Location = new System.Drawing.Point(7, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(106, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 54;
            this.pictureBox1.TabStop = false;
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.AutoSize = true;
            this.lblDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcion.ForeColor = System.Drawing.Color.Navy;
            this.lblDescripcion.Location = new System.Drawing.Point(428, 18);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(76, 16);
            this.lblDescripcion.TabIndex = 53;
            this.lblDescripcion.Text = "Sequencia:";
            // 
            // btPlay
            // 
            this.btPlay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btPlay.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.btPlay.Enabled = false;
            this.btPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPlay.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btPlay.ImageIndex = 2;
            this.btPlay.ImageList = this.imageList1;
            this.btPlay.Location = new System.Drawing.Point(1130, 9);
            this.btPlay.Name = "btPlay";
            this.btPlay.Size = new System.Drawing.Size(75, 73);
            this.btPlay.TabIndex = 28;
            this.btPlay.TabStop = false;
            this.btPlay.Text = "Run";
            this.btPlay.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btPlay.Click += new System.EventHandler(this.btPlay_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Crystal_Clear_action_apply128x128.png");
            this.imageList1.Images.SetKeyName(1, "Crystal_Clear_action_button_cancel.png");
            this.imageList1.Images.SetKeyName(2, "Play-Normal-icon.png");
            this.imageList1.Images.SetKeyName(3, "Stop-Normal-Red-icon.png");
            // 
            // lblProducto
            // 
            this.lblProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProducto.ForeColor = System.Drawing.Color.Black;
            this.lblProducto.Location = new System.Drawing.Point(505, 56);
            this.lblProducto.Name = "lblProducto";
            this.lblProducto.Size = new System.Drawing.Size(272, 16);
            this.lblProducto.TabIndex = 45;
            this.lblProducto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblProducto.Visible = false;
            // 
            // lblOperario
            // 
            this.lblOperario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperario.Location = new System.Drawing.Point(214, 18);
            this.lblOperario.Name = "lblOperario";
            this.lblOperario.Size = new System.Drawing.Size(105, 16);
            this.lblOperario.TabIndex = 44;
            this.lblOperario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(147, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 16);
            this.label2.TabIndex = 43;
            this.label2.Text = "Operario";
            // 
            // labelProducto
            // 
            this.labelProducto.AutoSize = true;
            this.labelProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProducto.ForeColor = System.Drawing.Color.Navy;
            this.labelProducto.Location = new System.Drawing.Point(437, 56);
            this.labelProducto.Name = "labelProducto";
            this.labelProducto.Size = new System.Drawing.Size(62, 16);
            this.labelProducto.TabIndex = 38;
            this.labelProducto.Text = "Producto";
            this.labelProducto.Visible = false;
            // 
            // lblDesc
            // 
            this.lblDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesc.ForeColor = System.Drawing.Color.Black;
            this.lblDesc.Location = new System.Drawing.Point(510, 18);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(338, 16);
            this.lblDesc.TabIndex = 37;
            this.lblDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumOrden
            // 
            this.lblNumOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumOrden.ForeColor = System.Drawing.Color.Black;
            this.lblNumOrden.Location = new System.Drawing.Point(214, 56);
            this.lblNumOrden.Name = "lblNumOrden";
            this.lblNumOrden.Size = new System.Drawing.Size(184, 16);
            this.lblNumOrden.TabIndex = 36;
            this.lblNumOrden.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblNumOrden.Visible = false;
            // 
            // lblOrden
            // 
            this.lblOrden.AutoSize = true;
            this.lblOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrden.ForeColor = System.Drawing.Color.Navy;
            this.lblOrden.Location = new System.Drawing.Point(163, 56);
            this.lblOrden.Name = "lblOrden";
            this.lblOrden.Size = new System.Drawing.Size(45, 16);
            this.lblOrden.TabIndex = 35;
            this.lblOrden.Text = "Orden";
            this.lblOrden.Visible = false;
            // 
            // object_9ab53d85_09c9_4d32_bf57_92280561d02c
            // 
            this.object_9ab53d85_09c9_4d32_bf57_92280561d02c.Name = "object_9ab53d85_09c9_4d32_bf57_92280561d02c";
            this.object_9ab53d85_09c9_4d32_bf57_92280561d02c.StretchHorizontally = true;
            this.object_9ab53d85_09c9_4d32_bf57_92280561d02c.StretchVertically = true;
            // 
            // Header
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.pnlLeft);
            this.Name = "Header";
            this.Size = new System.Drawing.Size(1280, 96);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPlay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlLeft;
        private Telerik.WinControls.UI.RadButton btPlay;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.Label lblProducto;
        private System.Windows.Forms.Label lblOperario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelProducto;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.Label lblNumOrden;
        private System.Windows.Forms.Label lblOrden;
        private System.Windows.Forms.ImageList imageList1;
        private Telerik.WinControls.RootRadElement object_9ab53d85_09c9_4d32_bf57_92280561d02c;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblDescripcion;
        private System.Windows.Forms.Label labelTitulo;
    }
}
