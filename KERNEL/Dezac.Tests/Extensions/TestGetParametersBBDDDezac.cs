﻿using Dezac.Core.Exceptions;
using Dezac.Data.ViewModels;
using Dezac.Services;
using Dezac.Tests.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskRunner.Model;

namespace Dezac.Tests.Extensions
{
    public static class TesGetParametersBBDDDezac
    {
        public static string[] GetMacServiceDezac(this TestBase testBase, int numMacs, string VariableName="")
        {           
            bool opa = false;
            bool opaPGI = false;

            var valueML = testBase.Identificacion.MAC_CUSTOMER.ToUpper();
            if(valueML == "MARCA")
                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("Cliente no especificado por el Model Leader en BBDD para obtener MAC").Throw();
     
            opa = valueML.Contains("ORM");
            
            using (DezacService svc = new DezacService())
            {               
                var producto = svc.GetProducto(testBase.Model.NumProducto, testBase.Model.Version);
                if (producto != null)
                    opaPGI = producto.ARTICULO.IDMARCAVENTA == "ORM";
            }
            
            if(opa != opaPGI)
                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("Cliente especificado por el Model Leader para obtener rango de MAC no coincide con el del PGI").Throw();

            if (string.IsNullOrEmpty(VariableName))
                VariableName = ConstantsParameters.TestInfo.MAC;

            if (opa)
                return GetMacOPA(testBase, numMacs, VariableName);
            else
                return GetMacDezac(testBase, numMacs, VariableName);
        }

        private static string[] GetMacDezac(TestBase testBase, int numMacs, string VariableName)
        {
            string[] macs;

            testBase.Logger.Debug(" Pedimos MAC's Rango de Dezac");

            using (var DWS = new ServiceWebDezac.MacTrackWSService())
            {
                macs = DWS.getMac(testBase.Model.IdOperario, numMacs, testBase.Model.NumOrden.Value.ToString());
            }

            Assert.IsNotNullOrEmpty(macs, TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("No se han obtenido MAC del Servicio Web"));

            if (numMacs == 1)
            {
                string mac = macs.FirstOrDefault();
                Assert.IsNotNullOrEmpty(mac, TestException.Create().SOFTWARE.BBDD.VARIABLE_ES_NULA("MAC nula"));

                testBase.Logger.Debug(string.Format(" MAC obtenida = {0}", mac));

                SequenceContext.Current.Variables.AddOrUpdate(VariableName, mac);

               testBase.TestInfo.NumMAC = mac;
            }
            else
            {
                if (macs.Count() < 1)
                    throw new Exception("No se ha obtenido una lista de MAC del Servicio Web");

                for (var i = 0; i < macs.Count(); i++)
                {
                    testBase.Logger.Debug(string.Format(" MAC({0}) obtenida = {1}", i, macs[i]));
                    SequenceContext.Current.Variables.AddOrUpdate(string.Format("{0}({1})", VariableName, i), macs[i]);
                }
            }

            return macs;      
        }

        private static string[] GetMacOPA(TestBase testBase, int numMacs, string VariableName)
        {
            var macFixed = "9802D8";
            string[] macs = new string[numMacs];

            testBase.Logger.Debug(string.Format(" Pedimos MAC's del Rango de ORMAZABAL"));

            using (DezacService svc = new DezacService())
            {
                for (int macToGet = 0; macToGet < numMacs; macToGet++)
                {               
                    var macVariable = svc.GetMAC_OPA();

                    if(macVariable < 0x7186A0 || macVariable > 0x7C34FF)
                        TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("GetMAC_OPA FUERA DE RANGO").Throw();

                    macs[macToGet] = String.Format("{0}{1}", macFixed , macVariable.ToString("X6"));
                }
            }

            if (numMacs == 1)
            {
                Assert.IsNotNullOrEmpty(macs.FirstOrDefault(), TestException.Create().SOFTWARE.BBDD.VARIABLE_ES_NULA("MAC nula"));
                testBase.Logger.Debug(string.Format(" MAC obtenida = {0}", macs.FirstOrDefault()));
                SequenceContext.Current.Variables.AddOrUpdate(VariableName, macs.FirstOrDefault());
                testBase.TestInfo.NumMAC = macs.FirstOrDefault();         
            }
            else
            {
                if (macs.Count() < 1)
                    throw new Exception("No se ha obtenido una lista de MAC dentro del rango de OPA del Servicio Web");

                for (var i = 0; i < macs.Count(); i++)
                {
                    testBase.Logger.Debug(string.Format(" MAC({0}) obtenida = {1}", i, macs[i]));
                    SequenceContext.Current.Variables.AddOrUpdate(string.Format("{0}({1})", VariableName, i), macs[i]);
                }
            }

            return macs;
        }

        public static bool HasComponentInStructure(this TestBase testBase, int numProducto, int version, int componentToFind)
        {
            List<ComponentsEstructureInProduct> structure = new List<ComponentsEstructureInProduct>(); 

            using (var svc = new DezacService())
            {
                 structure = svc.GetComponentsEstructureInProduct(numProducto, version, false, false); // Esta consulta se tiene que modificar               data = svc.GetSequenceFileData(PRODUCTO, fase.IDFASE, ref tipoSecuencia, ref nombreFichero);
            }

            foreach (ComponentsEstructureInProduct component in structure)
                if (component.numproducto == componentToFind)
                    return true;

            return false;
        }
    }
}
