namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTREPORTVALOR")]
    public partial class T_TESTREPORTVALOR
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMVALOR { get; set; }

        public int NUMFAMILIA { get; set; }

        public int? NUMPRODUCTO { get; set; }

        public int? VERSION { get; set; }

        [Required]
        [StringLength(10)]
        public string IDFASE { get; set; }

        public int? NUMPARAM { get; set; }

        [StringLength(100)]
        public string DESCRIPCION { get; set; }

        [StringLength(100)]
        public string VALORAPLICADO { get; set; }

        [StringLength(100)]
        public string TOLERANCIA { get; set; }

        public int NIVEL { get; set; }

        public int ORDEN { get; set; }

        [Required]
        [StringLength(1)]
        public string VISIBLE { get; set; }

        public int? TIPO { get; set; }

        [StringLength(20)]
        public string UNIDAD { get; set; }

        public int? VERSIONREPORT { get; set; }

        public int? NUMPARAM2 { get; set; }

        public virtual FASE FASE { get; set; }

        public virtual PRODUCTO PRODUCTO { get; set; }

        public virtual T_FAMILIA T_FAMILIA { get; set; }

        public virtual T_PARAMETRO T_PARAMETRO { get; set; }

        public virtual T_PARAMETRO T_PARAMETRO1 { get; set; }
    }
}
