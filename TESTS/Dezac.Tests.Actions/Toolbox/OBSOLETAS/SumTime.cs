﻿using Dezac.DLMS;
using System.ComponentModel;
using System.Drawing;


namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(SumTime_Description))]
   

    public class SumTime : ActionBase
    {

        [Category("fecha")]
        [Description("fecha actual")]
        public string Clock { get; set; }


        [Category("año")]
        [Description("años a sumar a la fecha actual(valor por defecto 0")]
        [DefaultValue(0)]
        public int año { get; set; }

        [Category("mes")]
        [Description("meses a sumar a la fecha actual(valor por defecto 0")]
        [DefaultValue(0)]
        public int mes { get; set; }

        [Category("dias")]
        [Description("dias a sumar a la fecha actual(valor por defecto 1")]
        [DefaultValue(1)]
        public double dias { get; set; }

        [Category("horas")]
        [Description("horas a sumar a la fecha actual(valor por defecto 0")]
        [DefaultValue(0)]
        public double horas { get; set; }

        [Category("minutos")]
        [Description("minutos a sumar a la fecha actual(valor por defecto 0")]
        [DefaultValue(0)]
        public double min { get; set; }

        [Category("segundos")]
        [Description("segundos a sumar a la fecha actual(valor por defecto 0")]
        [DefaultValue(0)]
        public double segundos { get; set; }

        public override void Execute()
        {
            var T = GetVariable("DlmsDevice") as DLMSDevice;
            T.SumT(año,mes,dias,horas,min,segundos);
        }
    }

    public class SumTime_Description : ActionDescription
    {
        public override string Name { get { return "SumTime"; } }
        public override string Description { get { return "Sumar tiempo a reloj del dispositivo(en días)"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Logo_DLMS; } }
        public override string Dependencies { get { return "CreateDlmsSerialPort;DisposeDlmsSerialPort:2"; } }
    }




    
}
