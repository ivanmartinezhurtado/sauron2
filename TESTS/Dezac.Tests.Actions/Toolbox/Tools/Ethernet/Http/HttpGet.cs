﻿using Comunications.Ethernet;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(HttpGetActionDescription))]
    public class HttpGet : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre o dirección del servidor")]
        public ActionVariable Host { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Dirección URL/URI")]
        public ActionVariable URL { get; set; } = new ActionVariable();

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var url = URL.Value.ToString();
            var host = Host.Value.ToString();
            var resp = ComunicationHttpClient.HttpGetContent(host, url);

            var varName = DataOutput.Value.ToString() ?? Name;
            Context.Variables.AddOrUpdate(varName, resp);
            Context.ResultList.Add(string.Format("{0} = {1}", varName, resp));
        }
    }

    public class HttpGetActionDescription : ActionDescription
    {
        public override string Name { get { return "HttpGet"; } }
        public override string Description { get { return "Pedir al servidor un documento"; } }
        public override string Category { get { return "Tools/Ethernet/Http"; } }
        public override Image Icon { get { return Properties.Resources.HTTP; } }
    }
}
