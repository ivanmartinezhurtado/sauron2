﻿using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using Instruments.Auxiliars;
using Instruments.Measure;
using System;
using Dezac.Tests.Extensions;
using System.Collections.Generic;
using System.Linq;
using TaskRunner.Model;
using Dezac.Tests.Parameters;
using Dezac.Core.Exceptions;

namespace Dezac.Tests.Protection
{
    [TestVersion(2.19)]
    public class IVDSTest : TestBase
    {
        public string CAMERA_SN { get; set; }

        public void TestInitialization(string portMetrix)
        {
            SetSharedVariable("NUM_TEST", GetSharedVariable<List<string>>(ConstantsParameters.TestInfo.BASTIDORES).Where((s)=> s != "").Count());

            var portMTX = Convert.ToByte(GetSharedVariable<string>(portMetrix, portMetrix).Replace("COM", ""));
            var metrix = new MTX3293();
            metrix.PortCom = portMTX;
            SetSharedVariable("METRIX", metrix);

            var generator = new KeySight33500();
            SetSharedVariable("GENERATOR", generator);
        }

        public void TestSetVoltage(TipoFase tipoFase)
        {
            var metrix = GetSharedVariable<MTX3293>("METRIX");
            var numTest = GetSharedVariable<int>("NUM_TEST");
            SetSharedVariable("ERROR_MARGENES", false);

            double amplitud = Consignas.GetDouble(string.Format("AMPLITUD_{0}_{1}", tipoFase, numTest), 0, ParamUnidad.V);

            var signal = new SignalInfo();
            signal.Channel = 1;
            signal.Function = SignalFunction.SINUSOIDAL;
            signal.Frequency = 50;
            signal.Amplitude = amplitud;
            signal.VoltageOffset = 0;
            signal.Phase = 0;
            signal.UnitVoltage = UnitVoltage.VRMS;

            var generator = GetSharedVariable<KeySight33500>("GENERATOR");
            generator.SetSignal(signal);
            generator.On(1);
            Delay(1500, "Esperando consigna de señal");

            AdjustValueDef intensidadMetrix;

            intensidadMetrix = new AdjustValueDef { Name = Params.I.Null.TestPoint(string.Format("{0}_{1}", tipoFase, numTest)).Name, Min = Params.I.Null.TestPoint(string.Format("{0}_{1}", tipoFase, numTest)).Min(), Max = Params.I.Null.TestPoint(string.Format("{0}_{1}", tipoFase, numTest)).Max(), Unidad = ParamUnidad.mA };

            List<double> medida = new List<double>();
            double currentNeeded = (intensidadMetrix.Min + intensidadMetrix.Max) / 2d;

            TestMeasureBase(intensidadMetrix,
            (step) =>
            {
                if (step % 5 == 4)
                {
                    var medidaAverage = medida.GetRange(step - 4, 4).Sum() / 4;
                    Logger.Info("Rectificando la amplitud con la medida de Intensidad");
                    amplitud = signal.Amplitude.Value * ((((currentNeeded / medidaAverage) - 1) / ((step + 1) / 5d)) + 1);
                    signal.Amplitude = amplitud;
                    generator.SetSignal(signal);
                    generator.On(1);
                    Delay(4000, "Cambiando consigna generador de funciones");
                }
                medida.Add(metrix.ReadCurrent(MTX3293.measureTypes.AC));
                return medida[step];
            }, 2, 30, 1000);
        }

        public void TestHalcon(TipoFase tipoFase, FasesEnum fase)
        {
            CAMERA_SN = GetSharedVariable<string>("CAMERA_SN", CAMERA_SN);

            var muestras = 1;
            if (tipoFase == TipoFase.OFF)
                muestras = (int)Configuracion.GetDouble("NUMERO_MUESTRAS_OFF_TOTAL", 5, ParamUnidad.SinUnidad);

            for (int i = 0; i < muestras; i++)
            {
                this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLedsReturnColors, CAMERA_SN, $"IVDS_LEDS_{tipoFase}_{fase}_{i}", $"LEDS_{tipoFase}_{fase}_{i}", "IVDS", 1);
                var halconProcedure = GetVariable($"HALCON_PROCEDURE_IVDS_LEDS_{tipoFase}_{fase}_{i}");
                SetSharedVariable("LEDS_" + tipoFase.ToString() + i.ToString(), halconProcedure);
            }

            var files = GetSharedVariable("IMAGES_FILES", new Dictionary<string, byte[]>());
            SetVariable("IMAGES_FILES", files);

            for (int i = 0; i < muestras; i++)
            {
                var halconProcedure = GetSharedVariable<TestHalconExtension.ProcedureInputs>("LEDS_" + tipoFase.ToString() + i.ToString());
                var halconResultTupla = this.TestHalconProcedureAsync(halconProcedure);
                SetSharedVariable("COLORS_" + tipoFase.ToString() + i.ToString(), halconResultTupla.Item2.Replace("\"", "").Trim('[', ']').ToUpper().Replace(" ", "").Split(',').ToList());
            }

            files = GetVariable("IMAGES_FILES", new Dictionary<string, byte[]>());
            SetSharedVariable("IMAGES_FILES", files);
        }

        public void TestLedsON(FasesEnum fase)
        {
            if (fase == FasesEnum.L1)
                Resultado.Set("POSICION", "#" + SequenceContext.Current.NumInstance.ToString(), ParamUnidad.SinUnidad);

            var leds = GetSharedVariable<List<string>>("COLORS_ON0").GetRange((NumInstance - 1) * 3, 3);
            var ledsBBDD = Vision.GetString("LEDS_ON_"+ fase +"_COLORS", "NO", ParamUnidad.SinUnidad).Trim().Split(';').ToList();

            var exceptions = new List<SauronException>();
            for (int i = 0; i < leds.Count; i++)
                if (leds[i] != ledsBBDD[i])
                    exceptions.Add(Error().UUT.HARDWARE.LEDS("LEDS_ON_L" + (i + 1).ToString()));

            Resultado.Set($"LEDS_ON_{fase}", string.Join(";", leds), ParamUnidad.SinUnidad);

            if (exceptions.Count > 0)
                throw new AggregateException(exceptions);
        }

        public void TestLedsOFF()
        {
            List<List<string>> multipleImageLeds = new List<List<string>>();

            for (int i = 0; i < (int)Configuracion.GetDouble("NUMERO_MUESTRAS_OFF_TOTAL", 3, ParamUnidad.SinUnidad); i++)
                multipleImageLeds.Add(GetSharedVariable<List<string>>("COLORS_OFF" + i.ToString()).GetRange((NumInstance - 1) * 3, 3));

            //var allLeds = new List<string>() { "RED", "RED", "RED" };
            var ledsOffCount = new List<int>() { 0, 0, 0 };

            foreach (List<string> leds in multipleImageLeds)
                for (int led = 0; led < ledsOffCount.Count; led++)
                    if (leds[led] == "OFF")
                        ledsOffCount[led]++;

            //No se usa porque se mira que el led este apagado dos veces
            //var ledsBBDD = Vision.GetString("LEDS_OFF_COLORS", "NO", ParamUnidad.SinUnidad).Trim().Split(';').ToList();

            var numOK = (int)Configuracion.GetDouble("NUMERO_MUESTRAS_OFF_ACEPTADAS", 2, ParamUnidad.SinUnidad);

            var exceptions = new List<SauronException>();
            for (int i = 0; i < ledsOffCount.Count; i++)
            {
                Resultado.Set("LEDS_OFF_L" + (i + 1).ToString(), ledsOffCount[i], ParamUnidad.SinUnidad);

                if (ledsOffCount[i] < numOK)
                    exceptions.Add(Error().UUT.HARDWARE.LEDS("LEDS_OFF_L" + (i + 1).ToString()));
            }

            if (exceptions.Count > 0)
                throw new AggregateException(exceptions);
         }

        public void SaveImages()
        {
            var files = GetSharedVariable("IMAGES_FILES", new Dictionary<string, byte[]>());
            SetVariable("IMAGES_FILES", files);
        }

        public void TestFinish()
        {        
            var metrix = GetSharedVariable<MTX3293>("METRIX", (MTX3293)null);
            var generator = GetSharedVariable<KeySight33500>("GENERATOR", (KeySight33500)null);

            if (metrix != null)
                metrix.Dispose();

            if (generator != null)
            {
                generator.Off(1);
                generator.Dispose();
            }
        }

        public enum TipoFase
        {
            ON,
            ON_ALL,
            OFF,
            SPORADIC
        }

        public enum FasesEnum
        {
            L1,
            L2,
            L3,
            ALL
        }

        // TEST DE ESPORADICOS PARA RECOGER DATOS 

        public void TestLedsSporadic()
        {
            var leds = GetSharedVariable<List<string>>("COLORS_SPORADIC0").GetRange((NumInstance - 1) * 3, 3);
            var ledsBBDD = Vision.GetString("LEDS_OFF_COLORS", "NO", ParamUnidad.SinUnidad).Trim().Split(';').ToList();

            var numSporadic = 0;

            for (int i = 0; i < leds.Count; i++)
                if (leds[i] != ledsBBDD[i])
                    numSporadic++;

            Resultado.Set("NUMERO_ESPORADICOS", numSporadic, ParamUnidad.Numero);
        }

        public void TestCaptureLedsSporadic()
        {
            CAMERA_SN = GetSharedVariable<string>("CAMERA_SN", CAMERA_SN);

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLedsReturnColors, CAMERA_SN, "IVDS_LEDS_SPORADIC0", "LEDS_SPORADIC0", "IVDS", 1);
            var halconProcedure = GetVariable("HALCON_PROCEDURE_IVDS_LEDS_SPORADIC0");
            SetSharedVariable("LEDS_SPORADIC0", halconProcedure);
        }

        public void AsignedManufacturedDate()
        {
            TestInfo.FechaFabricacion = string.Format("{0:00}/{1:0000}", this.GetWeekNumber(), DateTime.Now.Year);
        }

    }
}
