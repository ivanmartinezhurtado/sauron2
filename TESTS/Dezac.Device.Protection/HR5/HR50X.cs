﻿using System;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.00)]
    public class HR50X : DeviceBase
    {
        #region Instance and disposal

        public HR50X()
        {
        }

        public HR50X(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 19200, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferic;
            Modbus.UseFunction06 = true;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #endregion

        #region Read operations

        public string ReadFimwareVersion()
        {
            return (Modbus.ReadRegister((ushort)Registers.FIRMWARE_VERSION) / 10D).ToString();
        }

        public Models ReadModel()
        {
            return (Models)Modbus.ReadRegister((ushort)Registers.DEVICE_MODEL);
        }

        public Inputs ReadInputs()
        {
            return (Inputs)Modbus.ReadRegister((ushort)Registers.INPUTS);
        }

        public int ReadCalibrationResult()
        {
            var highLevel = Modbus.ReadRegister((ushort)Registers.CALIBRATION_RESULT_HIGH_LEVEL);
            var lowLevel =  Modbus.ReadRegister((ushort)Registers.CALIBRATION_RESULT_LOW_LEVEL);

            return ((highLevel << 16) + lowLevel);            
        }

        public int ReadSerialNumber()
        {
            var highLevel = Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER_HIGH_LEVEL);
            var lowLevel = Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER_LOW_LEVEL);

            return ((highLevel << 16) + lowLevel);
        }

        public int ReadFrameNumber()
        {
            var highLevel = Modbus.ReadRegister((ushort)Registers.FRAME_NUMBER_HIGH_LEVEL);
            var lowLevel = Modbus.ReadRegister((ushort)Registers.FRAME_NUMBER_LOW_LEVEL);

            return ((highLevel << 16) + lowLevel);
        }

        public ushort ReadErrorCode()
        {
            return Modbus.ReadRegister((ushort)Registers.ERROR_CODE);
        }

        #endregion

        #region Write operations

        public void FlagTest()
        {
            Modbus.Write((ushort)Registers.DEVICE_OPERATIONS, (ushort)Operations.FLAG_TEST);
        }

        public void Reset()
        {
            Modbus.Retries = 0;
            Modbus.WithTimeOut((x) => x.Write((ushort)Registers.DEVICE_OPERATIONS, (ushort)Operations.RESET));
            Modbus.Retries = 3;
        }

        public void WriteModel(Models model)
        {
            Modbus.Write((ushort)Registers.DEVICE_MODEL, (ushort)model);
        }

        public void WriteDeviceOperations(Operations operation)
        {
            Modbus.Write((ushort)Registers.DEVICE_OPERATIONS, (ushort)operation);
        }

        public void WriteOutputs(params Outputs[] outputs)
        {
            var outputValues = 0;
            foreach (Outputs output in outputs)
                outputValues |= (ushort)output;

            Modbus.Write((ushort)Registers.OUTPUTS, (ushort)outputValues);
        }

        public void WriteLeds(params Leds[] leds)
        {
            var ledValues = 0;
            foreach (Leds led in leds)
                ledValues |= (ushort)led;

            Modbus.Write((ushort)Registers.LEDS, (ushort)ledValues);
        }

        public void WriteCalibrationOrder()
        {
            Modbus.Write((ushort)Registers.DEVICE_OPERATIONS, (ushort)Operations.START_CALIBRATION);
        }

        public void WriteCalibrationFactors(int calibrationFactor)
        {
            Modbus.Write((ushort)Registers.CALIBRATION_RESULT_HIGH_LEVEL, (ushort)(calibrationFactor / ushort.MaxValue));
            Modbus.Write((ushort)Registers.CALIBRATION_RESULT_LOW_LEVEL, (ushort)(calibrationFactor % ushort.MaxValue));
        }

        public void WriteSerialNumber(int serialNumber)
        {
            Modbus.Write((ushort)Registers.SERIAL_NUMBER_HIGH_LEVEL, (ushort)(serialNumber >> 16));
            Modbus.Write((ushort)Registers.SERIAL_NUMBER_LOW_LEVEL, (ushort)(serialNumber));
        }

        public void WriteFrameNumber(int frameNumber)
        {
            Modbus.Write((ushort)Registers.FRAME_NUMBER_HIGH_LEVEL, (ushort)(frameNumber >> 16));
            Modbus.Write((ushort)Registers.FRAME_NUMBER_LOW_LEVEL, (ushort)(frameNumber));
        }

        public void WriteErrorCode(ushort errorCode)
        {
            Modbus.Write((ushort)Registers.ERROR_CODE, (ushort)(errorCode / ushort.MaxValue));

        }

        #endregion

        #region Registers and registers info


        public enum Models
        {
            HR_500 = 0x00,
            HR_502 = 0x02,
            CIRCUTOR_500 = 0x0A,
            CIRCUTOR_1000 = 0X0B,
            NULL = 0XFF
        }

        public enum Operations
        {
            RESET = 0x0000,
            FLAG_TEST = 0x0002,
            START_CALIBRATION = 0x0003
        }

        [Flags]
        public enum Inputs
        {
            EXTERNAL_TEST_RESET = 0x0001,
            TECLA_TEST = 0x0008,
            TECLA_RESET = 0x0010,
            TOROIDAL_ERROR = 0x0020
        }

        public enum Outputs
        {
            NONE = 0x0000,
            RELE_NO = 0x0001,
            RELE_NC = 0x0008
        }

        public enum Leds
        {
            NONE = 0x0000, 
            ON = 0x0100,
            TRIGGER = 0x0200,
            TEST_KEY_ILLUMINATION = 0x0004,
            RESET_KEY_ILLUMINATION = 0x0008,
        }

        public enum Registers
        {
            FIRMWARE_VERSION = 0x0000,
            DEVICE_OPERATIONS = 0x0001,
            OUTPUTS = 0x0002,
            LEDS = 0x0003,
            INPUTS = 0x0004,
            CALIBRATION_RESULT_HIGH_LEVEL = 0x0007,
            CALIBRATION_RESULT_LOW_LEVEL = 0x0008,
            DEVICE_MODEL = 0x0009,
            SERIAL_NUMBER_HIGH_LEVEL = 0x000A,
            SERIAL_NUMBER_LOW_LEVEL = 0x000B,
            FRAME_NUMBER_HIGH_LEVEL = 0x000C,
            FRAME_NUMBER_LOW_LEVEL = 0x000D,
            ERROR_CODE = 0x000E,
        }

        #endregion
    }
}