﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Utils;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(UpgradeFirmwareESP32Description))]
    public class UpgradeFirmwareESP32 : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Puerto de comunicaciones para grabar el equipo.")]
        public ActionVariable ComPort { get; set; } = new ActionVariable("COM4");

        [DefaultValue(BaudRateEnum.Bps230400)]
        [Category("3. Configuration")]
        [Description("Velocidad de comunicaciones en la grabación.")]
        public BaudRateEnum BaudRate { get; set; }

        [DefaultValue(SPIModeEnum.dio)]
        [Category("3. Configuration")]
        [Description("SPI Mode")]
        public SPIModeEnum SPIMode { get; set; }

        [DefaultValue(FlashSizeEnum._4Mbit)]
        [Category("3. Configuration")]
        [Description("Flash Size")]
        public FlashSizeEnum FlashSize { get; set; }

        [Category("3. Configuration")]
        [Description("Lista de archivos de binario con sus direcciones de memoria. La Key es el nombre y funciona por contains, no hace falta poner el nombre completo ni la extension. El Value es la direccion en hexadecimal.")]
        public TextValuePair[] BinaryFiles { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo hasta que el action de fallo por tiempo.")]
        public ActionVariable TimeOut { get; set; } = new ActionVariable("95");

        private string binaryPath = ConfigurationManager.AppSettings["PathFileBinary"];
        private string programPath = @"esptool.exe ";

        public override void Execute()
        {
            if (!File.Exists(programPath))
                Error().SOFTWARE.ESP32_FLASH_LOADER.NO_INSTALADO("No se encuentra el programa").Throw();

            var Arguments = new StringBuilder();
            Arguments.Append($"--port {ComPort.Value} ");
            Arguments.AppendFormat($"--baud {(int)BaudRate} ");
            Arguments.AppendFormat($"write_flash ");
            Arguments.AppendFormat($"--flash_mode {SPIMode} ");
            Arguments.AppendFormat($"--flash_size {(int)FlashSize}MB ");

            foreach (var binary in BinaryFiles)
            {
                var file = Directory.GetFiles(binaryPath).ToList().Where(f => f.Contains(binary.Key)).FirstOrDefault();
                Arguments.AppendFormat($@"0x{binary.Value} {file} ");              
            }

            Logger.InfoFormat("Arguments: {0}", Arguments.ToString());
            var ps = new ProcessStartInfo(programPath, Arguments.ToString());
            ps.RedirectStandardOutput = false;
            ps.UseShellExecute = true;
            ps.CreateNoWindow = true;

            using (var process = new Process())
            {
                process.StartInfo = ps;
                process.Start();

                var timeInit = new TimeSpan(0, 0, TimeOut.GetValue<int>());
                var timeNow = DateTime.Now;
                do
                {
                    var current = DateTime.Now;
                    if (current.Subtract(timeNow).TotalSeconds > timeInit.TotalSeconds)
                    {
                        process.Kill();
                        Error().SOFTWARE.FIRMWARE.ERROR_GRABACION("TIME OUT").Throw();
                    }

                } while (!process.HasExited && !Context.IsCancellationRequested);

                if (process.ExitCode != 0)
                    Error().SOFTWARE.FIRMWARE.ERROR_GRABACION(string.Format("EXIT CODE {0}",process.ExitCode)).Throw();
            }
        }
    }

    public enum SPIModeEnum
    {
        NotDefined,
        qio,
        qout,
        dio,
        dout,
        fsatrd,
    }

    public enum FlashSizeEnum
    {
        NotDefined,
        _4Mbit = 4,
        _8Mbit = 8,
        _16Mbit = 16,
        _32Mbit = 32,
        _64Mbit = 64,
        _128Mbit = 128,
    }

    public class UpgradeFirmwareESP32Description : ActionDescription
    {
        public override string Name { get { return "UpgradeFirmwareESP32"; } }
        public override string Category { get { return "Tools/Firmware"; } }
        public override Image Icon { get { return Properties.Resources.stmicroelectronics; } }
    }
}
