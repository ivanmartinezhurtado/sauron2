﻿using System.Windows.Forms;

namespace Dezac.Tests.Actions.Views
{
    public partial class TextAreaEditor : Form
    {
        public TextAreaEditor()
        {
            InitializeComponent();

            this.Icon = Application.OpenForms[0].Icon;
        }

        public TextAreaEditor(string text)
            : this()
        {
            Value = text;
        }

        public string Value
        {
            get
            {
                return text.Text;
            }
            set
            {
                text.Text = value;
            }
        }
    }
}
