﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.06)]
    public class RECMAXTest : RECMAX_CVMTestBase
    {

        protected new const byte DETECTION_PUNTAS_SUPERIORES = 27;
        protected new const byte DETECTION_PINS_COMS = 32;
        protected new const byte PUNTAS_INFERIORES = 48;
        protected new const byte PISTON_COMS = 52;
        protected new const byte DETECTION_OUT_TRIP = 9;
        protected new const byte DETECTION_OUT_AUX = 11;
        protected const byte OUT_RECMAX_MODEL = 0;
        protected const byte CLOSE_CURRENT_CIRCUIT = 3;

        protected override void DeviceConnection()
        {
            Tower.Active24VDC();
            Tower.ActiveElectroValvule();

            SamplerWithCancel((p) => { return Tower.IO.DI[BLOCK_SECURITY]; }, "No se ha detectado el bloque de seguridad", 10, 500);
            SamplerWithCancel((p) => { return Tower.IO.DI[DEVICE_PRESENCE]; }, "No se ha detectado la presencia del equipo", 5, 500);
            SamplerWithCancel((p) => { return Tower.IO.DI[PUERTA_ABIERTA]; }, "No se ha detectado la puerta del útil cerrada", 10, 500);

            Tower.IO.DO.On(EVs);
            Tower.IO.DO.On(OUT_RECMAX_MODEL);

            //PREPARACION RECMAX O RECMAXCVM
            Tower.IO.DO.On(FIJACION_CARRO);
            Tower.IO.DO.Off(SWITCH_PISTON_PUNTAS);

            SamplerWithCancel((p) => { return !Tower.IO.DI[DETECTION_PISTON_SUPERIOR_CVM] && Tower.IO.DI[DETECTION_PISTON_SUPERIOR]; }, "No se ha detectado la posición corRecta del utillaje en los pistones superiores para el RECMAX", 20, 500);
            Tower.IO.DO.Off(SWITCH_PISTON_COMS);
            SamplerWithCancel((p) => { return !Tower.IO.DI[DETECTION_COMS_CVM] && Tower.IO.DI[DETECTION_COMS]; }, "No se ha detectado la posición corRecta del utillaje en los pistones de comunicación para el RECMAX", 10, 500);
            ////////

            Tower.IO.DO.On(FIJACION_EQUIPO);
            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_FIJACION_EQUIPO]; }, "No se ha detectado la fijación del equipo", 5, 500);

            //Tower.IO.DO.On(UNLOCK_DEVICE);
            //SamplerWithCancel((p) => { return tower.IO.DI[DETECTION_BLOQUEO_EQUIPO]; }, "No se ha detectado el bloqueo amarillo de Reconexión del equipo", 5, 500);

            Tower.IO.DO.On(PUNTAS_INFERIORES, PUNTAS_SUPERIORES);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PUNTAS_INFERIORES]; }, "No se ha detectado la entrada de los pistones inferiores en el equipo", 10, 500);
            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PUNTAS_SUPERIORES]; }, "No se ha detectado la entrada de los pistones superiores en el equipo", 10, 500);

            Tower.IO.DO.On(PISTON_COMS);
            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PINS_COMS]; }, "No se ha detectado la entrada del piston de comunicaciones TTL", 10, 500);

            Tower.IO.DO.OffWait(500, UNLOCK_DEVICE);
        }

        public override void CheckDeviceConnection()
        {

            SamplerWithCancel((p) => { return Tower.IO.DI[BLOCK_SECURITY]; }, "No se ha detectado el bloque de seguridad", 10, 500);
            SamplerWithCancel((p) => { return Tower.IO.DI[DEVICE_PRESENCE]; }, "No se ha detectado la presencia del equipo", 5, 500);


            SamplerWithCancel((p) => { return !Tower.IO.DI[DETECTION_PISTON_SUPERIOR_CVM] && Tower.IO.DI[DETECTION_PISTON_SUPERIOR]; }, "No se ha detectado la posición corRecta del utillaje en los pistones superiores para el RECMAX", 10, 500);

            SamplerWithCancel((p) => { return !Tower.IO.DI[DETECTION_COMS_CVM] && Tower.IO.DI[DETECTION_COMS]; }, "No se ha detectado la posición corRecta del utillaje en los pistones de comunicación para el RECMAX", 10, 500);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_FIJACION_EQUIPO]; }, "No se ha detectado la fijación del equipo", 5, 500);

            //Tower.IO.DO.On(UNLOCK_DEVICE);
            //SamplerWithCancel((p) => { return tower.IO.DI[DETECTION_BLOQUEO_EQUIPO]; }, "No se ha detectado el bloqueo amarillo de Reconexión del equipo", 5, 500);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PUNTAS_INFERIORES] && Tower.IO.DI[DETECTION_PUNTAS_SUPERIORES]; }, "No se ha detectado la entrada de los pistones laterales en el equipo", 10, 500);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NO] || Tower.IO.DI[DETECTION_OUT_BREAK_NC]; }, "No se ha detectado la palanca en ninguna de las dos posiciones (ON/OFF) --> SENSOR KO", 10, 500);

            string posicionPalanca = string.Empty;

            if (Tower.IO.DI[DETECTION_PALANCA_MOTOR_ON] && !Tower.IO.DI[DETECTION_PALANCA_MOTOR_OFF])
                posicionPalanca = "OFF";
            else
                posicionPalanca = "ON";

            logger.InfoFormat("SENSOR OK --> Ha detectado la palanca en " + posicionPalanca);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PALANCA_MOTOR_OFF]; }, "No se ha detectado la palanca en la posicion OFF", 10, 500);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_ANCLA]; }, "No se ha detectado el ancla del equipo", 10, 500);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PINS_COMS]; }, "No se ha detectado la entrada del piston de comunicaciones TTL", 10, 500);

            Tower.IO.DO.OffWait(500, UNLOCK_DEVICE);
        }

        protected override void DeviceDisConnection()
        {
             Tower.IO.DO.Off(PISTON_COMS);
            SamplerWithCancelWhitOutCancelToken(() => { return !Tower.IO.DI[DETECTION_PINS_COMS]; }, "No se ha detectado la salida del piston de comunicaciones TTL", 5, 500, 0, false, false);

            Tower.IO.DO.Off(PUNTAS_INFERIORES, PUNTAS_SUPERIORES);
            SamplerWithCancelWhitOutCancelToken(() => { return !Tower.IO.DI[DETECTION_PUNTAS_INFERIORES] && !Tower.IO.DI[DETECTION_PUNTAS_SUPERIORES]; }, "No se ha detectado la salida de los pistones laterales en el equipo", 5, 500, 0, false, false);

            Tower.IO.DO.Off(PISTON_BLOQUEO);
            Tower.IO.DO.Off(UNLOCK_DEVICE);

            SamplerWithCancelWhitOutCancelToken(() => { return !Tower.IO.DI[DETECTION_PISTON_DESBLOQUEO_EQUIPO]; }, "No se ha detectado el desbloqueo del equipo", 5, 500, 0, false, false);
            Tower.IO.DO.Off(FIJACION_EQUIPO);
            SamplerWithCancelWhitOutCancelToken(() => { return !Tower.IO.DI[DETECTION_FIJACION_EQUIPO]; }, "No se ha detectado la desconexión de la fijación del equipo", 5, 500, 0, false, false);

            Tower.IO.DO.Off(FIJACION_CARRO);
            Delay(1000, "");

            Tower.IO.DO.Off(OUT_RECMAX_MODEL);
            //Tower.IO.DO.Off(SWITCH_PISTON_COMS);
            //SamplerWithCancel((p) => { return tower.IO.DI[DETECTION_COMS_CVM] && !tower.IO.DI[DETECTION_COMS]; }, "No se ha detectado la posición corRecta del utillaje en los pistones de comunicación para el RECMAXCVM", 5, 500);
            //Tower.IO.DO.On(FIJACION_CARRO);
            //SamplerWithCancel((p) => { return tower.IO.DI[DETECTION_PISTON_SUPERIOR_CVM] && !tower.IO.DI[DETECTION_PISTON_SUPERIOR]; }, "No se ha detectado la posición corRecta del utillaje en los pistones superiores para el RECMAXCVM", 5, 500);

        }

        public override void TestInitialization(int portCVM = 0, int portREC = 0)
        {
            CAMARA_DISPLAY = GetVariable<string>("CAMARA_DISPLAY", CAMARA_DISPLAY);
            CAMARA_DIFERENCIAL = GetVariable<string>("CAMARA_DIFERENCIAL", CAMARA_DIFERENCIAL);

            if (portREC == 0)
                portREC = Comunicaciones.SerialPort;
        
            Rec.Modbus.PerifericNumber = Convert.ToByte(1);

            DeviceConnection();

            DesfaseValue = new TriLineValue { L1 = 0, L2 = 120, L3 = 240 };

            Tower.ActiveVoltageCircuit();

            Tower.IO.DO.Off(PILOTO_ERROR);

            Tower.PowerSourceIII.Tolerance = 5;

            Tower.IO.DO.On(CLOSE_CURRENT_CIRCUIT);

            Tower.IO.DO.On(DISCONECT_TOROIDAL);
        }

        public override void TestComunications()
        {
            var versionREC = string.Empty;
            SamplerWithCancel((p) =>
            {
                versionREC = Rec.ReadFirmwareVersion();
                return true;
            }, "Error de comunicaciones por TTL", 1, 500, 500, false, false);
            Resultado.Set("COMUNICATION_TTL", "OK", ParamUnidad.SinUnidad);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, versionREC, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware del REC incorrecta"), ParamUnidad.SinUnidad); 
        }

        public override void TestLock()
        {
            Tower.IO.DO.On(UNLOCK_DEVICE);
            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PISTON_DESBLOQUEO_EQUIPO]; }, "No se ha detectado el bloque de deteccion del bloqueo amarillo llegar a su fin de carrera", 5, 500, 250, false, false);

            //SamplerWithCancel((p) =>
            //{
            //    return Tower.IO.DI[DETECTION_BLOQUEO_EQUIPO];
            //}, "Error, no se ha detectado el bloqueo del equipo en la posición correcta", 5, 100, 500, false, false);

            SamplerWithCancel((p) =>
            {           
                return Tower.Chroma.ReadCurrent() <= 0.02 && Tower.IO.DI[DETECTION_BLOQUEO_EQUIPO];
            }, "Error, no se ha detectado el equipo bloqueado", 5, 100, 100, false, false);
            Resultado.Set("LOCK_DEVICE", "OK", ParamUnidad.SinUnidad);

            Tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0.5, 50);
            Tower.IO.DO.PulseOn(150, PISTON_BLOQUEO);
            Tower.IO.DO.Off(UNLOCK_DEVICE);

            SamplerWithCancel((p) =>
            {
                return Tower.Chroma.ReadCurrent() > 0;
            }, "Error, no se ha detectado el equipo desbloqueado", 5, 100, 100, false, false);


            //SamplerWithCancel((p) =>
            //{
            //    var reading = Rec.ReadInputStatus();
            //    return reading.HasFlag(RECMAX.Inputs.ARD_MANUAL);
            //}, "Error, no se ha detectado el equipo desbloqueado", 5, 100, 100, false, false);
            Resultado.Set("UNLOCK_DEVICE", "OK", ParamUnidad.SinUnidad);

            Tower.IO.DO.OffWait(100, PISTON_BLOQUEO);
            Tower.IO.DO.Off(UNLOCK_DEVICE);
        }

        public override void TestSetupDefault()
        {
            Rec.FlagTest();
            ParametrizationCustom();
            Delay(1200, "Escribiendo parametrizacion por defecto REC");

            Tower.Chroma.ApplyOffAndWaitStabilisation();
            Delay(3000, "Reseteando equipo");
            Tower.Chroma.ApplyAndWaitStabilisation();

            Delay(1000, "Espera antes de enviar FlagTest para evitar que se sature."); //Si enviamos FlagTest sin la espera tras reinicio, el equipo va bajando la tension interna.
            Rec.FlagTest();
            var x = Rec.ReadErrorCode();
            logger.InfoFormat("Error Code after reset: {0}", x.ToString());

        }

        public override void TestDigitalOutputs()
        {
            var listOutputsStates = new List<RECMAX.Outputs>();
            listOutputsStates.Add(RECMAX.Outputs.ALL_RELAYS);

            foreach (var output in listOutputsStates)
            {
                Rec.WriteOutputs(output, RECMAX.Outputs.DISPLAY_ACTIVATION);

                SamplerWithCancel((p) =>
                {
                    if (!Tower.IO.DI[DETECTION_OUT_TRIP] && Tower.IO.DI[DETECTION_OUT_AUX])
                        return true;

                    if (Tower.IO.DI[DETECTION_OUT_TRIP] && !Tower.IO.DI[DETECTION_OUT_AUX])
                        throw new Exception("Error, no se ha detectado la activación de ninguna de las dos salidas");
                    else
                    {
                        if (Tower.IO.DI[DETECTION_OUT_TRIP])
                            throw new Exception("Error, no se ha detectado la activación de la salida TRIP");
                        else
                            throw new Exception("Error, no se ha detectado la activación de la salida AUX");
                    }
                }, string.Empty, 10, 100, 100, false, false);

                Rec.WriteOutputs(RECMAX.Outputs.DISPLAY_ACTIVATION);
                SamplerWithCancel((p) =>
                {
                    if (Tower.IO.DI[DETECTION_OUT_TRIP] && !Tower.IO.DI[DETECTION_OUT_AUX])
                        return true;

                    if (!Tower.IO.DI[DETECTION_OUT_TRIP] && Tower.IO.DI[DETECTION_OUT_AUX])
                        throw new Exception("Error, no se ha detectado la desactivación de ninguna de las dos salidas");
                    else
                    {
                        if (!Tower.IO.DI[DETECTION_OUT_TRIP])
                            throw new Exception("Error, no se ha detectado la desactivación de la salida TRIP");
                        else
                            throw new Exception("Error, no se ha detectado la desactivación de la salida AUX");
                    }
                }, string.Empty, 10, 100, 100, false, false);

                Resultado.Set(string.Format("{0}", output.ToString()), "OK", ParamUnidad.SinUnidad);
            } 
        }

        public override void TestAnalogOuputs()
        {
            var hasDisplay = Configuracion.GetString("HAS_DISPLAY", "SI", ParamUnidad.SinUnidad) == "SI" ? true : false;

            Tower.Chroma.ApplyOff();
            Delay(3000, "Reseteando equipo");
            Tower.Chroma.ApplyAndWaitStabilisation();

            if (Tower.IO.DI[DETECTION_OUT_BREAK_NO] && !Tower.IO.DI[DETECTION_OUT_BREAK_NC]) //TODO mirar sensor palanca "Espera modificacion util"
                {
                    Tower.IO.DO.PulseOn(500, EXTERNAL_LOCKED);
                    Delay(1000, "Subiendo Motor");
                    SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NC] && !Tower.IO.DI[DETECTION_OUT_BREAK_NO]; }, "Error, no se ha detectado la salida estado MCB activada", 3, 100, 1500, false, false);
                    Delay(2500, "Espera entre rearmes");
                }

            Tower.IO.DO.PulseOn(500, hasDisplay ? EXTERNAL_LOCKED : EXTERNAL_TRIGGER);
            SamplerWithCancel((p) =>
            {
                return Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.MAGNETOTHERMIC);
            }, "Error, no se ha detectado el disparo de bobina", 5, 100, 500, false, false);
         
            Resultado.Set("TRIGGER_INDUCTOR", "OK", ParamUnidad.SinUnidad);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NO] && !Tower.IO.DI[DETECTION_OUT_BREAK_NC]; }, "Error, no se ha detectado la salida estado MCB desactivada", 5, 100, 0, false, false);
            Resultado.Set("OUT_BREAK_NO", "OK", ParamUnidad.SinUnidad);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NO] && !Tower.IO.DI[DETECTION_OUT_BREAK_NC]; }, "Error, no se ha detectado la palanca en la posición OFF", 5, 100, 0, false, false);

            Delay(1000, "Reposo de motor entre rearmes");

            Rec.FlagTest();

            Rec.WriteOutputs(RECMAX.Outputs.ENGINE_UP, RECMAX.Outputs.DISPLAY_ACTIVATION);
            Delay(1000, "Subiendo el motor");
            Rec.WriteOutputs(RECMAX.Outputs.ENGINE_DOWN, RECMAX.Outputs.DISPLAY_ACTIVATION);
            SamplerWithCancel((p) =>
            {
                return !Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.MAGNETOTHERMIC);
            }, string.Format("Error, no se ha detectado la activacion del Magnetotermico"), 10, 100, 1500, false, false);
            Resultado.Set("REARM", "OK", ParamUnidad.SinUnidad);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NC] && !Tower.IO.DI[DETECTION_OUT_BREAK_NO]; }, "Error, no se ha detectado la salida estado MCB activada", 5, 100, 0, false, false);
            Resultado.Set("OUT_BREAK_NC", "OK", ParamUnidad.SinUnidad);

            //SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PALANCA_MOTOR_ON] && !Tower.IO.DI[DETECTION_PALANCA_MOTOR_OFF]; }, "Error, no se ha detectado la palanca en la posición ON", 10, 100, 500, false, false);
        }

        public override void TestDigitalInputs()
        {
            Tower.IO.DO.On(EXTERNAL_TRIGGER);
            SamplerWithCancel((p) =>
            {
                return Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.EXTERNAL_TRIGGER);
            }, "Error, no se ha detectado la entrada Ext activada", 10, 100, 100, false, false);
            Tower.IO.DO.Off(EXTERNAL_TRIGGER);
            SamplerWithCancel((p) =>
            {
                return !Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.EXTERNAL_TRIGGER);
            }, "Error, no se ha detectado la entrada Ext desactivada", 10, 100, 100, false, false);

            Resultado.Set("EXTERNAL_INPUT", "OK", ParamUnidad.SinUnidad);
        }

        public override void TestMeasureIDiferencial()
        {
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 230, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0.25, L2 = 0, L3 = 0 }, 50, 0, DesfaseValue, true);
            base.TestMeasureIDiferencial();
        }

        public override void TestTrigger()
        {
            Tower.IO.DO.On(CORRIENTE_DIRECTA);

            Tower.Chroma.ApplyOff();
            Delay(3000, "Reseteando equipo");
            Tower.Chroma.ApplyAndWaitStabilisation();

            Tower.ActiveCurrentCircuit(true, true, true);

            var magnetoType = Configuracion.GetString("TIPO_MAGNETO", "4P", ParamUnidad.SinUnidad);

            if (magnetoType.Contains("4P"))
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 230, L2 = 230, L3 = 230 }, new TriLineValue { L1 = Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 0.15, ParamUnidad.A), L2 = 0, L3 = 0 }, 50, 0, DesfaseValue, true);
            else
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 230, L2 = 0, L3 = 0 }, new TriLineValue { L1 = Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 0.15, ParamUnidad.A), L2 = 0, L3 = 0 }, 50, 0, DesfaseValue, true);

            Rec.WriteOperatingMode(RECMAX.OperatingMode.NORMAL);
            Delay(1000, "Reset equipo al ponerlo modo normal");

            Rec.WriteTriggerCurrent(2);
            Rec.WriteTriggerTime(0);

            ///
            Delay(100, "Cambiando de escala");
            Tower.ActiveCurrentCircuit(false, true, true);
            Delay(2100, "Estabilizando medida del equipo");

            var currentLeakVerif = new AdjustValueDef(Params.I.Other("LEAK").Verificacion.Name, 0, 0, 0, 0, Params.I.Null.TestPoint("LEAK").Tol(), ParamUnidad.s);

            TestCalibracionBase(currentLeakVerif,
               () =>
               {
                   var reading = Rec.ReadLeakCurrent();
                   return reading;

               },
               () =>
               {
                   return Tower.PowerSourceIII.ReadCurrent().L1;
               }, 1, 2, 1050, "Error, corriente medida de fuga fuera de márgenes");


            Tower.ActiveCurrentCircuit(true, true, true);

            var current = Rec.ReadLeakCurrent();
            logger.InfoFormat("Corriente leída mientras esta cortocircuitada en la torre : {0}", current);

            Rec.WriteTriggerCurrent(0);
            Delay(2100, "");


            SamplerWithCancel((step) =>
            {
                if (step == 3)
                {
                    Rec.FlagTest();
                    Rec.WriteOutputs(RECMAX.Outputs.ENGINE_UP, RECMAX.Outputs.DISPLAY_ACTIVATION);
                    Delay(2000, "Subiendo motor");
                    Rec.WriteOutputs(RECMAX.Outputs.ENGINE_DOWN, RECMAX.Outputs.DISPLAY_ACTIVATION);
                    Delay(3000, "Rearmando equipo");
                    Rec.WriteOperatingMode(RECMAX.OperatingMode.NORMAL);
                }

                return Tower.IO.DI[DETECTION_MCB_L1];
            }, "Error, no se ha detectado la continuidad del magnetotermico en L1", 6, 1000, 0, false, false);

            if (magnetoType.Contains("4P"))
            {

                SamplerWithCancel((step) =>
                {
                    return Tower.IO.DI[DETECTION_MCB_L2];
                }, "Error, no se ha detectado la continuidad del magnetotermico en L2", 6, 1000, 0, false, false);

                SamplerWithCancel((step) =>
                {
                    return Tower.IO.DI[DETECTION_MCB_L3];
                }, "Error, no se ha detectado la continuidad del magnetotermico en L3", 6, 1000, 0, false, false);

            }

            this.InParellel(
           () =>
           {
               Tower.MUX.Reset();
               Tower.MUX.Connect2((int)InputMuxEnum.IN9, Instruments.IO.Outputs.FreCH1, (int)InputMuxEnum.IN2, Instruments.IO.Outputs.FreCH2);

               Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
               Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);

               Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
               Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);
           });

            var adjustValue = new AdjustValueDef(Params.TIME.Null.TRIGGER, ParamUnidad.s);

            var adjustResult = TestMeasureBase(adjustValue, (s) =>
            {
                try
                {
                    var timeResult = Tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 5000, HP53131A.Channels.CH1, () =>
                    {
                        Delay(50, "");

                        if (!Tower.IO.DI[DETECTION_OUT_BREAK_NC] && Tower.IO.DI[DETECTION_OUT_BREAK_NO])
                            logger.InfoFormat("El equipo ya está disparado antes de sacar la corriente de la torre.");

                        Tower.ActiveCurrentCircuit(false, true, true);
                    });

                    return (timeResult.Value);
                }
                catch (Exception)
                {
                    throw new Exception(string.Format("Error, el equipo no ha disparado"));
                }
            }, 0, 1, 1000);

            Tower.ActiveCurrentCircuit(true, true, true);
        }

        public override void TestCustomization()
        {
            Rec.FlagTest();

            Rec.WriteBastidor(Convert.ToInt32(TestInfo.NumBastidor));

            ParametrizationCustom();

            Tower.Chroma.ApplyOffAndWaitStabilisation();
            Delay(3500, "Reset equipo");
            Tower.Chroma.ApplyAndWaitStabilisation();

            Rec.FlagTest();           

            var bastidor = Rec.ReadBastidor();
            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_BASTIDOR, bastidor.ToString(), TestInfo.NumBastidor.ToString(), Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error, número de bastidor leído del equipo no coincide con el grabado"), ParamUnidad.SinUnidad);

            var numSerie = Rec.ReadSerialNumber();

            var errorCode = Rec.ReadErrorCode();
            if (errorCode != RECMAX.ErrorCodes.NO_ERROR)
                Error().UUT.FIRMWARE.SETUP(errorCode.ToString()).Throw();

        }

        public override void TestFinish()
        {
            Tower.IO.DO.Off(UNLOCK_DEVICE);
            try
            {
                if (!Tower.IO.DI[DETECTION_PALANCA_MOTOR_OFF])
                {
                    Delay(2000, "Esperando maniobra del motor para no dejarlo a medias");

                    Rec.FlagTest();

                    Rec.WriteOutputs(RECMAX.Outputs.ENGINE_DOWN, RECMAX.Outputs.TRIGGER_INDUCTOR, RECMAX.Outputs.DISPLAY_ACTIVATION);

                    SamplerWithCancelWhitOutCancelToken(() =>
                    {
                        return Tower.IO.DI[DETECTION_PALANCA_MOTOR_OFF];
                    }, "Error, no se ha detectado la palanca en la posición OFF", 10, 200, 200, false, false);
                }
            }
            catch (Exception) { };

            Tower.ShutdownSources();

            Tower.IO.DO.Off(CLOSE_CURRENT_CIRCUIT);

            DeviceDisConnection();

            if (Rec != null)
                Rec.Dispose();

            if (Tower != null)
                Tower.Dispose();

            if (IsTestError)
                Tower.IO.DO.On(PILOTO_ERROR);
        }

        private void ParametrizationFab()
        {
            //BORRADO PAGS
            Rec.WritePageErasing(RECMAX.PagesErasing.SETUP);
            Delay(30, "Borrando página SETUP");
            Rec.WritePageErasing(RECMAX.PagesErasing.PARAMETRIZATIONS);
            Delay(50, "Borrando página PARAMETRIZACIONES");

            //MODELO SUBMODELO
            RECMAX.DeviceModel model;
            Enum.TryParse<RECMAX.DeviceModel>(Identificacion.MODELO_FAB, out model);
            Rec.WriteDeviceModel(model);
            Delay(30, "Escribiendo Modelo");
            Resultado.Set("MODELO_FAB", model.ToString(), ParamUnidad.SinUnidad);
            var modelRead = Rec.ReadDeviceModel();
            logger.InfoFormat(string.Format("MODELO FAB LEIDO : {0}", modelRead));

            var subModel = Identificacion.SUBMODELO_FAB;
            Rec.WriteDeviceSubModel(subModel);
            Delay(30, "Escribiendo SubModelo");
            Resultado.Set("SUBMODELO_FAB", subModel, ParamUnidad.SinUnidad);
            var submodelRead = Rec.ReadDeviceSubModel();
            logger.InfoFormat(string.Format("SUBMODELO FAB LEIDO : {0}", submodelRead));

            WriteCostumerTableCurrentTimeFab();

            //FRECUENCIA DE TRABAJO
            var frec = RECMAX.OperatingFrequency._50Hz;
            Enum.TryParse<RECMAX.OperatingFrequency>(Parametrizacion.GetString("WORK_FRECUENCY_FAB", "_50Hz", ParamUnidad.SinUnidad), out frec);
            Rec.WriteOperatingFrequency(frec);
            Delay(30, "Escribiendo Frecuencia de trabajo");
            Resultado.Set("FRECUENCIA_TRABAJO_FAB", frec.ToString(), ParamUnidad.SinUnidad);
            var fr = Rec.ReadOperatingFrequency();
            logger.InfoFormat(string.Format("FRECUENCIA_TRABAJO LEIDA : {0}", fr.ToString()));

            //FUNCIÓN RELÉ AUXILIAR
            var alarm = RECMAX.TriggerCurrentAlarmPercentage.ERROR;
            Enum.TryParse<RECMAX.TriggerCurrentAlarmPercentage>(Parametrizacion.GetString("FUNCTION_RELAY_AUXILIAR_FAB", "ERROR", ParamUnidad.SinUnidad), out alarm);
            Rec.WritePrealarmCurrentPercentage(alarm);
            Delay(30, "Escribiendo Funcion relé auxiliar");
            Resultado.Set("FUNCION_RELE_AUX_FAB", alarm.ToString(), ParamUnidad.SinUnidad);
            var al = Rec.ReadPrealarmCurrentPercentage();
            logger.InfoFormat(string.Format("FUNCION RELE AUX LEIDA : {0}", al.ToString()));

            //SEGURIDAD RELÉ TRIP AND RELAY AUX
            var tripSecurity = RECMAX.RelayLogic.STANDARD;
            Enum.TryParse<RECMAX.RelayLogic>(Parametrizacion.GetString("RELAY_TRIP_SECURITY_FAB", "STANDARD", ParamUnidad.SinUnidad), out tripSecurity);
            Rec.WriteSecurityTripRelay(tripSecurity);
            Delay(30, "Escribiendo seguridad rele Trip");
            Resultado.Set("SEGURIDAD_RELE_TRIP_FAB", tripSecurity.ToString(), ParamUnidad.SinUnidad);
            var trip = Rec.ReadSecurityTripRelay();
            logger.InfoFormat(string.Format("SEGURIDAD RELE TRIP LEIDA : {0}", trip.ToString()));

            var auxiliarSecurity = RECMAX.RelayLogic.STANDARD;
            Enum.TryParse<RECMAX.RelayLogic>(Parametrizacion.GetString("RELAY_AUXILIAR_SECURITY_FAB", "STANDARD", ParamUnidad.SinUnidad), out auxiliarSecurity);
            Rec.WriteSecurityAuxRelay(auxiliarSecurity);
            Delay(30, "Escribiendo seguridad rele Auxiliar");
            Resultado.Set("SEGURIDAD_RELE_AUXILIAR_FAB", auxiliarSecurity.ToString(), ParamUnidad.SinUnidad);
            var aux = Rec.ReadSecurityAuxRelay();
            logger.InfoFormat(string.Format("SEGURIDAD RELE AUX LEIDA: {0}", aux.ToString()));

            //SECUENCIA RECONEXIÓN DIFERENCIAL
            var srd = Parametrizacion.GetString("SRD_FAB", "10", ParamUnidad.SinUnidad);
            Rec.WriteDifferentialReconnectionSequence(Convert.ToUInt16(srd));
            Delay(30, "Escribiendo SRD");
            Resultado.Set("SRD_FAB", srd, ParamUnidad.SinUnidad);
            var srdRead = Rec.ReadDifferentialReconnectionSequence();
            logger.InfoFormat(string.Format("SRD LEIDO : {0}", srdRead));

            //SECUENCIA RECONEXIÓN MAGNETOTÉRMICO
            var srm = Parametrizacion.GetString("SRM_FAB", "0", ParamUnidad.SinUnidad);
            Rec.WriteMagnetothermicReconnectionSequence(Convert.ToUInt16(srm));
            Delay(30, "Escribiendo SRM");
            Resultado.Set("SRM_FAB", srm, ParamUnidad.SinUnidad);
            var srmRead = Rec.ReadMagnetothermicReconnectionSequence();
            logger.InfoFormat(string.Format("SRM LEIDO : {0}", srmRead));

            //BLOQUEO TECLADO DISPLAY
            var keyboardLocking = RECMAX.KeyboardLocking.NO;
            Enum.TryParse<RECMAX.KeyboardLocking>(Parametrizacion.GetString("KEYBOARD_LOCK_FAB", "NO", ParamUnidad.SinUnidad), out keyboardLocking);
            Rec.WriteKeyboardLocking(keyboardLocking);
            Delay(30, "Escribiendo bloqueo teclado");
            Resultado.Set("BLOQUEO_TECLADO_FAB", keyboardLocking.ToString(), ParamUnidad.SinUnidad);
            var lockKeyboardRead = Rec.ReadKeyboardLocking();
            logger.InfoFormat(string.Format("BLOQUEO TECLADO LEIDO : {0}", lockKeyboardRead.ToString()));

            //BLOQUEO MENU OPCIONES
            var OptionsLocking = RECMAX.MenuOptions.NO;
            Enum.TryParse<RECMAX.MenuOptions>(Parametrizacion.GetString("OPTIONS_SETUP_LOCK_FAB", "NO", ParamUnidad.SinUnidad), out OptionsLocking);
            Rec.WriteMenuOptionsLocking(OptionsLocking);
            Delay(30, "Escribiendo bloqueo menús");
            Resultado.Set("BLOQUEO_MENU_OPCIONES_FAB", OptionsLocking.ToString(), ParamUnidad.SinUnidad);
            var oprionsLockRead = Rec.ReadMenuOptionsLocking();
            logger.InfoFormat(string.Format("BLOQUEO OPCIONES LEIDO : {0}", lockKeyboardRead.ToString()));

            WriteSetupsFab();

            Rec.WriteCodeParametrization((ushort)Identificacion.CRC_FIRMWARE_FAB);
            Delay(30, "Escribiendo CRC FABRICACION");
            var crcRead = Rec.ReadParametrizationCode();
            logger.InfoFormat(string.Format("CRC FAB LEIDO : {0}", crcRead));
            Rec.WritePageErasing(RECMAX.PagesErasing.SETUP);
            Delay(2000, "Borrando paginas de parametrización");

            var x = Rec.ReadErrorCode();
            logger.InfoFormat("Error Code : {0}", x.ToString());
        }

        private void WriteSetupsFab()
        {
            List<RECMAX.Settings> settings = new List<RECMAX.Settings>();

            var activationExternInput = RECMAX.ActivationTypeExternalInput._2TERMINALS;
            Enum.TryParse<RECMAX.ActivationTypeExternalInput>(Parametrizacion.GetString("ACTIVATION_TYPE_EXTERNAL_INPUT_FAB", "_2TERMINALS", ParamUnidad.SinUnidad), out activationExternInput);
            Resultado.Set("TIPO_ACTIVACION_ENTRADA_EXTERNA_FAB", activationExternInput.ToString(), ParamUnidad.SinUnidad);

            var activationModeExternInput = RECMAX.ActivationModeExternalInput.LEVEL;
            Enum.TryParse<RECMAX.ActivationModeExternalInput>(Parametrizacion.GetString("ACTIVATION_MODE_EXTERNAL_INPUT_FAB", "LEVEL", ParamUnidad.SinUnidad), out activationModeExternInput);
            Resultado.Set("MODO_ACTIVACION_ENTRADA_EXTERNA_FAB", activationModeExternInput.ToString(), ParamUnidad.SinUnidad);

            var infraTension = RECMAX.ValuesYesNo.NO;
            Enum.TryParse<RECMAX.ValuesYesNo>(Parametrizacion.GetString("SUPERVISION_INFRATENSION_FAB", "NO", ParamUnidad.SinUnidad), out infraTension);
            Resultado.Set("SUPERVISION_INFRATENSION_FAB", infraTension.ToString(), ParamUnidad.SinUnidad);

            var sobreTension = RECMAX.ValuesYesNo.NO;
            Enum.TryParse<RECMAX.ValuesYesNo>(Parametrizacion.GetString("SUPERVISION_SOBRETENSION_FAB", "NO", ParamUnidad.SinUnidad), out sobreTension);
            Resultado.Set("SUPERVISION_SOBRETENSION_FAB", sobreTension.ToString(), ParamUnidad.SinUnidad);

            var reconectionFixed = RECMAX.ValuesYesNo.NO;
            Enum.TryParse<RECMAX.ValuesYesNo>(Parametrizacion.GetString("SEQUENCE_RECONECTION_FIXED_FAB", "NO", ParamUnidad.SinUnidad), out reconectionFixed);
            Resultado.Set("SECUENCIA_RECONEXION_FIJA_FAB", reconectionFixed.ToString(), ParamUnidad.SinUnidad);

            settings.Add(activationExternInput == RECMAX.ActivationTypeExternalInput._2TERMINALS ? RECMAX.Settings.AMOUNT_TERMINALS : RECMAX.Settings.NO_SETTINGS);
            settings.Add(activationModeExternInput == RECMAX.ActivationModeExternalInput.PULSE ? RECMAX.Settings.EXTERNAL_TRIGGER : RECMAX.Settings.NO_SETTINGS);
            settings.Add(infraTension == RECMAX.ValuesYesNo.SI ? RECMAX.Settings.UNDERVOLTAGE_MONITORING : RECMAX.Settings.NO_SETTINGS);
            settings.Add(sobreTension == RECMAX.ValuesYesNo.SI ? RECMAX.Settings.OVERVOLTAGE_MONITORING : RECMAX.Settings.NO_SETTINGS);
            settings.Add(reconectionFixed == RECMAX.ValuesYesNo.SI ? RECMAX.Settings.RECONNECTION_SEQUENCE_FIXED : RECMAX.Settings.NO_SETTINGS);

            Rec.WriteSettings(settings.ToArray());
            Delay(30, "Escribiendo Ajustes1");
            var settingsRead = Rec.ReadSettings();
            logger.InfoFormat(string.Format("AJUSTES1 LEIDO : {0}", settingsRead));
        }

        protected void WriteCostumerTableCurrentTimeFab()
        {
            var currentScaleElements = Parametrizacion.GetString("TABLA_I_DISPARO_FAB", "30;100;300;500;1000", ParamUnidad.SinUnidad);

            var currents = GetCurrentScaleTable(currentScaleElements);

            var timeTableElements = Parametrizacion.GetString("TABLA_T_RETARDO_FAB", "INS;SEL;100;200;300;400;500;800;1000", ParamUnidad.SinUnidad);

            var times = GetTimeScaleTable(timeTableElements);

            Rec.WriteScaleTable(currents, RECMAX.TableKind.CURRENT);

            Rec.WriteScaleTable(times, RECMAX.TableKind.TIME);

            //LECTURA DE TABLAS
            var resultCurrents = Rec.ReadScaleTable(RECMAX.TableKind.CURRENT);

            Resultado.Set("TABLA_CORRIENTES_FAB", resultCurrents, ParamUnidad.SinUnidad, currents.listElementsString.Trim());

            if (resultCurrents.Trim() != currents.listElementsString.Trim())
                Error().UUT.CONFIGURACION.PARAMETROS_NO_GRABADOS("TABLA DE CORRIENTES LEIDA NO ES IGUAL A LA DE BBDD");

            var resultTime = Rec.ReadScaleTable(RECMAX.TableKind.TIME);

            Resultado.Set("TABLA_TIEMPOS_FAB", resultTime, ParamUnidad.SinUnidad, times.listElementsString.Trim());

            if (resultTime.Trim() != times.listElementsString.Trim())
                Error().UUT.CONFIGURACION.PARAMETROS_NO_GRABADOS("TABLA DE TIEMPOS DE RETARDO LEIDA NO ES IGUAL A LA DE BBDD");

            var current = Parametrizacion.GetDouble("I_DISPARO_DEFECTO_FAB", 30, ParamUnidad.mA);

            var posCurrent = currents.ListElemnts.Where((p) => p.GetElementValue == (ushort)current);

            if (posCurrent.Count() == 0)
                throw new Exception("Error parametrización incorrecta, el valor del parametro CURRENT_TRIGGER no existe en la tabla de corrientes");

            Resultado.Set("POSICION_I_DISPARO_DEFECTO_FAB", posCurrent.FirstOrDefault().Position.ToString(), ParamUnidad.SinUnidad);

            Rec.WriteTriggerCurrent(posCurrent.FirstOrDefault().Position);
            Delay(30, "Escribirendo Posicion de la tabla de Corriente disparo");

            var tiempo = Parametrizacion.GetDouble("T_RETARDO_DEFECTO_FAB", 100, ParamUnidad.ms);

            var posTime = times.ListElemnts.Where((p) => p.GetElementValue == (ushort)tiempo);

            if (posTime.Count() == 0)
                throw new Exception("Error parametrización incorrecta, la T_RETARDO_DEFECTO no existe en la tabla de tiempos");

            Resultado.Set("POSICION_T_RETARDO_DEFECTO_FAB", posTime.FirstOrDefault().Position, ParamUnidad.SinUnidad);

            Rec.WriteTriggerTime(posTime.FirstOrDefault().Position);
            Delay(30, "Escribirendo Posicion de la tabla de Tiempo de retardo de disparo");
        }
    }
}