﻿namespace WinTestPlayer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.telerikMetroBlueTheme1 = new Telerik.WinControls.Themes.TelerikMetroBlueTheme();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.statusBar = new Telerik.WinControls.UI.RadStatusStrip();
            this.sbTxtAutoTest = new Telerik.WinControls.UI.RadLabelElement();
            this.slideMenu = new WinTestPlayer.Controls.Expanders.ControlsExpander();
            this.header1 = new WinTestPlayer.Views.Header();
            ((System.ComponentModel.ISupportInitialize)(this.statusBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slideMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // mainPanel
            // 
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(365, 96);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(1319, 867);
            this.mainPanel.TabIndex = 3;
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.sbTxtAutoTest});
            this.statusBar.Location = new System.Drawing.Point(0, 963);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1684, 26);
            this.statusBar.TabIndex = 0;
            this.statusBar.Text = "radStatusStrip1";
            // 
            // sbTxtAutoTest
            // 
            this.sbTxtAutoTest.Name = "sbTxtAutoTest";
            this.statusBar.SetSpring(this.sbTxtAutoTest, false);
            this.sbTxtAutoTest.Text = "AutoTest";
            this.sbTxtAutoTest.TextWrap = true;
            // 
            // slideMenu
            // 
            this.slideMenu.AllowExpand = false;
            this.slideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.slideMenu.Location = new System.Drawing.Point(0, 96);
            this.slideMenu.Name = "slideMenu";
            this.slideMenu.Size = new System.Drawing.Size(365, 867);
            this.slideMenu.TabIndex = 2;
            // 
            // header1
            // 
            this.header1.BackColor = System.Drawing.Color.AliceBlue;
            this.header1.Dock = System.Windows.Forms.DockStyle.Top;
            this.header1.Location = new System.Drawing.Point(0, 0);
            this.header1.Margin = new System.Windows.Forms.Padding(4);
            this.header1.Name = "header1";
            this.header1.Size = new System.Drawing.Size(1684, 96);
            this.header1.TabIndex = 4;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1684, 989);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.slideMenu);
            this.Controls.Add(this.header1);
            this.Controls.Add(this.statusBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Studio Dezac (TSD)";
            this.ThemeName = "TelerikMetroBlue";
            ((System.ComponentModel.ISupportInitialize)(this.statusBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slideMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.TelerikMetroBlueTheme telerikMetroBlueTheme1;
        private Views.Header header;
        private Controls.Expanders.ControlsExpander slideMenu;
        private System.Windows.Forms.Panel mainPanel;
        private Views.Header header1;
        private Telerik.WinControls.UI.RadStatusStrip statusBar;
        private Telerik.WinControls.UI.RadLabelElement sbTxtAutoTest;
    }
}
