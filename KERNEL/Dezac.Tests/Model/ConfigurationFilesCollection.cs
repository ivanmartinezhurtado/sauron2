﻿using Dezac.Data;
using Dezac.Services;
using System.Collections.Generic;

namespace Dezac.Tests.Model
{
    public class ConfigurationFilesCollection : List<ConfigurationFile>
    {
        public ConfigurationFilesCollection()
        {
        }

        public void LoadAllFilesFromBBDD(PRODUCTO producto)
        {
            var configFileList = new List<T_TESTCONFIGFILEVERSION>();
            using (DezacService svc = new DezacService())
            {
                configFileList = svc.GetAllProductTestConfigFiles(producto);
            }
            foreach (var file in configFileList)
                this.Add(new ConfigurationFile(file));
        }

        public byte[] GetData(string clave)
        {
            ConfigurationFile file = Get(clave);
            if (file == null)
                return null;
            return file.File;
        }

        public ConfigurationFile Get(string clave)
        {
            ConfigurationFile pValue = this.Find(p => p.Clave == clave);
            return pValue;
        }
    }
}
