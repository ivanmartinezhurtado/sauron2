﻿using System;
using System.Collections.Generic;

namespace WinTaskRunner.Services
{
    public class EventBus
    {
        private Dictionary<string, List<Action<object, string, object>>> bindings = new Dictionary<string, List<Action<object, string, object>>>();

        public void Register(string eventName, Action<object, string, object> action)
        {
            List<Action<object, string, object>> actions = GetActions(eventName);
            if (actions == null)
            {
                actions = new List<Action<object, string, object>>();
                bindings.Add(eventName, actions);
            }

            actions.Add(action);
        }

        public void Fire(object sender, string eventName, object args)
        {
            List<Action<object, string, object>> actions = GetActions(eventName);
            if (actions == null)
                return;

            actions.ForEach(p => p(sender, eventName, args));
        }

        private List<Action<object, string, object>> GetActions(string eventName)
        {

            bindings.TryGetValue(eventName, out List<Action<object, string, object>> list);

            return list;
        }
    }
}
