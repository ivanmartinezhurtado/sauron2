﻿using Comunications.Message;
using Comunications.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Comunications.Protocolo
{
    public class ModbusProtocol : IProtocol
    {
        public const int RequestFrameStartLength = 6;
        public const int ResponseFrameStartLength = 3; //4;

        public string CaracterFinTx
        {
            get { return null; }
            set
            {
            }
        }

        public string CaracterFinRx
        {
            get { return null; }
            set
            {
            }
        }

        public virtual byte[] BuildMessage(ITransportMessage message)
        {
            ModbusMessage msg = message as ModbusMessage;

            byte[] frame = message.MessageFrame;

            List<byte> messageBody = new List<byte>(frame);
           
            messageBody.AddRange(ModbusUtility.CalculateCrc(frame));

            return messageBody.ToArray();
        }

        public virtual byte[] Read(int count, IStreamResource streamResource)
        {
            byte[] frameBytes = new byte[count];
            int numBytesRead = 0;

            while (numBytesRead != count)
                numBytesRead += streamResource.Read(frameBytes, numBytesRead, count - numBytesRead);

            return frameBytes;
        }

        public virtual byte[] ReadMessage(ITransportMessage message, IStreamResource streamResource)
        {
            byte[] frameStart = Read(ResponseFrameStartLength, streamResource);
            byte[] frameEnd = Read(ResponseBytesToRead(frameStart), streamResource);
            
            byte[] frame = frameStart.Concat(frameEnd).ToArray();
            
            byte functionCode = frameStart[1];

            if (functionCode > Modbus.ExceptionOffset)
                return frame;

            byte[] crc = Read(2, streamResource);

            if (!ChecksumsMatch(crc, frame))
            {
                string errorMessage = "Checksums failed to match";
                throw new IOException(errorMessage);
            }

            return frame;
        }

        protected bool ChecksumsMatch(byte[] crc, byte[] frame)
        {
            return BitConverter.ToUInt16(crc, 0) == BitConverter.ToUInt16(ModbusUtility.CalculateCrc(frame), 0);
        }

        public static int ResponseBytesToRead(byte[] frameStart)
        {
            byte functionCode = frameStart[1];

            // exception response
            if (functionCode > Modbus.ExceptionOffset)
                return 1;

            int numBytes;
            switch (functionCode)
            {
                case Modbus.WriteFileRecord:
                case Modbus.ReadCoils:
                case Modbus.ReadInputs:
                case Modbus.ReadHoldingRegisters:
                case Modbus.ReadInputRegisters:
                    numBytes = frameStart[2]; // + 1;
                    break;
                case Modbus.WriteSingleCoil:
                case Modbus.WriteSingleRegister:
                case Modbus.WriteMultipleCoils:
                case Modbus.WriteMultipleRegisters:
                case Modbus.Diagnostics:
                    numBytes = 3; //4;
                    break;
                default:
                    string errorMessage = String.Format("Function code {0} not supported.", functionCode);
                   // _logger.Error(errorMessage);
                    throw new NotImplementedException(errorMessage);
            }

            return numBytes;
        }

        public virtual byte[] ReadRequestMessage(ITransportMessage message, IStreamResource streamResource)
        {
            byte[] frame = Read(RequestFrameStartLength, streamResource);

            var NumBytesToReuqest = RequestBytesToRead(frame);

            byte functionCode = frame[1];

            byte[] crc = Read(2, streamResource);

            if (!ChecksumsMatch(crc, frame))
            {
                string errorMessage = "Checksums failed to match";
                throw new IOException(errorMessage);
            }

            return frame;
        }

        public static int RequestBytesToRead(byte[] frameStart)
        {
            byte functionCode = frameStart[1];

            // exception response
            if (functionCode > Modbus.ExceptionOffset)
            {
                string errorMessage = String.Format("Function code {0} not supported.", functionCode);
                throw new NotImplementedException(errorMessage);
            }

            int numBytes;
            switch (functionCode)
            {
                case Modbus.WriteFileRecord:
                case Modbus.ReadCoils:
                case Modbus.ReadInputs:
                case Modbus.ReadHoldingRegisters:
                case Modbus.ReadInputRegisters:
                    numBytes = frameStart[5];
                    break;
                case Modbus.WriteSingleCoil:
                case Modbus.WriteSingleRegister:
                case Modbus.WriteMultipleCoils:
                case Modbus.WriteMultipleRegisters:
                case Modbus.Diagnostics:
                    numBytes = frameStart[5]; //4;
                    break;
                default:
                    string errorMessage = String.Format("Function code {0} not supported.", functionCode);
                    throw new NotImplementedException(errorMessage);
            }

            return numBytes;
        }

    }
}
